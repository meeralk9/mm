﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.CardPost
{

    //public class RequestPostCard
    //{
    //    public RailsBankHandler.CardPost.Root RootObject { get; set; }
    //    //public string ledger_id { get; set; }
    //}

    public class ResponseObject
    {
        public string card_id { get; set; }
        public string error { get; set; }

    }
    public class CardDeliveryAddress
    {
        public string address_number { get; set; }
        public string address_refinement { get; set; }
        public string address_region { get; set; }
        public string address_iso_country { get; set; }
        public string address_street { get; set; }
        public string address_city { get; set; }
        public string address_postal_code { get; set; }
    }

    public class RequestPostCard
    {
        public string ledger_id { get; set; }
        public string name_on_card { get; set; }
        public string card_carrier_type { get; set; }
        public string card_delivery_name { get; set; }
        public string qr_code_content { get; set; }
        public string telephone { get; set; }
        public string card_type { get; set; }
        public List<string> card_rules { get; set; }
        public string card_delivery_method { get; set; }
        public string card_design { get; set; }
        public CardDeliveryAddress card_delivery_address { get; set; }
        public string card_programme { get; set; }
        public string error { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public List<string> path { get; set; }
        public string type { get; set; }
    }

}
