﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetLedgers
{
    public class RequestGetLegders
    {

        //public RailsBankHandler.LegdersPost.Root RootObject { get; set; }
        public string ledger_id { get; set; }
    }

    //public class ResponseObject
    //{
    //    public string ledger_id { get; set; }
    //    public string error { get; set; }

    //}
    public class LedgerHolder
    {
        public string enduser_id { get; set; }
        public Person person { get; set; }
      
    }

    public class Company
    {
        public string name { get; set; }
    }

    public class Partner
    {
        public string partner_ref { get; set; }
        public Company company { get; set; }
        public string partner_id { get; set; }
    }
    public class LedgerMeta
    {
        public string foo { get; set; }
    }

    public class Person
    {
        public string name { get; set; }
    }

    public class ResponseObject
    {
        public string iban { get; set; }
        public DateTime last_modified_at { get; set; }
        public List<string> ledger_primary_use_types { get; set; }
        public string ledger_id { get; set; }
        public LedgerHolder ledger_holder { get; set; }
        public string credit_details_id { get; set; }
        public string ledger_who_owns_assets { get; set; }
        public string account_number { get; set; }
        public string account { get; set; }
        public string uk_sort_code { get; set; }
        public string partner_ref { get; set; }
        public List<string> missing_data { get; set; }
        public string holder_id { get; set; }
        public string partner_id { get; set; }
        public string ledger_t_and_cs_country_of_jurisdiction { get; set; }
        public string bic_swift { get; set; }
        public string ledger_status { get; set; }
        public string routing_number { get; set; }
        public string amount { get; set; }
        public List<string> failure_reasons { get; set; }
        public DateTime created_at { get; set; }
        public string partner_product { get; set; }
        public Partner partner { get; set; }
        public string ledger_iban_status { get; set; }
        public string asset_type { get; set; }
        public string uk_account_number { get; set; }
        public string asset_class { get; set; }
        public string ledger_type { get; set; }
        public LedgerMeta ledger_meta { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
    }
}
