﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.UpdtBeneficiaryAcct
{
    //public class RequestUpdtBeneficiaryAcct
    //{
        
    //    public RailsBankHandler.UpdtBeneficiaryAcct.Root RootObject { get; set; }
    //    public string beneficiary_id { get; set; }
    //    public string account_id { get; set; }
    //}

    public class ResponseObject
    {
        public string account_id { get; set; }
    }

    public class RequestUpdtBeneficiaryAcct
    {
        public string beneficiary_id { get; set; }
        public string account_id { get; set; }
        public string bank_code_type { get; set; }
        public string bank_country { get; set; }
        public string bic_swift { get; set; }
        public string bank_code { get; set; }
        public string iban { get; set; }
        public string account_number { get; set; }
        public string bank_name { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string error_detail { get; set; }
        public List<string> path { get; set; }
        public string type { get; set; }
    }

    public class UpdtDefaultBenAcctErrorResponse
    {
        public string error { get; set; }
    }

    public class RequestUpdtDefaultBenAcct
    {
        public string beneficiary_id { get; set; }
        public string account_id { get; set; }
    }
}
