﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetCards
{

    public class RequestGetCards
    {
        public string card_id { get; set; }
    }
    public class CardDeliveryAddress
    {
        public string address_number { get; set; }
        public string address_refinement { get; set; }
        public string address_region { get; set; }
        public string address_iso_country { get; set; }
        public string address_street { get; set; }
        public string address_city { get; set; }
        public string address_postal_code { get; set; }
    }

    public class ResponseObject
    {
        public string ledger_id { get; set; }
        public string card_token { get; set; }
        public string name_on_card { get; set; }
        public string card_carrier_type { get; set; }
        public string card_status_reason { get; set; }
        public string card_expiry_date { get; set; }
        public string card_id { get; set; }
        public string card_delivery_name { get; set; }
        public string qr_code_content { get; set; }
        public string card_type { get; set; }
        public List<string> card_rules { get; set; }
        public DateTime created_at { get; set; }
        public string partner_product { get; set; }
        public string card_delivery_method { get; set; }
        public string card_design { get; set; }
        public string truncated_pan { get; set; }
        public CardDeliveryAddress card_delivery_address { get; set; }
        public string card_status { get; set; }
        public string card_programme { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public string field { get; set; }
        public string type { get; set; }
    }

}
