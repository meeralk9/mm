﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.CloseCard
{
    public class RequestGetCards
    {
        public string card_id { get; set; }
    }

    public class ResponseObject
    {
        public string close_reason { get; set; }
        public string error { get; set; }

    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public string field { get; set; }
        public string type { get; set; }
    }


}
