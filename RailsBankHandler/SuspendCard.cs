﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.SuspendCard
{
    //public class RequestSuspendCard
    //{  
    //    public RailsBankHandler.SuspendCard.Root RootObject { get; set; }
    //    public string card_id { get; set; }

    //}

    public class ResponseObject
    {
        public string card_id { get; set; }
        public string error { get; set; }

    }

    public class RequestSuspendCard
    {
        public string card_id { get; set; }
        public string suspend_reason { get; set; }
    }

    public class ErrorResponse
    {
        public string error { get; set; }
        public string detail { get; set; }
        public string field { get; set; }
        public string type { get; set; }
    }

}
