﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.GetBeneficiaryAcct
{
    public class RequestGetBeneficiaryAcct
    {
        
        public string beneficiary_id { get; set; }
        public string account_id { get; set; }
    }

    public class ResponseObject
    {
        public string iban { get; set; }
        public DateTime last_modified_at { get; set; }
        public string account_number { get; set; }
        public string bank_country { get; set; }
        public string bank_code { get; set; }
        public string bank_name { get; set; }
        public string account_type { get; set; }
        public string bic_swift { get; set; }
        public DateTime created_at { get; set; }
        public string beneficiary_id { get; set; }
        public string asset_type { get; set; }
        public string asset_class { get; set; }
        public string account_id { get; set; }
        public string bank_code_type { get; set; }
    }
    public class ErrorResponse
    {
        public string error { get; set; }
    }


}
