﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RailsBankHandler.EndUserPut
{
        //public class RequestEndUsersPut
        //{
        //    public RailsBankHandler.EndUserPut.Root RootObject { get; set; }
        //    public string enduser_id { get; set; }
        //}
    public class RequestEndUsersPut
    {
        public string enduser_id { get; set; }
        public Person person { get; set; }
        public EnduserMeta enduser_meta { get; set; }
    }

    public class ResponseObject
        {
            public string enduser_id { get; set; }
            public string error { get; set; }
        }

        public class Income
        {
            public string frequency { get; set; }
            public string currency { get; set; }
            public string amount { get; set; }
        }

        public class AddressHistory
        {

            public string address_end_date { get; set; }
            public string address_start_date { get; set; }
            public string address_iso_country { get; set; }
        }
        public class Address
        {
            public string address_region { get; set; }
            public string address_iso_country { get; set; }
            public string address_number { get; set; }
            public string address_postal_code { get; set; }
            public string address_refinement { get; set; }
            public string address_street { get; set; }
            public string address_city { get; set; }
        }

        public class Person
        {
            public List<string> country_of_residence { get; set; }
            public string document_number { get; set; }
            public string document_type { get; set; }
            public Income income { get; set; }
            //public string pep_type { get; set; }
            //public string pep_notes { get; set; }
            public List<AddressHistory> address_history { get; set; }
            //public bool pep { get; set; }
            public string date_onboarded { get; set; }
            public string email { get; set; }
            public string tin { get; set; }
            public string document_issue_date { get; set; }
            public string name { get; set; }
            public Address address { get; set; }
            public string social_security_number { get; set; }
            public string telephone { get; set; }
            public string date_of_birth { get; set; }
            public string document_expiration_date { get; set; }
            public string tin_type { get; set; }
            public List<string> citizenship { get; set; }
            public List<string> nationality { get; set; }
            public string document_issuer { get; set; }
            public string country_of_birth { get; set; }
        }

        public class EnduserMeta
        {
            public string foo { get; set; }
        }
    }