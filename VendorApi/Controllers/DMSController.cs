﻿using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VendorApi.Helper;
using Swashbuckle.AspNetCore.Annotations;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using System.Net.Http;
using DBHandler.Model;
using System.Collections.Generic;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DMSController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private readonly IDocumentsRepository _documentsRepository;


        public DMSController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, IDocumentsRepository DocumentsRepository)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
            _documentsRepository = DocumentsRepository;
        }

        [Route("CCMGenerateDocument")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.Email.RootObject>", typeof(ActiveResponseSucces<DMSHandler.CCMGenerateDocument.ResponseObject>))]
        public async Task<IActionResult> CCMGenerateDocument([FromBody] DMSHandler.CCMGenerateDocument.CCMGenerateDocumentModel Model)
        {
            ActiveResponseSucces<DMSHandler.CCMGenerateDocument.ResponseObject> response = new ActiveResponseSucces<DMSHandler.CCMGenerateDocument.ResponseObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["DMS:BaseUrl"].ToString(),
                                                                        _config["DMS:CCMGenerateDocument"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            DMSHandler.CCMGenerateDocument.ResponseObject resp = new DMSHandler.CCMGenerateDocument.ResponseObject();
            resp.base64EncodedDoc = "/9j/4AAQSkZJRgABAQEAyADIAAD/4SfgRXhpZgAATU0AKgAAAAgABgALAAIAAAAmAAAIYgESAAMAAAABAAEAAAExAAIAAAAmAAAIiAEyAAIAAAAUAAAIrodpAAQAAAABAAAIwuocAAcAAAgMAAAAVgAA";
            resp.documentType = "doc";
            resp.fileName = "test.doc";
            response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

            response.Content = resp;
            return Ok(response);
        }

        [Route("DMSSaveDocument")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DMSSaveDocument([FromBody] DMSHandler.DMSSaveDocument.DMSSaveDocumentModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["DMS:BaseUrl"].ToString(),
                                                                        _config["DMS:DMSSaveDocument"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            ErrorMessages error = _documentsRepository.DMSSaveDocument(Model.RootObject.Document, Model.RootObject.metadata, Model.SignOnRq);
            response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
            response.Content = error.obj.ToString();
            return Ok(response);
        }


        [Route("ReadDocument")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ReadDocument([FromBody] DMSHandler.DMSReadDocument.DMSReadDocumentModel Model)
        {
            ActiveResponseSucces<List<DMSHandler.DMSSaveDocument.RootObject>> response = new ActiveResponseSucces<List<DMSHandler.DMSSaveDocument.RootObject>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["DMS:BaseUrl"].ToString(),
                                                                        _config["DMS:ReadDocument"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            ErrorMessages error = _documentsRepository.ReadDocument(Model.RootObject.documentId, Model.SignOnRq);
            if (error.isError)
            {
                response.Status = new Status { Severity = Severity.Error, Code = error.ErrorCode, StatusMessage = error.ErrorMessage};
                response.Content = null;
            }
            else
            {
                response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                response.Content = (List<DMSHandler.DMSSaveDocument.RootObject>)error.obj;
            }
            return Ok(response);
        }
    }
}