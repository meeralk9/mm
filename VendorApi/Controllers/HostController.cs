﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HostController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;

        private Exception excetionForLog;

        public HostController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }
        [Route("GetWorkingDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetWorkingDate([FromBody] SignOnRq Model)
        {
            ActiveResponseSucces<DateTime> response = new ActiveResponseSucces<DateTime>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:GetWorkingDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DateTime>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CashWithDrawalTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CashWithDrawalTrx([FromBody] CashWithDrawalDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.CashWithDrawalResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.CashWithDrawalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:CashWithDrawalTrx"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.CashWithDrawalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("PreAuthTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> PreAuthTrx([FromBody] PreAuthorizationDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.PreAuthResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.PreAuthResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:PreAuthTrx"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.PreAuthResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("PurchaseRequestTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> PurchaseRequestTrx([FromBody] PurchaseRequestDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.PurchaseRequestTrxResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.PurchaseRequestTrxResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:PurchaseRequestTrx"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.PurchaseRequestTrxResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("ATM_FullReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> ATM_FullReversal([FromBody] FullReversal Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.FullReversalResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.FullReversalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:ATM_FullReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.FullReversalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("ATM_PartialReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> ATM_PartialReversal([FromBody] PartialReversal Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.PartialReversalResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.PartialReversalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:ATM_PartialReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.PartialReversalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("BalanceEnquiryTrx")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> BalanceEnquiryTrx([FromBody] BalanceEnquiryDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.BalanceEnquiryResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.BalanceEnquiryResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:BalanceEnquiryTrx"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.BalanceEnquiryResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("AccountVerification")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> AccountVerification([FromBody] AccountVerificationDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.AccountVerificationResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.AccountVerificationResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:AccountVerification"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.AccountVerificationResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("CreditTransaction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CreditTransaction([FromBody] CreditTransactionDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.CreditTransactionResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.CreditTransactionResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:CreditTransaction"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.CreditTransactionResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("CheckBODEOD")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CheckBODEOD([FromBody] GenericHostDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.Host.CheckBODEODResponse> response = new ActiveResponseSucces<DBHandler.Model.Host.CheckBODEODResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:CheckBODEOD"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Host.CheckBODEODResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        [Route("CheckBalanceUploaded")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> CheckBalanceUploaded([FromBody] GenericHostDto Model)
        {
            ActiveResponseSucces<bool?> response = new ActiveResponseSucces<bool?>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["Host:BaseUrl"].ToString(),
                                                                        _config["Host:CheckBalanceUploaded"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool?>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
    }
}