﻿using CoreHandler.Model;
using DBHandler.Enum;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBHandler.Helper;
using HttpHandler;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using CoreHandler;
using CommonModel;
using System.Text;

namespace VendorApi.Helper
{
    public class APIRequestHandler
    {
        public static string Create16DigitString()
        {
            Random RNG = new Random();

            var builder = new StringBuilder();
            while (builder.Length < 16)
            {
                builder.Append(RNG.Next(10).ToString());
            }
            return builder.ToString();
        }

        public static async Task<InternalResult> GetResponse(DateTime starTime,
                                                              string responseCode,
                                                              string responseText,
                                                               object Response,
                                                               SignOnRq SignOnRq,
                                                               object Model,
                                                               Level level,
                                                               ILogRepository _logs,
                                                               IUserChannelsRepository _userChannels,
                                                               ControllerBase controllerBase,
                                                               IConfiguration _config)
        {
            Exception excetionForLog = null;
            DBHandler.Helper.Status status = new DBHandler.Helper.Status() { Code = responseCode, StatusMessage = responseText, Severity = responseCode == "00" ? Severity.Success : Severity.Error };
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = controllerBase.Request.HttpContext;
            var id = controllerBase.HttpContext.User.Claims.First().Value;
            RequestResponse httpResult = null;

            try
            {
                bool isModelValid = controllerBase.TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, SignOnRq?.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        var errors = controllerBase.ModelState
                       .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
                        //ErrorMessages mes = _logs.GetErrorMessage("ERROR-02");
                        string errormessage = "";
                        foreach (var e in errors)
                        {
                            errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + Environment.NewLine;
                        }
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };
                    }
                    else
                    {
                        ErrorMessages mes = _logs.GetErrorMessage("ERROR-01");
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                    }

                    level = Level.Error;
                    excetionForLog = new Exception(status.StatusMessage);
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                ErrorMessages mes = _logs.GetErrorMessage("ERROR-03");
                status = new DBHandler.Helper.Status { Severity = Severity.Exception, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                excetionForLog = ex;
            }

            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Response);
            _logs.Logs(level, status.Code != null ? status.Code.ToString() : "",
                        controllerBase.Request.HttpContext,
                        _config,
                        excetionForLog,
                        starTime,
                        DateTime.Now,
                        res,
                        req,
                        id,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.ChannelId,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.LogId,
                        (Model == null || SignOnRq == null) ? DateTime.Now : SignOnRq.DateTime);
            return new InternalResult() { Status = status, httpResult = httpResult };
        }

        public static async Task<InternalResult> GetResponse(  SignOnRq SignOnRq, 
                                                               object Model, 
                                                               string baseurl, 
                                                               string url,
                                                               Level level,
                                                               ILogRepository _logs,
                                                               IUserChannelsRepository _userChannels,
                                                               ControllerBase controllerBase,
                                                               IConfiguration _config,
                                                               HttpHandler.HttpHandler httpHandler,
                                                               int type = 1,
                                                               int isGet = 1,
                                                               string newBody ="")
        {
            Exception excetionForLog= null;
            DBHandler.Helper.Status status = new DBHandler.Helper.Status();
            DateTime starTime = DateTime.Now;
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = controllerBase.Request.HttpContext;
            var id = controllerBase.HttpContext.User.Claims.First().Value;
            RequestResponse httpResult = null;
            string body = "";

            try
            {
                bool isModelValid = controllerBase.TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, SignOnRq?.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        var errors = controllerBase.ModelState
                       .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
                        //ErrorMessages mes = _logs.GetErrorMessage("ERROR-02");
                        string errormessage = "";
                        foreach(var e  in errors)
                        {
                            errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + ",  ";
                        }
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };
                    }
                    else
                    {
                        ErrorMessages mes = _logs.GetErrorMessage("ERROR-01");
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                    }

                    level = Level.Error;
                    excetionForLog = new Exception(status.StatusMessage);
                }
                else
                {
                    Dictionary<string, string> header = null;
                    if (type == 2)
                    {
                        header = Common.GetHeaderForECSFin();
                    }
                    else if (type == 3)
                    {
                        header = Common.GetHeaderForIMTF();
                    }
                    else if (type == 4)
                    {
                        header = Common.GetHeaderForERCCorporate();
                    }
                    else if (type == 5)
                    {
                        header = Common.GetHeaderForRetail();
                    }
                    else if (type == 6)
                    {
                        header = Common.GetHeaderForShipa();
                    }
                    else
                    {
                        header = Common.GetHeader();
                    }

                    body = JsonConvert.SerializeObject(Model);
                    body = string.IsNullOrEmpty(newBody) ? body : newBody;
                   
                    if (isGet == 2)
                    {
                        httpResult = await httpHandler.GetJsonData(baseurl, url, header).ConfigureAwait(false);
                    }
                    else if (isGet == 3)
                    {
                        httpResult = await httpHandler.PutJsonData(baseurl, url, header, body).ConfigureAwait(false);
                    }
                    else
                    {
                        httpResult = await httpHandler.PostJsonData(baseurl, url, header, body).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                ErrorMessages mes = _logs.GetErrorMessage("ERROR-03");
                status = new DBHandler.Helper.Status { Severity = Severity.Exception, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                excetionForLog = ex;
            }

            string severity = httpResult != null ? httpResult.severity : "";
            string req = httpResult == null ? JsonConvert.SerializeObject(status) : httpResult.httpResponse;
            _logs.Logs(level, status.Code != null ? status.Code.ToString() : severity,
                        controllerBase.Request.HttpContext,
                        _config,
                        excetionForLog,
                        starTime,
                        DateTime.Now,
                        body,
                        req,
                        id,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.ChannelId,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.LogId,
                        (Model == null || SignOnRq == null) ? DateTime.Now : SignOnRq.DateTime);
            return new InternalResult() { Status = status, httpResult = httpResult };
        }

        public static async Task<InternalResult> GetResponseX(SignOnRq SignOnRq,
                                                              object Model,
                                                              string baseurl,
                                                              string url,
                                                              Level level,
                                                              ILogRepository _logs,
                                                              IUserChannelsRepository _userChannels,
                                                              ControllerBase controllerBase,
                                                              IConfiguration _config,
                                                              HttpHandler.HttpHandler httpHandler,
                                                              Dictionary<string, string> headers,
                                                              Dictionary<string, string> body,
                                                              int isJson = 1)
        {
            Exception excetionForLog = null;
            DBHandler.Helper.Status status = new DBHandler.Helper.Status();
            DateTime starTime = DateTime.Now;
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = controllerBase.Request.HttpContext;
            var id = controllerBase.HttpContext.User.Claims.First().Value;
            RequestResponse httpResult = null;
            try
            {
                bool isModelValid = controllerBase.TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        var errors = controllerBase.ModelState
                       .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
                        //ErrorMessages mes = _logs.GetErrorMessage("ERROR-02");
                        string errormessage = "";
                        foreach (var e in errors)
                        {
                            errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + Environment.NewLine;
                        }
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };
                    }
                    else
                    {
                        ErrorMessages mes = _logs.GetErrorMessage("ERROR-01");
                        status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                    }

                    level = Level.Error;
                    excetionForLog = new Exception(status.StatusMessage);
                }
                else
                {
                    if (isJson==2)
                    {
                        var b = JsonConvert.SerializeObject(Model);
                        httpResult = await httpHandler.PostJsonData(baseurl, url, headers, b).ConfigureAwait(false);
                       
                    }
                    else if (isJson == 3)
                    {
                        var b = JsonConvert.SerializeObject(Model);
                        httpResult = await httpHandler.GetJsonData(baseurl, url, headers).ConfigureAwait(false);

                    }
                    else
                    {
                        httpResult = await httpHandler.PostFormData(baseurl, url, headers, body).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                ErrorMessages mes = _logs.GetErrorMessage("ERROR-03");
                status = new DBHandler.Helper.Status { Severity = Severity.Exception, StatusMessage = mes.ErrorMessage, Code = mes.ErrorCode };
                excetionForLog = ex;
            }
            string severity = httpResult != null ? httpResult.severity : "";
            string res = JsonConvert.SerializeObject(Model);
            string req = httpResult == null ? JsonConvert.SerializeObject(status) : httpResult.httpResponse;
            _logs.Logs(level, status.Code != null ? status.Code.ToString() : severity,
                        controllerBase.Request.HttpContext,
                        _config,
                        excetionForLog,
                        starTime,
                        DateTime.Now,
                        res,
                        req,
                        id,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.ChannelId,
                        (Model == null || SignOnRq == null) ? "" : SignOnRq.LogId,
                        (Model == null || SignOnRq == null) ? DateTime.Now : SignOnRq.DateTime);
            return new InternalResult() { Status = status, httpResult = httpResult };
        }
    }
}
