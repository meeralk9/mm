﻿using DBHandler.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace VendorApi.MyAuth
{
    public class AuthenticationMiddleWare
    {
        private readonly RequestDelegate _next;
        public AuthenticationMiddleWare(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, UserManager<ApplicationUser> _userManager, SignInManager<ApplicationUser> _signInManager)
        {
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                System.Text.Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');

                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    context.Response.StatusCode = 400;
                    //context.Response.ContentType = @"application/json";
                    //await context.Response.WriteAsync("Invalid User Name");
                    return;

                }
                DateTime nowTime = DateTime.Now;
                if (user.LockoutEnd != null)
                {
                    DateTime? lockTime = DateTime.Parse(user.LockoutEnd.ToString());

                    if (lockTime > nowTime)
                    {
                        context.Response.StatusCode = 400;
                        context.Response.ContentType = @"application/json";
                        await context.Response.WriteAsync($"Account is Lockout until {user.LockoutEnd.Value}");
                        return;
                    }
                }
                var _password = user.Id + password;
                var result = await _signInManager.CheckPasswordSignInAsync(user, password, true);
                if (result.Succeeded)
                {
                    await _next.Invoke(context);
                }

                else
                {
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }
            }
            else
            {
                // no authorization header
                context.Response.StatusCode = 401; //Unauthorized
                return;
            }
        }
    }
}


