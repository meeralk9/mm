﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RailsBankHandler.EndUsersPost;
using RailsBankHandler.CardPost;
using RailsBankHandler.EndUserPut;
using RailsBankHandler.GetEndUser;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;
using RailsBankHandler.LegdersPost;
using Swashbuckle.AspNetCore.Filters;
using DigiBancMiddleware.Swagger.Request.GetBeneficiariesRequestExample;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RailsBankController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public RailsBankController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("AddEndUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>), statusCode: 201)]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.EndUsersPost.ErrorResponse>), statusCode: 400)]
        public async Task<IActionResult> AddEndUser([FromBody] RequestEndUsersPost Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:AddEndUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }
    
        [Route("UpdateEndUsers")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPut]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.EndUsersPost.ResponseObj>), statusCode: 200)]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.EndUsersPost.ErrorResponse>), statusCode: 400)]
        public async Task<IActionResult> UpdateEndUsers([FromBody] RailsBankHandler.EndUserPut.RequestEndUsersPut Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:UpdateEndUsers"].ToString().Replace("{0}",Model.enduser_id),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20, 3);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        //[Route("GetEndUser")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetEndUser.Root>", typeof(ActiveResponseSucces<RailsBankHandler.GetEndUser.ResponseObject>))]
        //public async Task<IActionResult> GetEndUser([FromBody] RailsBankHandler.GetEndUser.RequestGetEndUsers Model)
        //{
        //    ActiveResponseSucces<RailsBankHandler.GetEndUser.ResponseObject> response = new ActiveResponseSucces<RailsBankHandler.GetEndUser.ResponseObject>();
        //    InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
        //                                                                    _config["RailsBankAPI:BaseUrl"].ToString(),
        //                                                                    _config["RailsBankAPI:GetEndUser"].ToString().Replace("{0}", Model.clientID),
        //                                                                    level,
        //                                                                    _logs,
        //                                                                    _userChannels,
        //                                                                    this,
        //                                                                    _config,
        //                                                                    httpHandler, 20, 2);
        //    response.RequestDateTime = Model.SignOnRq.DateTime;
        //    response.LogId = Model.SignOnRq.LogId;
        //    var obj = JsonConvert.DeserializeObject<RailsBankHandler.GetEndUser.ResponseObject>(result.httpResult.httpResponse);
        //    response.Content = obj;
        //    if (result.Status.Code == null)
        //    {
        //        if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
        //        {
        //            response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
        //            return Ok(response);
        //        }

        //    }
        //    response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
        //    return BadRequest(response);
        //}

        /// <summary>
        /// End User Wait
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        clientID : 6023f9f1-8c0c-43a6-a7dd-0ac195024096
        ///     
        ///
        /// </remarks>
        [Route("EndUserWait")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.End.Root>", typeof(ActiveResponseSucces<RailsBankHandler.End.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>))]
        public async Task<IActionResult> EndUserWait([FromQuery] RailsBankHandler.End.RequestEnd Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:EndUserWait"].ToString().Replace("{0}", Model.clientID),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok( response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("CreateLedger")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.LegdersPost.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.LegdersPost.ErrorResponse>))]
        public async Task<IActionResult> CreateLedger([FromBody] RequestLegdersPost Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:CreateLedger"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get Ledger Details
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        ledger_id : 60238bb5-901d-487a-8787-364405bca218
        ///     
        ///
        /// </remarks>
        [Route("GetLedgerDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetLedgers.Root>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgers.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetLedgers.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgers.ErrorResponse>))]
        public async Task<IActionResult> GetLedgerDetails([FromQuery] RailsBankHandler.GetLedgers.RequestGetLegders Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetLedgerDetails"].ToString().Replace("{0}", Model.ledger_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {                   
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }


        
        [Route("CreateCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "RailsBankHandler.CardPost.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.CardPost.ResponseObject>))]
        [SwaggerResponse(400, "RailsBankHandler.CardPost.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.CardPost.ErrorResponse>))]
        public async Task<IActionResult> CreateCard([FromBody] RequestPostCard Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:CreateCard"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object> (result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get Card Details
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        card_id : 60238bb5-901d-487a-8787-364405bca218
        ///     
        ///
        /// </remarks>
        [Route("GetCardDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetCards.Root>", typeof(ActiveResponseSucces<RailsBankHandler.GetCards.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetCards.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetCards.ErrorResponse>))]
        public async Task<IActionResult> GetCardDetails([FromQuery] RailsBankHandler.GetCards.RequestGetCards Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetCardDetails"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {        
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("ActivateCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.ActivateCard.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.ActivateCard.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.ActivateCard.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.ActivateCard.ErrorResponse>))]
        public async Task<IActionResult> ActivateCard([FromBody] RailsBankHandler.ActivateCard.RequestActivateCard Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:ActivateCard"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Get Ledger Details Wait
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        ledger_id : 60238bb5-901d-487a-8787-364405bca218
        ///     
        ///
        /// </remarks>
        [Route("GetLedgerDetailsWait")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetLedgersWait.Root>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgersWait.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetLedgersWait.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgersWait.ErrorResponse>))]
        public async Task<IActionResult> GetLedgerDetailsWait([FromQuery] RailsBankHandler.GetLedgersWait.RequestGetLegdersWait Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetLedgerDetailsWait"].ToString().Replace("{0}", Model.ledger_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    var data = JsonConvert.DeserializeObject<RailsBankHandler.GetLedgersWait.ResponseObject>(result.httpResult.httpResponse);
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    response.Content = data;
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("CloseLedger")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgersClose.ErrorResponse>))]
        public async Task<IActionResult> CloseLedger([FromBody] RailsBankHandler.GetLedgersClose.RequestGetLedgersClose Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:CloseLedger"].ToString().Replace("{0}", Model.ledger_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }


        /// <summary>
        /// Get PIN
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        card_id : 60238bb5-901d-487a-8787-364405bca218
        ///     
        ///
        /// </remarks>
        [Route("GetPIN")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "RailsBankHandler.GetPIN.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.GetPIN.ResponseObject>))]
        [SwaggerResponse(400, "RailsBankHandler.GetPIN.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetPIN.ErrorResponse>))]
        public async Task<IActionResult> GetPIN([FromQuery] RailsBankHandler.GetPIN.RequestGetPIN Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetPIN"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("CloseCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "RailsBankHandler.CloseCard.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.CloseCard.ResponseObject>))]
        [SwaggerResponse(400, "RailsBankHandler.CloseCard.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.CloseCard.ErrorResponse>))]
        public async Task<IActionResult> CloseCard([FromBody] RailsBankHandler.CloseCard.RequestGetCards Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:CloseCard"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("ResetPIN")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "RailsBankHandler.ResetPIN.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.ResetPIN.ResponseObject>))]
        [SwaggerResponse(400, "RailsBankHandler.ResetPIN.ResponseObject.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.ResetPIN.ErrorResponse>))]
        public async Task<IActionResult> ResetPIN([FromBody] RailsBankHandler.ResetPIN.RequestResetPIN Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:ResetPIN"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("SuspendCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "RailsBankHandler.ResetPIN.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.SuspendCard.ResponseObject>))]
        [SwaggerResponse(400, "RailsBankHandler.ResetPIN.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.SuspendCard.ErrorResponse>))]
        public async Task<IActionResult> SuspendCard([FromBody] RailsBankHandler.SuspendCard.RequestSuspendCard Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:SuspendCard"].ToString().Replace("{0}", Model.card_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Get End User
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        clientID : 6023f9f1-8c0c-43a6-a7dd-0ac195024096
        ///     
        ///
        /// </remarks>
        [Route("GetEndUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.End.Root>", typeof(ActiveResponseSucces<RailsBankHandler.End.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.End.ErrorResponse>))]
        public async Task<IActionResult> GetEndUser([FromQuery] RailsBankHandler.End.RequestEnd Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetEndUser"].ToString().Replace("{0}", Model.clientID),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Get Ledger by AccNum
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        holder_id: null,
        ///        partner_product : null,
        ///        account_number : null,
        ///        items_per_page: 1,
        ///        offset: 1,
        ///        from_date: null,
        ///        to_date: null,
        ///        order : null
        ///     
        ///
        /// </remarks>
        [Route("GetLedgerbyAccNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        //[SwaggerResponse(200, "ActiveResponseSucces<List<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject>>", typeof(ActiveResponseSucces<List<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject>>))]
        //[SwaggerResponse(400, "ActiveResponseSucces<List<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse>>", typeof(ActiveResponseSucces<List<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse>>))]

        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject>), statusCode: 200)]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ErrorResponse>), statusCode: 400)]
        public async Task<IActionResult> GetLedgerbyAccNum([FromQuery] RailsBankHandler.GetLedgerbyAcctNum.RequestLedgerbyAcctNum Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                             string.Format(_config["RailsBankAPI:GetLedgerbyAccNum"].ToString(), Model.items_per_page,
                                                                             Model.offset)+ (string.IsNullOrEmpty(Model.holder_id)?"" : "&holder_id=" + Model.holder_id)
                                                                             + (string.IsNullOrEmpty(Model.partner_product)?"": "&partner_product=" + Model.partner_product)
                                                                             + (string.IsNullOrEmpty(Model.account_number) ? "" : "&account_number=" + Model.account_number)
                                                                             + (string.IsNullOrEmpty(Model.from_date) ? "" : "&from_date=" + Model.from_date)
                                                                             + (string.IsNullOrEmpty(Model.to_date) ? "" : "&to_date=" + Model.to_date)
                                                                             + (string.IsNullOrEmpty(Model.order) ? "" : "&order=" + Model.order),                     

                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var data = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = data;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("AddBeneficiary")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ResponseObj>", typeof(ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ResponseObj>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.BeneficiariesPost.ErrorResponse>))]
        public async Task<IActionResult> AddBeneficiary([FromBody] RailsBankHandler.BeneficiariesPost.RequestBeneficiariessPost Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:AddBeneficiary"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get Beneficiaries.
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        holder_id: null,
        ///        items_per_page: 2,
        ///        offset: 2,
        ///        from_date: null,
        ///        to_date: null,
        ///        order : null
        ///     
        ///
        /// </remarks>   
        [Route("GetBeneficiaries")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ResponseObject>), statusCode: 200)]
        [ProducesResponseType(typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaries.ErrorResponse>), statusCode: 400)]
        public async Task<IActionResult> GetBeneficiaries([FromQuery] RailsBankHandler.GetBeneficiaries.RequestGetBeneficiaries Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                             string.Format(_config["RailsBankAPI:GetBeneficiaries"].ToString(), Model.items_per_page,
                                                                             Model.offset) + (string.IsNullOrEmpty(Model.holder_id) ? "" : "&holder_id=" + Model.holder_id)
                                                                             + (string.IsNullOrEmpty(Model.from_date) ? "" : "&from_date=" + Model.from_date)
                                                                             + (string.IsNullOrEmpty(Model.to_date) ? "" : "&to_date=" + Model.to_date)
                                                                             + (string.IsNullOrEmpty(Model.order) ? "" : "&order=" + Model.order),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var data = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = data;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("UpdateBeneficiary")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPut]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ResponseObj>", typeof(ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ResponseObj>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.UpdateBeneficiary.ErrorResponse>))]

        public async Task<IActionResult> UpdateBeneficiary([FromBody] RailsBankHandler.UpdateBeneficiary.RequestUpdateBeneficiary Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse( null , Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:UpdateBeneficiary"].ToString().Replace("{0}", Model.beneficiary_id),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20, 3);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);

                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get Beneficiary Dets
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        beneficiary_id: 602911b8-76e9-4ed3-9ae5-02bb92765734
        ///     
        ///
        /// </remarks>
        [Route("GetBeneficiaryDets")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaryDets.ErrorResponse>))]
        public async Task<IActionResult> GetBeneficiaryDets([FromQuery] RailsBankHandler.GetBeneficiaryDets.RequestGetBeneficiaryDets Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetBeneficiaryDets"].ToString().Replace("{0}", Model.beneficiary_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("AddBeneficiaryAcct")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.AddBeneficiaryAcct.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.AddBeneficiaryAcct.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.AddBeneficiaryAcct.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.AddBeneficiaryAcct.ErrorResponse>))]
        public async Task<IActionResult> AddBeneficiaryAcct([FromBody] RailsBankHandler.AddBeneficiaryAcct.RequestAddBeneficiaryAcct Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:AddBeneficiaryAcct"].ToString().Replace("{0}", Model.beneficiary_id),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null, response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get All Beneficiary Accts
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        beneficiary_id: 602911b8-76e9-4ed3-9ae5-02bb92765734
        ///     
        ///
        /// </remarks>
        [Route("GetAllBeneficiaryAccts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ErrorResponse>))]
        public async Task<IActionResult> GetAllBeneficiaryAccts([FromQuery] RailsBankHandler.GetAllBeneficiaryAccts.RequestGetAllBeneficiaryAccts Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse( null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetAllBeneficiaryAccts"].ToString().Replace("{0}", Model.beneficiary_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Get Beneficiary Accts
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        beneficiary_id: 602911b8-76e9-4ed3-9ae5-02bb92765734,
        ///        account_id : 602911b8-00c2-494d-b242-422e71798054
        ///     
        ///
        /// </remarks>
        [Route("GetBeneficiaryAcct")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.GetBeneficiaryAcct.ErrorResponse>))]
        public async Task<IActionResult> GetBeneficiaryAcct([FromQuery] RailsBankHandler.GetBeneficiaryAcct.RequestGetBeneficiaryAcct Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null , Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetBeneficiaryAcct"].ToString().Replace("{0}", Model.beneficiary_id).Replace("{1}",Model.account_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        
        [Route("UpdtBeneficiaryAcct")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPut]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ErrorResponse>))]
        public async Task<IActionResult> UpdtBeneficiaryAcct([FromBody] RailsBankHandler.UpdtBeneficiaryAcct.RequestUpdtBeneficiaryAcct Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:UpdtBeneficiaryAcct"].ToString().Replace("{0}", Model.beneficiary_id).Replace("{1}", Model.account_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 3);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }


        
        [Route("UpdtDefaultBenAcct")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPut]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject>", typeof(ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse>))]
        public async Task<IActionResult> UpdtDefaultBenAcct([FromBody] RailsBankHandler.UpdtBeneficiaryAcct.RequestUpdtDefaultBenAcct Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null , Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:UpdtDefaultBenAcct"].ToString().Replace("{0}", Model.beneficiary_id).Replace("{1}", Model.account_id),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 3);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {

                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }

            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        /// <summary>
        /// Get Ledger Entries
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        ledger_id: 602911b8-76e9-4ed3-9ae5-02bb92765734,       
        ///        items_per_page: 2,
        ///        offset: 2,
        ///        from_date: null,
        ///        to_date: null,
        ///        order : null
        ///        ledger_entry_event_type : null
        ///     
        ///
        /// </remarks>
        [Route("GetLedgerEntries")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ResponseGetLedgerEntries>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ResponseGetLedgerEntries>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ErrorResponseGetLedgerEntries>", typeof(ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ErrorResponseGetLedgerEntries>))]
        public async Task<IActionResult> GetLedgerEntries([FromQuery] RailsBankHandler.GetLedgerEntries.RequestGetLedgerEntries Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                            _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                            _config["RailsBankAPI:GetLedgerEntries"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 20, 2);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return Ok(response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }

        //WebHook
        [Route("Webhook")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<RailsBankHandler.Webhook.WebhookResponse>", typeof(ActiveResponseSucces<RailsBankHandler.Webhook.WebhookResponse>))]
        [SwaggerResponse(400, "ActiveResponseSucces<RailsBankHandler.Webhook.WebhookErrorResponse>", typeof(ActiveResponseSucces<RailsBankHandler.Webhook.WebhookErrorResponse>))]
        public async Task<IActionResult> Webhook([FromBody] RailsBankHandler.Webhook.WebhookRequest Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model, _config["RailsBankAPI:BaseUrl"].ToString(),
                                                                        _config["RailsBankAPI:Webhook"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 20);
            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.Status.Code == null)
            {
                if (result != null && result.httpResult?.StatusCode == HttpStatusCode.OK)
                {
                    response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                    return CreatedAtAction(null , response);
                }
            }
            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "" };
            return BadRequest(response);
        }
    }
}