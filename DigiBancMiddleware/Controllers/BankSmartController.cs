﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;
//using RuleEngineHandler;
using DBHandler.CreditProgramSearchByCIFModel;
using DBHandler.CreditProgramUpdateModel;
using DBHandler.PostChargesModel;
using DBHandler.Model.Loan;
using DBHandler.Model.Gurantee;
using System.Globalization;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        #region Fahad Functions

        [Route("BalanceInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquiry([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;

            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse( null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler,19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (Model.SignOnRq.ChannelId == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Name = response.Content.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Name, specialChar) : null;
                        response.Content.ProductType = response.Content.ProductType != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductType, specialChar) : null;
                        response.Content.ProductDesc = response.Content.ProductDesc != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductDesc, specialChar) : null;
                    }

                    return CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateCIFDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>", typeof(ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>))]
        public async Task<IActionResult> GetCorporateCIFDetail([FromBody] RimInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> response = new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateCIFDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp")?.Value.ToString();
                    if (reaons != null)
                        channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    if (response.Content.Cust.KYCReviewDateCorp != null)
                    {
                        DateTime d = Convert.ToDateTime(response.Content.Cust.KYCReviewDateCorp);
                        response.Content.Cust.KYCReviewDateCorp = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
                    }

                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (Model.SignOnRq.ChannelId == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Cust.Name = response.Content.Cust.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Name, specialChar) : null;
                        response.Content.Cust.RManager = response.Content.Cust.RManager != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RManager, specialChar) : null;
                        response.Content.Cust.EntityNameCorp = response.Content.Cust.EntityNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityNameCorp, specialChar) : null;
                        response.Content.Cust.EntityTypeCorp = response.Content.Cust.EntityTypeCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityTypeCorp, specialChar) : null;
                        response.Content.Cust.RegisteredAddressCorp = response.Content.Cust.RegisteredAddressCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegisteredAddressCorp, specialChar) : null;
                        response.Content.Cust.ContactName1Corp = response.Content.Cust.ContactName1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName1Corp, specialChar) : null;
                        response.Content.Cust.ContactID1Corp = response.Content.Cust.ContactID1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID1Corp, specialChar) : null;
                        response.Content.Cust.Address1Corp = response.Content.Cust.Address1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address1Corp, specialChar) : null;
                        response.Content.Cust.ContactName2Corp = response.Content.Cust.ContactName2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName2Corp, specialChar) : null;
                        response.Content.Cust.ContactID2Corp = response.Content.Cust.ContactID2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID2Corp, specialChar) : null;
                        response.Content.Cust.Address2Corp = response.Content.Cust.Address2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address2Corp, specialChar) : null;
                        response.Content.Cust.BusinessActivity = response.Content.Cust.BusinessActivity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.BusinessActivity, specialChar) : null;
                        response.Content.Cust.TradeLicenseNumberCorp = response.Content.Cust.TradeLicenseNumberCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseNumberCorp, specialChar) : null;
                        response.Content.Cust.CountryofIncorporationCorp = response.Content.Cust.CountryofIncorporationCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.CountryofIncorporationCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.SponsorNameCorp = response.Content.Cust.SponsorNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.SponsorNameCorp, specialChar) : null;
                        response.Content.Cust.NFFECorp = response.Content.Cust.NFFECorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.NFFECorp, specialChar) : null;
                        response.Content.Cust.RegAddCityCorp = response.Content.Cust.RegAddCityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCityCorp, specialChar) : null;
                        response.Content.Cust.RegAddCountryCorp = response.Content.Cust.RegAddCountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCountryCorp, specialChar) : null;
                        response.Content.Cust.RegAddStateCorp = response.Content.Cust.RegAddStateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddStateCorp, specialChar) : null;
                        response.Content.Cust.Add1CountryCorp = response.Content.Cust.Add1CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CountryCorp, specialChar) : null;
                        response.Content.Cust.Add1StateCorp = response.Content.Cust.Add1StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1StateCorp, specialChar) : null;
                        response.Content.Cust.Add1CityCorp = response.Content.Cust.Add1CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CityCorp, specialChar) : null;
                        response.Content.Cust.Add2CountryCorp = response.Content.Cust.Add2CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CountryCorp, specialChar) : null;
                        response.Content.Cust.Add2StateCorp = response.Content.Cust.Add2StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2StateCorp, specialChar) : null;
                        response.Content.Cust.Add2CityCorp = response.Content.Cust.Add2CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;

                        for (int i = 0; i < response.Content.ShareHolders.Count; i++)
                        {
                            response.Content.ShareHolders[i].ShareHolderName = response.Content.ShareHolders[i].ShareHolderName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderName, specialChar) : null;
                            response.Content.ShareHolders[i].ShareHolderAddress = response.Content.ShareHolders[i].ShareHolderAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderAddress, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofIncorporation = response.Content.ShareHolders[i].CountryofIncorporation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofIncorporation, specialChar) : null;
                            response.Content.ShareHolders[i].AuthorisedSignatory = response.Content.ShareHolders[i].AuthorisedSignatory != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].AuthorisedSignatory, specialChar) : null;
                            response.Content.ShareHolders[i].BeneficialOwner = response.Content.ShareHolders[i].BeneficialOwner != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].BeneficialOwner, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofResidence = response.Content.ShareHolders[i].CountryofResidence != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofResidence, specialChar) : null;
                            response.Content.ShareHolders[i].SHCity = response.Content.ShareHolders[i].SHCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCity, specialChar) : null;
                            response.Content.ShareHolders[i].SHState = response.Content.ShareHolders[i].SHState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHState, specialChar) : null;
                            response.Content.ShareHolders[i].SHCountry = response.Content.ShareHolders[i].SHCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.Authignatories.Count; i++)
                        {
                            response.Content.Authignatories[i].AuthorisedsignatoriesName = response.Content.Authignatories[i].AuthorisedsignatoriesName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].AuthorisedsignatoriesName, specialChar) : null;
                            response.Content.Authignatories[i].CorrespondenceAddress = response.Content.Authignatories[i].CorrespondenceAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].CorrespondenceAddress, specialChar) : null;
                            response.Content.Authignatories[i].Remarks = response.Content.Authignatories[i].Remarks != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].Remarks, specialChar) : null;
                            response.Content.Authignatories[i].ASCity = response.Content.Authignatories[i].ASCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCity, specialChar) : null;
                            response.Content.Authignatories[i].ASState = response.Content.Authignatories[i].ASState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASState, specialChar) : null;
                            response.Content.Authignatories[i].ASCountry = response.Content.Authignatories[i].ASCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BoardDirectors.Count; i++)
                        {
                            response.Content.BoardDirectors[i].BoardDirectorName = response.Content.BoardDirectors[i].BoardDirectorName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorName, specialChar) : null;
                            response.Content.BoardDirectors[i].Designation = response.Content.BoardDirectors[i].Designation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].Designation, specialChar) : null;
                            response.Content.BoardDirectors[i].BoardDirectorAddress = response.Content.BoardDirectors[i].BoardDirectorAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorAddress, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCity = response.Content.BoardDirectors[i].BDCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCity, specialChar) : null;
                            response.Content.BoardDirectors[i].BDState = response.Content.BoardDirectors[i].BDState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDState, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCountry = response.Content.BoardDirectors[i].BDCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BeneficialOwners.Count; i++)
                        {
                            response.Content.BeneficialOwners[i].BeneficialName = response.Content.BeneficialOwners[i].BeneficialName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialName, specialChar) : null;
                            response.Content.BeneficialOwners[i].BeneficialAddress = response.Content.BeneficialOwners[i].BeneficialAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialAddress, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCity = response.Content.BeneficialOwners[i].BOCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCity, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOState = response.Content.BeneficialOwners[i].BOState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOState, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCountry = response.Content.BeneficialOwners[i].BOCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCountry, specialChar) : null;
                        }
                    }
                    return Ok(response);
                }
            }

                    return CreatedAtAction(null,response);
        }

        //todo fix this as account model should take accountid and status only

        [Route("UpdateAccountStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<Account>))]
        public async Task<IActionResult> UpdateAccountStatus([FromBody] AccountUpdateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<Account> response = new ActiveResponseSucces<Account>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateAccountStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Account>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

                    return CreatedAtAction(null,response);

        }


        [Route("CreateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateAccount([FromBody] CreateAccount Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;

            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();
            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:CreateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return Ok(response);
                }
            }
                    return CreatedAtAction(null,response);

        }

        //todo this need to fix as account model need to have validations
        [Route("CreateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateCorporateAccount([FromBody] CreateCorporateAccount Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return CreatedAtAction(null, response);

                }
            }
            return BadRequest(response);
        }


        [Route("UpdateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> UpdateCorporateAccount([FromBody] UpdateCorporateAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);

            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFinquiryByIdno")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryByIdno([FromBody] Riminquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:riminquirybyIdno"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);

            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFinquiryViaCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryViaCIF([FromBody] Riminquiryrim Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse( null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:riminquiryrim"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        /// <summary>
        /// Rim Inquiry.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///         "signOnRq":{ 
        ///        "LogId": "e0ccbe1a-dfdd-4753-a4c7-acc6fb0665e3",
        ///          "DateTime":"03-06-2019",
        ///          "ChannelId":"Swagger",
        ///         "OurBranchId":"079"},
        ///       
        ///        "CprNumber": "3840366576395",
        ///     }
        ///
        /// </remarks>
        /// <response code="400">If any Error Occur</response>            

        [ProducesResponseType(typeof(ActiveResponseSucces<CustomerInfo>), 200)]
        [ProducesResponseType(400)]
        [Route("CreateRetailCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CreateRetailCIF([FromBody] CreateCIF Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.NIC = Model.Cust.idno;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
                Model.Cust.Email = Model.Cust.customerEmail;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:CreateCif"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreateCorporateCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo> ", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> CreateCorporateCIF([FromBody] DBHandler.Model.Dtos.CustomerModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.ReasonACorp = Model.Cust.FATCANoReasonCorp;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetPaymentStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<TransferResponse>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferStatusResponse> ", typeof(ActiveResponseSucces<TransferStatusResponse>))]
        public async Task<IActionResult> GetPaymentStatus([FromBody] TransferStatusModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TransferStatusResponse> response = new ActiveResponseSucces<TransferStatusResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetTransferStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferStatusResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetNarrations")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionDescription>>", typeof(ActiveResponseSucces<List<TransactionDescription>>))]
        public async Task<IActionResult> GetNarrations([FromBody] NarrationModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TransactionDescription>> response = new ActiveResponseSucces<List<TransactionDescription>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetNarations"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionDescription>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountView>>", typeof(ActiveResponseSucces<List<AccountView>>))]
        public async Task<IActionResult> AccountInquiry([FromBody] AccountInquiryRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<AccountView>> response = new ActiveResponseSucces<List<AccountView>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AccountInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountView>>>(result.httpResult.httpResponse);
                List<AccountView> list = new List<AccountView>();
                if (response.Content != null && response.Content.Count > 0)
                {
                    foreach (AccountView v in response.Content)
                    {
                        if (v.Status == "")
                        {
                            v.Status = "Active";
                        }
                        else if (v.Status == "I")
                        {
                            v.Status = "InActive";
                        }
                        else if (v.Status == "T")
                        {
                            v.Status = "Dormant";
                        }
                        else if (v.Status == "X")
                        {
                            v.Status = "Deceased";
                        }
                        else if (v.Status == "D")
                        {
                            v.Status = "Blocked";
                        }
                        else if (v.Status == "C")
                        {
                            v.Status = "Closed";
                        }
                        else
                        {
                            v.Status = "Undefined";
                        }
                        list.Add(v);
                    }
                    response.Content = list;
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        //[Route("GetCurrency")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[ProducesResponseType(typeof(ActiveResponse), 400)]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<Currencies>", typeof(ActiveResponseSucces<Currencies>))]
        //public async Task<IActionResult> GetCurrency([FromBody] CurrenciesInquiryRequest Model)
        //{
        //    var dto = Utility.ReadFromHeaders(this.HttpContext);
        //    Model.SignOnRq = dto.SignOnRq;
        //    DateTime starTime = DateTime.Now;
        //    IActionResult Result = null;
        //    ActiveResponseSucces<Currencies> ResponseSuccess = new ActiveResponseSucces<Currencies>();
        //    DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
        //    HttpContext ctx = Request.HttpContext;
        //    var id = HttpContext.User.Claims.First().Value;

        //    try
        //    {
        //        bool isModelValid = TryValidateModel(Model);
        //        if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
        //        {
        //            if (!isModelValid)
        //            {
        //                var errors = ModelState
        //               .Where(x => x.Value.Errors.Count > 0)
        //                .Select(x => new { x.Key, x.Value.Errors })
        //                .ToArray();
        //                ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = errors[0].Key.ToUpper() + ":" + errors[0].Errors[0].ErrorMessage, Code = "ERROR-01" };
        //            }
        //            else
        //            {
        //                ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
        //            }

        //            level = Level.Error;
        //            excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
        //            Result = BadRequest(ResponseSuccess);
        //        }
        //        else
        //        {
        //            var cur = _userChannels.GetCurrencies(Model.Currency);

        //            if (cur != null)
        //            {
        //                ResponseSuccess.Content = cur;
        //                Result = Ok(ResponseSuccess);
        //                ResponseSuccess.Status = new Status { Severity = Severity.Success, StatusMessage = "Success", Code = "MSG-000000" };
        //            }
        //            else
        //            {
        //                Result = BadRequest(ResponseSuccess);
        //                ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = "No Currency found", Code = "ERROR-0101" };
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        level = Level.Error;
        //        //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
        //        ResponseSuccess.Content = null;
        //        ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
        //        excetionForLog = ex;
        //        Result = BadRequest(ResponseSuccess);
        //    }

        //    ResponseSuccess.LogId = (Model == null || Model.SignOnRq == null) ? "" : Model.SignOnRq.LogId;
        //    ResponseSuccess.RequestDateTime = (Model == null || Model.SignOnRq == null) ? DateTime.Now : Model.SignOnRq.DateTime;
        //    string channelID = (Model == null || Model.SignOnRq == null) ? "" : Model.SignOnRq.ChannelId;

        //    //var userID = HttpContext.User.Claims.First().Value;
        //    var userID = id;
        //    string res = JsonConvert.SerializeObject(Model);
        //    string req = JsonConvert.SerializeObject(Result);
        //    DateTime Endtime = DateTime.Now;
        //    HttpContext Ctx = Request.HttpContext;
        //    if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
        //    {
        //        _logs.Logs(level, ResponseSuccess.Status != null ? ResponseSuccess.Status.Code.ToString() : "", Ctx, excetionForLog, starTime, Endtime, res, req, userID, channelID, ResponseSuccess.LogId, ResponseSuccess.RequestDateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, "", "", "", _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], _config["SPLUNK:COLLECTOR"].ToString(), _config["SPLUNK:BASE_URL"].ToString(), _config["SPLUNK:AUTHKEY"].ToString());
        //    }
        //    return Result;
        //}


        [Route("UpdateAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAccountMandate([FromBody] AccountMandateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAccountMandate([FromBody] AccountMandateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountMandate>>", typeof(ActiveResponseSucces<List<AccountMandate>>))]
        public async Task<IActionResult> GetAccountMandate([FromBody] AccountMandateViewModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<AccountMandate>> response = new ActiveResponseSucces<List<AccountMandate>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountMandate>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetAccountStatusMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Accountstatusmarking>>", typeof(ActiveResponseSucces<List<Accountstatusmarking>>))]
        public async Task<IActionResult> GetAccountStatusMarking([FromBody] GetAccountStatusMarkingModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<Accountstatusmarking>> response = new ActiveResponseSucces<List<Accountstatusmarking>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountStatusMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Accountstatusmarking>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountStatusMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AccountStatusMarking([FromBody] MarkAccountStatusModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AccountStatusMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UnMarkAccountStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnMarkAccountStatus([FromBody] UnMarkAccountStatusModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnMarkAccountStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ShareHolder>>", typeof(ActiveResponseSucces<List<ShareHolder>>))]
        public async Task<IActionResult> GetShareHolders([FromBody] CIFDetailInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<ShareHolder>> response = new ActiveResponseSucces<List<ShareHolder>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ShareHolder>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AuthorisedSignatoriesView>>", typeof(ActiveResponseSucces<List<AuthorisedSignatoriesView>>))]
        public async Task<IActionResult> GetAuthorisedSignatories([FromBody] CIFDetailInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<AuthorisedSignatoriesView>> response = new ActiveResponseSucces<List<AuthorisedSignatoriesView>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AuthorisedSignatoriesView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BeneficialOwners>>", typeof(ActiveResponseSucces<List<BeneficialOwners>>))]
        public async Task<IActionResult> GetBeneficialOwners([FromBody] CIFDetailInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<BeneficialOwners>> response = new ActiveResponseSucces<List<BeneficialOwners>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BeneficialOwners>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BoardDirectors>>", typeof(ActiveResponseSucces<List<BoardDirectors>>))]
        public async Task<IActionResult> GetBoardDirectors([FromBody] CIFDetailInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<BoardDirectors>> response = new ActiveResponseSucces<List<BoardDirectors>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BoardDirectors>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteShareHolders([FromBody] DeleteRequestForClientDetail Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteAuthSignatory")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteAuthSignatory([FromBody] DeleteRequestForClientDetail Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteAuthSignatory"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBeneficialOwners([FromBody] DeleteRequestForClientDetail Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBoardDirectors([FromBody] DeleteRequestForClientDetail Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SignatureModel>", typeof(ActiveResponseSucces<SignatureModel>))]
        public async Task<IActionResult> GetSignature([FromBody] DeleteRequestForClientDetail Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<SignatureModel> response = new ActiveResponseSucces<SignatureModel>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SignatureModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateGroupAndSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateGroupAndSignature([FromBody] UpdateSignatureModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateGroupAndSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }



        [Route("UpdateShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("UpdateRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailClient([FromBody] UpdateCIF Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.Email = Model.Cust.customerEmail;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateCorporateClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCorporateClient([FromBody] UpdateCorporateCIF Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
            }
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("UpdateCorporateCifWithChilds")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo>", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> UpdateCorporateCifWithChilds([FromBody] DBHandler.Model.Dtos.UpdateCorporateCifWithChildsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
            }
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateCifWithChilds"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreateFIKycLiteCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo>", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> CreateFIKycLiteCustomer([FromBody] DBHandler.Model.Dtos.CreateFIKycLiteCustomerModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
            }
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateFIKycLiteCustomer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InsertCustomerRetail>", typeof(ActiveResponseSucces<InsertCustomerRetail>))]
        public async Task<IActionResult> GetRetailClient([FromBody] RetailCustomerInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InsertCustomerRetail> response = new ActiveResponseSucces<InsertCustomerRetail>();

            ActiveResponseSucces<InsertCustomerRetailView> res = new ActiveResponseSucces<InsertCustomerRetailView>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetRetailClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetail>>(result.httpResult.httpResponse);
                res = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetailView>>(result.httpResult.httpResponse);


                if (result != null && result.httpResult?.severity == "Success")
                {
                    res.Content.idno = response.Content.NIC;
                    res.Content.IDExpirydate = response.Content.NicExpirydate;
                    res.Content.customerEmail = response.Content.Email;
                    return Ok(res);
                }
            }
            return BadRequest(response);
        }


        [Route("GetCIFBasicDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerBasicDetailModel>", typeof(ActiveResponseSucces<CustomerBasicDetailModel>))]
        public async Task<IActionResult> GetCIFBasicDetails([FromBody] RetailCustomerInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CustomerBasicDetailModel> res = new ActiveResponseSucces<CustomerBasicDetailModel>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetCIFBasicDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            res.Status = result.Status;
            if (result.Status.Code == null)
            {
                res = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerBasicDetailModel>>(result.httpResult.httpResponse);

                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(res);
                }
            }
            return BadRequest(res);
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        [Route("GetAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountViewDetail>", typeof(ActiveResponseSucces<AccountViewDetail>))]
        public async Task<IActionResult> GetAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<AccountViewDetail> response = new ActiveResponseSucces<AccountViewDetail>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountViewDetail>>(result.httpResult.httpResponse);

                if (response.Content != null)
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                }

                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetCIFAccountsBalanceInq")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> GetCIFAccountsBalanceInq([FromBody] RetailCustomerInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetCIFAccountsBalanceInq"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateRetailAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailAccount([FromBody] UpdateAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateRetailAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("FreezeAccountAmountInquiryRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List< AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryRetail([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountInquiryRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("FreezeAccountAmountInquiryCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryCorporate([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountInquiryCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("FreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountRetail([FromBody] FreezeAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("FreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountCorporate([FromBody] FreezeAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("UnFreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountRetail([FromBody] UpdateAccountFreezeModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnFreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("UnFreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountCorporate([FromBody] UpdateAccountFreezeModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnFreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTransactionListRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListRetail([FromBody] GetTransactionModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetTransactionListRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTransactionListCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListCorporate([FromBody] GetTransactionModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetTransactionListCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("LinkDebitCardStatusUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCardStatusUpdate([FromBody] LinkDebitCardStatusUpdateView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:LinkDebitCardStatusUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LinkDebitCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCard([FromBody] LinkDebitCardStatusInsertView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:LinkDebitCard"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DebitCardInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCards>>", typeof(ActiveResponseSucces<List<DebitCards>>))]
        public async Task<IActionResult> DebitCardInquiry([FromBody] DebitCardInquiryView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DebitCards>> response = new ActiveResponseSucces<List<DebitCards>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:DebitCardInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCards>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("DebitCardCategoryInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCardCategory>>", typeof(ActiveResponseSucces<List<DebitCardCategory>>))]
        public async Task<IActionResult> DebitCardCategoryInquiry([FromBody] DebitCardCategoryInquiryView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DebitCardCategory>> response = new ActiveResponseSucces<List<DebitCardCategory>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:DebitCardCategoryInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCardCategory>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("CurrencyPairsinquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CurrencyPairing>>", typeof(ActiveResponseSucces<List<CurrencyPairing>>))]
        public async Task<IActionResult> CurrencyPairsinquiry([FromBody] DebitCardCategoryInquiryView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<CurrencyPairing>> response = new ActiveResponseSucces<List<CurrencyPairing>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CurrencyPairsinquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CurrencyPairing>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetStatement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CustomerStatement>>", typeof(ActiveResponseSucces<List<CustomerStatement>>))]
        public async Task<IActionResult> GetStatement([FromBody] GetStatementView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<CustomerStatement>> response = new ActiveResponseSucces<List<CustomerStatement>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetStatement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CustomerStatement>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelRealTimeSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetRealTimeSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse1>))]
        public async Task<IActionResult> GetRealTimeSweep([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<RealTimeSweepResponse1> response = new ActiveResponseSucces<RealTimeSweepResponse1>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetRealTimeSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse1>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Dictionary<string, string>>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> AddMandate([FromBody] DDMandateInsertView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAllMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DDMandateView>>", typeof(ActiveResponseSucces<List<DDMandateView>>))]
        public async Task<IActionResult> GetAllMandate([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DDMandateView>> response = new ActiveResponseSucces<List<DDMandateView>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAllMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DDMandateView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelMandate([FromBody] DDMandateCancelView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CancelMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "Dictionary<string, string>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> GetMandate([FromBody] DDMandateCancelView Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountCorporateView>", typeof(ActiveResponseSucces<AccountCorporateView>))]
        public async Task<IActionResult> GetCorporateAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<AccountCorporateView> response = new ActiveResponseSucces<AccountCorporateView>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                try
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountCorporateView>>(result.httpResult.httpResponse);
                }
                catch (Exception ex)
                {
                    int i = 0;
                }
                if (result != null && result.httpResult?.severity == "Success")
                {

                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("IssuanceOfChequeBooks")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChequeResponseModel>))]
        public async Task<IActionResult> IssuanceOfChequeBooks([FromBody] ChequeModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<ChequeResponseModel> response = new ActiveResponseSucces<ChequeResponseModel>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:IssuanceOfChequeBooks"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChequeResponseModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetChequeBookType")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChqBookTypes>))]
        public async Task<IActionResult> GetChequeBookType([FromBody] ChequeTypeModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<ChqBookTypes> response = new ActiveResponseSucces<ChqBookTypes>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetChequeBookType"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChqBookTypes>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquiryNOSTRO")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> InquiryNOSTRO([FromBody] InquiryNOSTROModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<NostroAccounts>> response = new ActiveResponseSucces<List<NostroAccounts>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:InquiryNOSTRO"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<NostroAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCharity([FromBody] CharityModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFixDepositProducts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ProductView>>", typeof(ActiveResponseSucces<List<ProductView>>))]
        public async Task<IActionResult> GetFixDepositProducts([FromBody] RelationshipOfficerModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<ProductView>> response = new ActiveResponseSucces<List<ProductView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.SignOnRq == null ? null : Model.SignOnRq, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFixDepositProducts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            try
            {
                response.RequestDateTime = Model.SignOnRq.DateTime;
                response.LogId = Model.SignOnRq.LogId;
                response.Status = result.Status;
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ProductView>>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        CreatedAtAction(null,response);
                    }
                }

                return BadRequest(response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("GetDepositRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DepositRatesDTO>", typeof(ActiveResponseSucces<DepositRatesDTO>))]
        public async Task<IActionResult> GetDepositRates([FromBody] DepositRatesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DepositRatesDTO> response = new ActiveResponseSucces<DepositRatesDTO>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetDepositRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositRatesDTO>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("OpenFDAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> OpenFDAccount([FromBody] OpenFDAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:OpenFDAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("OpenFixDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IssueFixDepositResponse>", typeof(ActiveResponseSucces<IssueFixDepositResponse>))]
        public async Task<IActionResult> OpenFixDeposit([FromBody] OpenFixDepositModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<IssueFixDepositResponse> response = new ActiveResponseSucces<IssueFixDepositResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:OpenFixDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<IssueFixDepositResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCustomerPostingAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetCustomerPostingAccount([FromBody] CustomerPostingAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<GetCustPostingAccountNew>> response = new ActiveResponseSucces<List<GetCustPostingAccountNew>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.SignOnRq == null ? null : Model.SignOnRq, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetCustomerPostingAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetCustPostingAccountNew>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("DepositsListAccountwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsListAccountwise([FromBody] DepositsListAccountwiseModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsListAccountwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositReceiptwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TDRIssuance>", typeof(ActiveResponseSucces<TDRIssuance>))]
        public async Task<IActionResult> DepositReceiptwise([FromBody] DepositReceiptwiseModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TDRIssuance> response = new ActiveResponseSucces<TDRIssuance>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositReceiptwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TDRIssuance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDAccountsProductwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDAccountsProductwise>>", typeof(ActiveResponseSucces<List<FDAccountsProductwise>>))]
        public async Task<IActionResult> GetFDAccountsProductwise([FromBody] FDAccountsProductwiseModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<FDAccountsProductwise>> response = new ActiveResponseSucces<List<FDAccountsProductwise>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDAccountsProductwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDAccountsProductwise>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDCollectionAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDCollectionAccounts>>", typeof(ActiveResponseSucces<List<FDCollectionAccounts>>))]
        public async Task<IActionResult> GetFDCollectionAccounts([FromBody] GetFDCollectionAccountsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<FDCollectionAccounts>> response = new ActiveResponseSucces<List<FDCollectionAccounts>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDCollectionAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDCollectionAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDCharges")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDChargesArray>>", typeof(ActiveResponseSucces<List<FDChargesArray>>))]
        public async Task<IActionResult> GetFDCharges([FromBody] FDChargesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<FDChargesArray>> response = new ActiveResponseSucces<List<FDChargesArray>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDCharges"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDChargesArray>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("MarkFDLost")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> MarkFDLost([FromBody] TDRLostModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:MarkFDLost"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("ModifyFixedDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyFixedDeposit([FromBody] ModifyFDModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:ModifyFixedDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("DepositsListClientWise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsListClientWise([FromBody] DepositsListClientWiseModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsListClientWise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireOutstandingAmt")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireOutstandingAmt>", typeof(ActiveResponseSucces<InquireOutstandingAmt>))]
        public async Task<IActionResult> InquireOutstandingAmt([FromBody] BalanceInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireOutstandingAmt> response = new ActiveResponseSucces<InquireOutstandingAmt>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:InquireOutstandingAmt"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireOutstandingAmt>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LienUnlienMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LienUnlienMarking([FromBody] LienUnlienMarkingModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:LienUnlienMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireLien")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<InquireLien>>", typeof(ActiveResponseSucces<List<InquireLien>>))]
        public async Task<IActionResult> InquireLien([FromBody] InquireLienModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<InquireLien>> response = new ActiveResponseSucces<List<InquireLien>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:InquireLien"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<InquireLien>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<TDRIssuanceModel>))]
        public async Task<IActionResult> DepositsDetails([FromBody] DepositsDetailsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TDRIssuanceModel> response = new ActiveResponseSucces<TDRIssuanceModel>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TDRIssuanceModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DepositAdviceResponse>>", typeof(ActiveResponseSucces<DepositAdviceResponse>))]
        public async Task<IActionResult> DepositAdvice([FromBody] DepositAdviceModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DepositAdviceResponse> response = new ActiveResponseSucces<DepositAdviceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositAdviceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAccrualRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> GetAccrualRates([FromBody] AccrualRatesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccrualRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetDailyAccrual")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<RptSavAccruals>", typeof(ActiveResponseSucces<List<RptSavAccruals>>))]
        public async Task<IActionResult> GetDailyAccrual([FromBody] AccrualRatesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<RptSavAccruals>> response = new ActiveResponseSucces<List<RptSavAccruals>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GettDailyAccrual"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RptSavAccruals>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    List<RptSavAccruals> newlist = new List<RptSavAccruals>();
                    List<RptSavAccruals> list = response.Content;
                    foreach (RptSavAccruals l in list)
                    {
                        if (l.Days == 1)
                        {
                            newlist.Add(l);
                        }
                        else
                        {
                            decimal amount = l.Amount / l.Days;
                            for (int i = 0; i < l.Days; i++)
                            {
                                RptSavAccruals r = new RptSavAccruals();
                                r.AccountID = l.AccountID;
                                r.AccrualDate = l.AccrualDate;
                                r.Amount = amount;
                                r.Balance = l.Balance;
                                r.CurrencyID = l.CurrencyID;
                                r.Days = l.Days;
                                r.Rate = l.Rate;
                                r.ValueDate = l.ValueDate.AddDays(i);
                                newlist.Add(r);
                            }
                        }
                    }
                    response.Content = newlist;
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CalculateMaturityDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<MaturityValues>", typeof(ActiveResponseSucces<MaturityValues>))]
        public async Task<IActionResult> CalculateMaturityDate([FromBody] CalculateMaturityDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<MaturityValues> response = new ActiveResponseSucces<MaturityValues>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculateMaturityDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<MaturityValues>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsList")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsList([FromBody] DepositsListModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsList"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CloseFixedDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CloseFXResponse>", typeof(ActiveResponseSucces<CloseFXResponse>))]
        public async Task<IActionResult> CloseFixedDeposit([FromBody] CloseFixedDepositModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CloseFXResponse> response = new ActiveResponseSucces<CloseFXResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CloseFixedDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CloseFXResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("FDPartialWithdrawal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FDPartialWithdrawalResponse>", typeof(ActiveResponseSucces<FDPartialWithdrawalResponse>))]
        public async Task<IActionResult> FDPartialWithdrawal([FromBody] FDPartialWithdrawalModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FDPartialWithdrawalResponse> response = new ActiveResponseSucces<FDPartialWithdrawalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:FDPartialWithdrawal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FDPartialWithdrawalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCustomerCardData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCustomerCardData([FromBody] CustomerCardDataModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;

            if (!string.IsNullOrEmpty(Model.publicdata.IssueDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.IssueDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy");
                Model.publicdata.IssueDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.ExpiryDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.ExpiryDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.ExpiryDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.DOB))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.DOB, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.DOB = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.PassportIssueDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.PassportIssueDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.PassportIssueDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.PassportExpiryDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.PassportExpiryDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.PassportExpiryDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.DateOfGraduation))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.DateOfGraduation, new CultureInfo("en-GB")).ToString("MM/dd/yyyyy"); ;
                Model.publicdata.DateOfGraduation = stringDate;
            }
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddCustomerCardData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCountryLimits([FromBody] CountryLimitsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("UpdateCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCountryLimits([FromBody] CountryLimitsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("CountryLimitInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<CountryLimits>))]
        public async Task<IActionResult> CountryLimitInquire([FromBody] InquirCountryLimitsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CountryLimits> response = new ActiveResponseSucces<CountryLimits>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CountryLimitInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CountryLimits>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<List<VAResponse>>))]
        public async Task<IActionResult> GetVirtualAccount([FromBody] GetVirtualAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<VAResponse>> response = new ActiveResponseSucces<List<VAResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<VAResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("CreateVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<VirtualAccountResponse>))]
        public async Task<IActionResult> CreateVirtualAccount([FromBody] VirtualAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<VirtualAccountResponse> response = new ActiveResponseSucces<VirtualAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CreateVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<VirtualAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("ModifyVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyVirtualAccount([FromBody] ModifyVirtualAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:ModifyVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelVirtualAccount([FromBody] CancelVirtualAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CancelVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTotalCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetCharity>", typeof(ActiveResponseSucces<GetCharity>))]
        public async Task<IActionResult> GetTotalCharity([FromBody] GetTotalCharityDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<GetCharity> response = new ActiveResponseSucces<GetCharity>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetTotalCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetCharity>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetPenaltyMatrix")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<PenaltyMatrix>>", typeof(ActiveResponseSucces<List<PenaltyMatrix>>))]
        public async Task<IActionResult> GetPenaltyMatrix([FromBody] PenaltyMatrixModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<PenaltyMatrix>> response = new ActiveResponseSucces<List<PenaltyMatrix>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetPenaltyMatrix"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<PenaltyMatrix>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetRelationshipManagers")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<RelationshipOfficer>>", typeof(ActiveResponseSucces<List<RelationshipOfficer>>))]
        public async Task<IActionResult> GetRelationshipManagers([FromBody] RelationshipOfficerModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<RelationshipOfficer>> response = new ActiveResponseSucces<List<RelationshipOfficer>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetRelationshipManagers"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RelationshipOfficer>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetBasicAccountInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetBasicAccountInquiryResponse>", typeof(ActiveResponseSucces<GetBasicAccountInquiryResponse>))]
        public async Task<IActionResult> GetBasicAccountInquiry([FromBody] GetBasicAccountInquiryModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<GetBasicAccountInquiryResponse> response = new ActiveResponseSucces<GetBasicAccountInquiryResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetBasicAccountInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetBasicAccountInquiryResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (Model.SignOnRq.ChannelId == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.ClientName = response.Content.ClientName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ClientName, specialChar) : null;
                        response.Content.AccountTitle = response.Content.AccountTitle != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.AccountTitle, specialChar) : null;
                        response.Content.CustomerAddress = response.Content.CustomerAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.CustomerAddress, specialChar) : null;
                    }
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetLoanAccountsPerCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>", typeof(ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>))]
        public async Task<IActionResult> GetLoanAccountsPerCIF([FromBody] GetLoanAccountsPerCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<LoanAccountsPerCIFResponse>> response = new ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanAccountsPerCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetLoanDetailsPerAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>", typeof(ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>))]
        public async Task<IActionResult> GetLoanDetailsPerAccount([FromBody] GetLoanDetailsPerAccountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<LoanDetailsPerAccountResponse>> response = new ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanDetailsPerAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetLoanScheduleDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<List<LoanScheduleDetailsResponse>>>", typeof(ActiveResponseSucces<List<LoanScheduleDetailsResponse>>))]
        public async Task<IActionResult> GetLoanScheduleDetails([FromBody] GetLoanScheduleDetailsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<LoanScheduleDetailsResponse>> response = new ActiveResponseSucces<List<LoanScheduleDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanScheduleDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanScheduleDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<List<LoanAdviceResponse>>>", typeof(ActiveResponseSucces<LoanAdviceResponse>))]
        public async Task<IActionResult> LoanAdvice([FromBody] LoanAdviceModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<LoanAdviceResponse> response = new ActiveResponseSucces<LoanAdviceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAdviceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LiabilityLetter")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LiabilityLetterResponse>", typeof(ActiveResponseSucces<LiabilityLetterResponse>))]
        public async Task<IActionResult> LiabilityLetter([FromBody] LiabilityLetterModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<LiabilityLetterResponse> response = new ActiveResponseSucces<LiabilityLetterResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LiabilityLetter"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LiabilityLetterResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("NoLiabilityLetter")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NoLiabilityLetterResponse>", typeof(ActiveResponseSucces<NoLiabilityLetterResponse>))]
        public async Task<IActionResult> NoLiabilityLetter([FromBody] LiabilityLetterModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<NoLiabilityLetterResponse> response = new ActiveResponseSucces<NoLiabilityLetterResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:NoLiabilityLetter"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<NoLiabilityLetterResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("BalanceConfirmation")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<BalanceConfirmationResponse>", typeof(ActiveResponseSucces<BalanceConfirmationResponse>))]
        public async Task<IActionResult> BalanceConfirmation([FromBody] LiabilityLetterModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<BalanceConfirmationResponse> response = new ActiveResponseSucces<BalanceConfirmationResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BalanceConfirmation"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<BalanceConfirmationResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("UpdateKYCDateCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateKYCDateCorporate([FromBody] UpdateKYCDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateKYCDateCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateKYCDateRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateKYCDateRetail([FromBody] UpdateKYCDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateKYCDateRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CheckIDForCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CheckIDForCIF([FromBody] CheckIDForCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CheckIDForCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("GetExpiredCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ExpiredCIFResponse>>", typeof(ActiveResponseSucces<List<ExpiredCIFResponse>>))]
        public async Task<IActionResult> GetExpiredCIF([FromBody] ExpiredCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<ExpiredCIFResponse>> response = new ActiveResponseSucces<List<ExpiredCIFResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetExpiredCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                var resp = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Customer>>>(result.httpResult.httpResponse);
                if (resp != null && result != null && result.httpResult?.severity == "Success")
                {
                    List<ExpiredCIFResponse> cif = new List<ExpiredCIFResponse>();
                    foreach (Customer cust in resp.Content)
                    {
                        ExpiredCIFResponse r = new ExpiredCIFResponse();
                        r.ClientID = cust.ClientID;
                        r.DOB = cust.DOB;
                        r.Email = cust.Email;
                        r.FirstName = cust.FirstName;
                        r.IDExpirydate = cust.NicExpirydate;
                        r.idno = cust.NIC;
                        r.KYCReviewDate = cust.KYCReviewDate;
                        r.LastName = cust.LastName;
                        r.MiddleName = cust.MiddleName;
                        r.MobileNo = cust.MobileNo;
                        r.Name = cust.Name;
                        r.Nationality = cust.Nationality;
                        r.OnboardingCountry = cust.OnboardingCountry;
                        r.OurBranchID = cust.OurBranchID;
                        r.PassportExpiry = cust.PassportExpiry;
                        r.PassportGender = cust.PassportGender;
                        r.PassportIssueCountry = cust.PassportIssueCountry;
                        r.PassportNumber = cust.PassportNumber;
                        r.Phone1 = cust.Phone1;
                        r.Phone2 = cust.Phone2;
                        cif.Add(r);
                    }
                    response.Content = cif;
                    response.Status = resp.Status;
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAccountBalance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountBalance>", typeof(ActiveResponseSucces<AccountBalance>))]
        public async Task<IActionResult> GetAccountBalance([FromBody] GetTotalCharityDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<AccountBalance> response = new ActiveResponseSucces<AccountBalance>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountBalance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountBalance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireFIKycLiteCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>", typeof(ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>))]
        public async Task<IActionResult> InquireFIKycLiteCustomer([FromBody] RimInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> response = new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireFIKycLiteCustomer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp").Value.ToString();
                    channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }




        [Route("UpdateBasicCIFDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateBasicCIFDetails([FromBody] UpdateBasicCIFDetails Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:UpdateBasicCIFDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("MultiBranch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> MultiBranch([FromBody] MultiBranchRq Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:multibranch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("TreasuryTransfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> TreasuryTransfer([FromBody] MultiBranchRq Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:TreasuryTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateSOCID")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateSOCIDResponse>", typeof(ActiveResponseSucces<UpdateSOCIDResponse>))]
        public async Task<IActionResult> UpdateSOCID([FromBody] UpdateSOCIDModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<UpdateSOCIDResponse> response = new ActiveResponseSucces<UpdateSOCIDResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateSOCID"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateSOCIDResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCifStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCIFStatusDTO>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCifStatus([FromBody] UpdateCIFStatusDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("InqDisbDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDisbursDetailsOut>", typeof(ActiveResponseSucces<InquireDisbursDetailsFinal>))]
        public async Task<IActionResult> InqDisbDetails([FromBody] InquireDisbursDetailsDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireDisbursDetailsFinal> response = new ActiveResponseSucces<InquireDisbursDetailsFinal>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InqDisbDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDisbursDetailsFinal>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireGuaranteeProductAvalIssuance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>", typeof(ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>))]
        public async Task<IActionResult> InquireGuaranteeProductAvalIssuance([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse> response = new ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireGuaranteeProductAvalIssuance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireIssGuaranteeAmedmentRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>", typeof(ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>))]
        public async Task<IActionResult> InquireIssGuaranteeAmedmentRefNum([FromBody] InquireIssGuaranteeAmedmentRefNumModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse> response = new ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireIssGuaranteeAmedmentRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireIssGuaranteeGuarRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>", typeof(ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>))]
        public async Task<IActionResult> InquireIssGuaranteeGuarRefNum([FromBody] InquireIssGuaranteeGuarRefNumModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse> response = new ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireIssGuaranteeGuarRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireAvailableLimit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireAvailableLimitResponse>", typeof(ActiveResponseSucces<InquireAvailableLimitResponseFinal>))]
        public async Task<IActionResult> InquireAvailableLimit([FromBody] InquireAvailableLimitModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireAvailableLimitResponseFinal> response = new ActiveResponseSucces<InquireAvailableLimitResponseFinal>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireAvailableLimit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireAvailableLimitResponseFinal>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireFeeRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireFeeRefNumResponse>", typeof(ActiveResponseSucces<InquireFeeRefNumResponse>))]
        public async Task<IActionResult> InquireFeeRefNum([FromBody] InquireFeeRefNumModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireFeeRefNumResponse> response = new ActiveResponseSucces<InquireFeeRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireFeeRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireFeeRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireDealRefNumODPayCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>", typeof(ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>))]
        public async Task<IActionResult> InquireDealRefNumODPayCIF([FromBody] InquireIssGuaranteeAmedmentRefNumModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireDealRefNumODPayCIFResponse> response = new ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireDealRefNumODPayCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireODPaymentDealRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireODPaymentDealRefNumResponse>", typeof(ActiveResponseSucces<InquireODPaymentDealRefNumResponse>))]
        public async Task<IActionResult> InquireODPaymentDealRefNum([FromBody] InquireODPaymentDealRefNumModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireODPaymentDealRefNumResponse> response = new ActiveResponseSucces<InquireODPaymentDealRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireODPaymentDealRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireODPaymentDealRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireRefNumDebitInstCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>", typeof(ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>))]
        public async Task<IActionResult> InquireRefNumDebitInstCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireRefNumDebitInstCIFResponse> response = new ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireRefNumDebitInstCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireRefNumFeeChargesCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>", typeof(ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>))]
        public async Task<IActionResult> InquireRefNumFeeChargesCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse> response = new ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireRefNumFeeChargesCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireDealRefNumEarlySettleCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>", typeof(ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>))]
        public async Task<IActionResult> InquireDealRefNumEarlySettleCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse> response = new ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireDealRefNumEarlySettleCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeIssue")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IssueResponse>", typeof(ActiveResponseSucces<IssueResponse>))]
        public async Task<IActionResult> GuaranteeIssue([FromBody] IssueRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<IssueResponse> response = new ActiveResponseSucces<IssueResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeIssue"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<IssueResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GuaranteeAmend")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AmendResponse>", typeof(ActiveResponseSucces<AmendResponse>))]
        public async Task<IActionResult> GuaranteeAmend([FromBody] AmendRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<AmendResponse> response = new ActiveResponseSucces<AmendResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeAmend"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AmendResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelResponse>", typeof(ActiveResponseSucces<CancelResponse>))]
        public async Task<IActionResult> GuaranteeCancel([FromBody] CancelRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CancelResponse> response = new ActiveResponseSucces<CancelResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeInvoke")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InvokeResponse>", typeof(ActiveResponseSucces<InvokeResponse>))]
        public async Task<IActionResult> GuaranteeInvoke([FromBody] InvokeRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InvokeResponse> response = new ActiveResponseSucces<InvokeResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeInvoke"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InvokeResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeClose")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CloseResponse>", typeof(ActiveResponseSucces<CloseResponse>))]
        public async Task<IActionResult> GuaranteeClose([FromBody] CloseRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CloseResponse> response = new ActiveResponseSucces<CloseResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeClose"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CloseResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GuaranteeinvokeRev")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InvokeRevResponse>", typeof(ActiveResponseSucces<InvokeRevResponse>))]
        public async Task<IActionResult> GuaranteeinvokeRev([FromBody] InvokeRevRequest Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InvokeRevResponse> response = new ActiveResponseSucces<InvokeRevResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeinvokeRev"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InvokeRevResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("FacilityOverview")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>))]
        public async Task<IActionResult> FacilityOverview([FromBody] DBHandler.Model.FacilityOverview.FacilityOverviewModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityOverview"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanOverview")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>", typeof(ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>))]
        public async Task<IActionResult> LoanOverview([FromBody] DBHandler.Model.LoanOverview.LoanOverviewModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanOverview"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("LoanSchedule")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>", typeof(ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>))]
        public async Task<IActionResult> LoanSchedule([FromBody] DBHandler.Model.LoanSchedule.LoanScheduleModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSchedule"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>))]
        public async Task<IActionResult> LoanDetails([FromBody] DBHandler.Model.LoanDetail.LoanDetailsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCP")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>))]
        public async Task<IActionResult> BuyerCCP([FromBody] DBHandler.Model.BuyerCCP.BuyerCCPsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCP"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCPDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>))]
        public async Task<IActionResult> BuyerCCPDetails([FromBody] DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("EarlySettlementExecution")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>))]
        public async Task<IActionResult> EarlySettlementExecution([FromBody] DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse> response = new ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:EarlySettlementExecution"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("EarlySettlementInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>))]
        public async Task<IActionResult> EarlySettlementInquiry([FromBody] DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse> response = new ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:EarlySettlementExecution"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("FeeChargesReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>))]
        public async Task<IActionResult> FeeChargesReversal([FromBody] DBHandler.Model.FeeChargesReversal.FeeChargesReversalModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse> response = new ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("DebitInstruction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.DebitInstruction.DebitInstructionResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>))]
        public async Task<IActionResult> DebitInstruction([FromBody] DBHandler.Model.DebitInstruction.DebitInstructionModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse> response = new ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanRepayment")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>))]
        public async Task<IActionResult> LoanRepayment([FromBody] DBHandler.Model.LoanRepayment.LoanRepaymentModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("CalculateReceiptMaturityDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>))]
        public async Task<IActionResult> CalculateReceiptMaturityDate([FromBody] DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse> response = new ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculateReceiptMaturityDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAllManageGroups")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>))]
        public async Task<IActionResult> GetAllManageGroups([FromBody] DBHandler.Model.GetAllManageGroups.GetAllManageGroupsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAllManageGroups"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetManageGroup")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>))]
        public async Task<IActionResult> GetManageGroup([FromBody] DBHandler.Model.GetManageGroup.GetManageGroupModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetManageGroup"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("SaveManageGroup")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>))]
        public async Task<IActionResult> SaveManageGroup([FromBody] DBHandler.Model.SaveManageGroup.SaveManageGroupModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:SaveManageGroup"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DeleteManageGroup")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>))]
        public async Task<IActionResult> DeleteManageGroup([FromBody] DBHandler.Model.DeleteManageGroup.DeleteManageGroupModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteManageGroup"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddUpdateBorrowMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>))]
        public async Task<IActionResult> AddUpdateBorrowMandate([FromBody] DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse> response = new ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBorrowManDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetBorrowMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>))]
        public async Task<IActionResult> GetBorrowMandate([FromBody] DBHandler.Model.GetBorrowManDate.GetBorrowManDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetBorrowManDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DeleteBorrowMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>))]
        public async Task<IActionResult> DeleteBorrowMandate([FromBody] DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteBorrowManDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetSweepDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>))]
        public async Task<IActionResult> GetSweepDetail([FromBody] DBHandler.Model.GetSweepDetial.GetSweepDetialModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse> response = new ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetSweepDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetCIFAccountsBalanceInqEBoss")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>))]
        public async Task<IActionResult> GetCIFAccountsBalanceInqEBoss([FromBody] DBHandler.Model.RetailCustomerInquiryEBossModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>> response = new ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetCIFAccountsBalanceInqEBoss"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        #endregion

        #region Ramiz Functions

        [Route("transfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> transfer([FromBody] TransferRq Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:DoTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (response.Content == null)
                {
                    response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
                }
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireCollateralOut>", typeof(ActiveResponseSucces<InquireCollateralOut>))]
        public async Task<IActionResult> CollateralInquiry([FromBody] CollateralInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<InquireCollateralOut> response = new ActiveResponseSucces<InquireCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CollateralSearch([FromBody] SearchCollateral Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCollateralOut>", typeof(ActiveResponseSucces<CreateCollateralOut>))]
        public async Task<IActionResult> CollateralCreate([FromBody] CreateCollateralDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreateCollateralOut> response = new ActiveResponseSucces<CreateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCollateralOut>", typeof(ActiveResponseSucces<UpdateCollateralOut>))]
        public async Task<IActionResult> CollateralUpdate([FromBody] UpdateCollateralDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<UpdateCollateralOut> response = new ActiveResponseSucces<UpdateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCollateralOut>", typeof(ActiveResponseSucces<UpdateCollateralOut>))]
        public async Task<IActionResult> CollateralCancel([FromBody] CollateralInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<UpdateCollateralOut> response = new ActiveResponseSucces<UpdateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //cif
        [Route("CIFExists")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFExists([FromBody] CIFExist Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFExists"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFSearch([FromBody] SearchCIF Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFExposure")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFExposure>>", typeof(ActiveResponseSucces<List<CIFExposure>>))]
        public async Task<IActionResult> CIFExposure([FromBody] CIFExist Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<CIFExposure>> response = new ActiveResponseSucces<List<CIFExposure>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFExposure"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFExposure>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetCASAAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFAccounts>>", typeof(ActiveResponseSucces<List<CIFAccounts>>))]
        public async Task<IActionResult> GetCASAAccounts([FromBody] CIFExist Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<CIFAccounts>> response = new ActiveResponseSucces<List<CIFAccounts>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFHierarchy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFHierarchy>>", typeof(ActiveResponseSucces<List<CIFHierarchy>>))]
        public async Task<IActionResult> CIFHierarchy([FromBody] CIFExist Model)
        {
            ActiveResponseSucces<List<CIFHierarchy>> response = new ActiveResponseSucces<List<CIFHierarchy>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFHierarchy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFHierarchy>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //Account Limit
        [Route("AccountLimitInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityAccInquiryOut>", typeof(ActiveResponseSucces<FacilityAccInquiryOut>))]
        public async Task<IActionResult> AccountLimitInquiry([FromBody] FacilityAccInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityAccInquiryOut> response = new ActiveResponseSucces<FacilityAccInquiryOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityAccInquiryOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SearchAccountLimitOut>", typeof(ActiveResponseSucces<SearchAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitSearch([FromBody] SearchAccountLimitDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<SearchAccountLimitOut> response = new ActiveResponseSucces<SearchAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateAccountLimitOut>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitCreate([FromBody] CreateAccountLimitDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }
        [Route("AccountLimitModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateAccountLimitOut>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitModify([FromBody] ModifyAccountLimitDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelAccountLimitOut>", typeof(ActiveResponseSucces<CancelAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitCancel([FromBody] CancelAccountLimitDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CancelAccountLimitOut> response = new ActiveResponseSucces<CancelAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //Facility Block/UnBlock

        [Route("FacilityBlockInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityBlockInquiry>", typeof(ActiveResponseSucces<FacilityBlockInquiry>))]
        public async Task<IActionResult> FacilityBlockInquire([FromBody] FacilityInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityBlockInquiry> response = new ActiveResponseSucces<FacilityBlockInquiry>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityBlockInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityBlockInquiry>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityBlockOut>", typeof(ActiveResponseSucces<FacilityBlockOut>))]
        public async Task<IActionResult> FacilityBlock([FromBody] FacilityInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityBlockOut> response = new ActiveResponseSucces<FacilityBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityUnBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityUnBlockOut>", typeof(ActiveResponseSucces<FacilityUnBlockOut>))]
        public async Task<IActionResult> FacilityUnBlock([FromBody] FacilityInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityUnBlockOut> response = new ActiveResponseSucces<FacilityUnBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityUnBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityUnBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        // Facility Product Block/UnBlock

        [Route("FacilityProductBlockInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductBlockInquiry>", typeof(ActiveResponseSucces<FacilityProductBlockInquiry>))]
        public async Task<IActionResult> FacilityProductBlockInquire([FromBody] FacilityProductInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityProductBlockInquiry> response = new ActiveResponseSucces<FacilityProductBlockInquiry>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductBlockInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductBlockInquiry>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityProductBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductBlockOut>", typeof(ActiveResponseSucces<FacilityProductBlockOut>))]
        public async Task<IActionResult> FacilityProductBlock([FromBody] FacilityProductInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityProductBlockOut> response = new ActiveResponseSucces<FacilityProductBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityProductUnBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductUnBlockOut>", typeof(ActiveResponseSucces<FacilityProductUnBlockOut>))]
        public async Task<IActionResult> FacilityProductUnBlock([FromBody] FacilityProductInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityProductUnBlockOut> response = new ActiveResponseSucces<FacilityProductUnBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductUnBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductUnBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //Facility
        [Route("FacilityInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityOut>", typeof(ActiveResponseSucces<FacilityOut>))]
        public async Task<IActionResult> FacilityInquiry([FromBody] FacilityInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<FacilityOut> response = new ActiveResponseSucces<FacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateFacilityOut>", typeof(ActiveResponseSucces<CreateFacilityOut>))]
        public async Task<IActionResult> FacilityCreate([FromBody] CreateFacilityDto Model)
        {
            ActiveResponseSucces<CreateFacilityOut> response = new ActiveResponseSucces<CreateFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ModifyFacilityOut>", typeof(ActiveResponseSucces<ModifyFacilityOut>))]
        public async Task<IActionResult> FacilityModify([FromBody] ModifyFacilityDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<ModifyFacilityOut> response = new ActiveResponseSucces<ModifyFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ModifyFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilitySearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SearchFacilityOut>", typeof(ActiveResponseSucces<SearchFacilityOut>))]
        public async Task<IActionResult> FacilitySearch([FromBody] SearchFacilityDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<SearchFacilityOut> response = new ActiveResponseSucces<SearchFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilitySearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelFacilityOut>", typeof(ActiveResponseSucces<CancelFacilityOut>))]
        public async Task<IActionResult> FacilityCancel([FromBody] CancelFacilityDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CancelFacilityOut> response = new ActiveResponseSucces<CancelFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //Loan
        [Route("LoanSimulator")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LoanSimulatorOut>", typeof(ActiveResponseSucces<LoanSimulatorOut>))]
        public async Task<IActionResult> LoanSimulator([FromBody] LoanSimulatorDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<LoanSimulatorOut> response = new ActiveResponseSucces<LoanSimulatorOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSimulator"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanSimulatorOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("LoanSimulatorV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>", typeof(ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>))]
        public async Task<IActionResult> LoanSimulatorV2([FromBody] DBHandler.Model.LoanSimulatorV2.LoanSimulatorDtoV2 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2> response = new ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSimulatorV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanCreateAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LoanAccountOut>", typeof(ActiveResponseSucces<LoanAccountOut>))]
        public async Task<IActionResult> LoanCreateAccounts([FromBody] CreateLoanAccountDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<LoanAccountOut> response = new ActiveResponseSucces<LoanAccountOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanCreateAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAccountOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanDisbursement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DisbursementOut>", typeof(ActiveResponseSucces<DisbursementOut>))]
        public async Task<IActionResult> LoanDisbursement([FromBody] DisbursementDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DisbursementOut> response = new ActiveResponseSucces<DisbursementOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDisbursement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DisbursementOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanDisbReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DisbursementReversalOut>", typeof(ActiveResponseSucces<DisbursementReversalOut>))]
        public async Task<IActionResult> LoanDisbReversal([FromBody] LoanDisbReversalDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DisbursementReversalOut> response = new ActiveResponseSucces<DisbursementReversalOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDisbursementReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DisbursementReversalOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        //CreditProgram
        [Route("CreditProgramInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramOut>", typeof(ActiveResponseSucces<CreditProgramOut>))]
        public async Task<IActionResult> CreditProgramInquiry([FromBody] CreditProgramInquiry Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreditProgramOut> response = new ActiveResponseSucces<CreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreditProgramCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<CreateCreditProgramOut>))]
        public async Task<IActionResult> CreditProgramCreate([FromBody] CreateCreditProgramDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreateCreditProgramOut> response = new ActiveResponseSucces<CreateCreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("CalculatePenaltyAmount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CalculatePenaltyAmountResponse>", typeof(ActiveResponseSucces<CalculatePenaltyAmountResponse>))]
        public async Task<IActionResult> CalculatePenaltyAmount([FromBody] CalculatePenaltyAmountModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CalculatePenaltyAmountResponse> response = new ActiveResponseSucces<CalculatePenaltyAmountResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculatePenaltyAmount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CalculatePenaltyAmountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetDynamicData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> GetDynamicData([FromBody] DynamicParametersDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetDynamicData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("SaveDynamicData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> SaveDynamicData([FromBody] SaveDynamicParametersDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:SaveDynamicData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetZXDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetZXDetailsOut>", typeof(ActiveResponseSucces<GetZXDetailsOut>))]
        public async Task<IActionResult> GetZXDetails([FromBody] GetZXDetailsDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<GetZXDetailsOut> response = new ActiveResponseSucces<GetZXDetailsOut>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetZXDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetZXDetailsOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }

        #endregion

        #region 321 Functions

        //[Route("GetShortListCustomerBasedOnEligibilityCriteria")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<List<Customer>>", typeof(ActiveResponseSucces<List<CustomerDTOFor321>>))]
        //public async Task<IActionResult> GetShortListCustomerBasedOnEligibilityCriteria(SelectSQLModel Model)
        //{
        //    var dto = Utility.ReadFromHeaders(this.HttpContext);
        //    Model.SignOnRq = dto.SignOnRq;
        //    ActiveResponseSucces<List<CustomerDTOFor321>> response = new ActiveResponseSucces<List<CustomerDTOFor321>>();
        //    InternalResult result = await APIRequestHandler.GetResponse(null, Model,
        //                                                                _config["BSLoansAPI:BaseUrl"].ToString(),
        //                                                                _config["BSLoansAPI:GetShortListCustomerBasedOnEligibilityCriteria"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler, 19);
        //    response.Status = result.Status;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CustomerDTOFor321>>>(result.httpResult.httpResponse);
        //        if (result != null && result.httpResult?.severity == "Success")
        //        {
        //            CreatedAtAction(null,response);
        //        }
        //    }

        //    return BadRequest(response);
        //}

        /// <summary>
        /// getting all cp for rule engine web portal
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        //[Route("GetAllCreditProgram")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<List<Customer>>", typeof(ActiveResponseSucces<List<CreditProgramOut>>))]
        //public async Task<IActionResult> GetAllCreditProgram(SignOnRq Model)
        //{
        //    ActiveResponseSucces<List<CreditProgramOut>> response = new ActiveResponseSucces<List<CreditProgramOut>>();
        //    InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model, Model,
        //                                                                _config["BSLoansAPI:BaseUrl"].ToString(),
        //                                                                _config["BSLoansAPI:GetAllCreditProgram"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler);
        //    response.Status = result.Status;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CreditProgramOut>>>(result.httpResult.httpResponse);
        //        if (result != null && result.httpResult?.severity == "Success")
        //        {
        //            CreatedAtAction(null,response);
        //        }
        //    }

        //    return BadRequest(response);
        //}


        /// <summary>
        /// Get Customer DigibancRule Details
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        //[ProducesResponseType(400)]
        //[HttpPost]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[Route("GetCustomerDigibancRuleDetails")]
        //public async Task<IActionResult> GetCustomerDigibancRuleDetails([FromBody] CustomerDigibancRuleDetails Model)
        //{
        //    var dto = Utility.ReadFromHeaders(this.HttpContext);
        //    Model.SignOnRq = dto.SignOnRq;
        //    ActiveResponseSucces<CustomerDigibancRuleDetails> response = new ActiveResponseSucces<CustomerDigibancRuleDetails>();
        //    InternalResult result = await APIRequestHandler.GetResponse(null, Model,
        //                                                                _config["BSLoansAPI:BaseUrl"].ToString(),
        //                                                                _config["BSLoansAPI:GetCustomerDigibancRuleDetails"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler, 19);
        //    response.Status = result.Status;
        //    response.LogId = Model?.SignOnRq?.LogId;
        //    response.RequestDateTime = Model?.SignOnRq?.DateTime;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerDigibancRuleDetails>>(result.httpResult.httpResponse);
        //        if (result != null && result.httpResult?.severity == "Success")
        //        {
        //            CreatedAtAction(null,response);
        //        }
        //    }

        //    return BadRequest(response);
        //}


        #endregion

        #region Wahaj Functions

        [Route("CreditProgramSearchByCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramSearchByCIFResponse>", typeof(ActiveResponseSucces<CreditProgramSearchByCIFResponse>))]
        public async Task<IActionResult> CreditProgramSearchByCIF([FromBody] CreditProgramSearchByCIFModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>> response = new ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramSearchByCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreditProgramUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<CreditProgramUpdateResponse>))]
        public async Task<IActionResult> CreditProgramUpdate([FromBody] CreditProgramUpdateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<CreditProgramUpdateResponse> response = new ActiveResponseSucces<CreditProgramUpdateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreditProgramUpdateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("PostCharges")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<PostChargesResponse>))]
        public async Task<IActionResult> PostCharges([FromBody] PostChargesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<PostChargesResponse> response = new ActiveResponseSucces<PostChargesResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:PostCharges"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<PostChargesResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("UpdateAccountCIFName")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<UpdateAccountCIFNameResponse>))]
        public async Task<IActionResult> UpdateAccountCIFName([FromBody] UpdateAccountCIFNameModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<UpdateAccountCIFNameResponse> response = new ActiveResponseSucces<UpdateAccountCIFNameResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateAccountCIFName"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateAccountCIFNameResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<GetDepositRateResponsee>>", typeof(ActiveResponseSucces<List<GetDepositRateResponse>>))]
        public async Task<IActionResult> GetDepositRate([FromBody] GetDepositRateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<GetDepositRateResponse>> response = new ActiveResponseSucces<List<GetDepositRateResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetDepositRateResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("AddUpdateDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AddUpdateDepositRateResponse>", typeof(ActiveResponseSucces<AddUpdateDepositRateResponse>))]
        public async Task<IActionResult> AddUpdateDepositRate([FromBody] AddUpdateDepositRateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<AddUpdateDepositRateResponse> response = new ActiveResponseSucces<AddUpdateDepositRateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:AddUpdateDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AddUpdateDepositRateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("DeleteDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DeleteDepositRateResponse>", typeof(ActiveResponseSucces<DeleteDepositRateResponse>))]
        public async Task<IActionResult> DeleteDepositRate([FromBody] DeleteDepositRateModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DeleteDepositRateResponse> response = new ActiveResponseSucces<DeleteDepositRateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DeleteDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DeleteDepositRateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("DisbursementV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>))]
        public async Task<IActionResult> DisbursementV2([FromBody] DBHandler.Model.DisbursementV2.DisbursementV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response> response = new ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:DisbursementV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreateCorporateCIFV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> CreateCorporateCIFV2([FromBody] DBHandler.Model.CreateCorporateCIFV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;

            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateCIFV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCorporateClientV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCorporateClientV2([FromBody] DBHandler.Model.UpdateCorporateClientV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateClientV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreateFIKycLiteCustomerV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> CreateFIKycLiteCustomerV2([FromBody] DBHandler.Model.CreateFIKycLiteCustomerV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateFIKycLiteCustomerV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCorporateCifWithChildsV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> UpdateCorporateCifWithChildsV2([FromBody] DBHandler.Model.UpdateCorporateCifWithChildsV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateCifWithChildsV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("ProductDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>))]
        public async Task<IActionResult> ProductDetails([FromBody] DBHandler.Model.ProductDetailsModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:ProductDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("BuyerCCPInvoices")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>))]
        public async Task<IActionResult> BuyerCCPInvoices([FromBody] DBHandler.Model.BuyerCCPInvoicesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPInvoices"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddEditOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>))]
        public async Task<IActionResult> AddEditOnlineUser([FromBody] DBHandler.Model.AddEditOnlineUserModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("ViewOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>))]
        public async Task<IActionResult> ViewOnlineUser([FromBody] DBHandler.Model.ViewOnlineUserModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:ViewOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("DeleteOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>))]
        public async Task<IActionResult> DeleteOnlineUser([FromBody] DBHandler.Model.DeleteOnlineUserModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetAllOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>))]
        public async Task<IActionResult> GetAllOnlineUser([FromBody] DBHandler.Model.GetAllOnlineUserModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAllOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("BuyerCCPJC")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>", typeof(ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>))]
        public async Task<IActionResult> BuyerCCPJC([FromBody] DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse> response = new ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPJC"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetRollOverReciept")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>))]
        public async Task<IActionResult> GetRollOverReciept([FromBody] DBHandler.Model.GetRollOverRecieptModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetRollOverReciept"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("UpdateFloatingRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateFloatingRates([FromBody] DBHandler.Model.UpdateFloatingRatesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateFloatingRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }






        [Route("UpdateFXRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateFXRates([FromBody] DBHandler.Model.UpdateFXRatesModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateFXRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetCorporateAccountDetailEBos")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>", typeof(ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>))]
        public async Task<IActionResult> GetCorporateAccountDetailEBos([FromBody] DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModel Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse> response = new ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateAccountDetailEBos"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        #endregion

        #region Arfeen

        [Route("InquireCorporateCifV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>))]
        public async Task<IActionResult> InquireCorporateCifV2([FromBody] DBHandler.Model.InquireCorporateCifV2Model Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response> response = new ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireCorporateCifV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("AddEditCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>))]
        public async Task<IActionResult> AddEditCIF([FromBody] DBHandler.Model.AddEditCIFDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            } 

            return BadRequest(response);
        }



        [Route("AddEditAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>))]
        public async Task<IActionResult> AddEditAccount([FromBody] DBHandler.Model.AddEditAccountDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }
        [Route("InquireNostroCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>", typeof(ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>))]
        public async Task<IActionResult> InquireNostroCIF([FromBody] DBHandler.Model.InquireNostroCIFDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse> response = new ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireNostroCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireNostroAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>", typeof(ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>))]
        public async Task<IActionResult> InquireNostroAccount([FromBody] DBHandler.Model.InquireNostroAccountDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse> response = new ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireNostroAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetInterestAccruedCurrentYear")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>))]
        public async Task<IActionResult> GetInterestAccruedCurrentYear([FromBody] DBHandler.Model.GetInterestAccruedCurrentYearDTO Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse> response = new ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetInterestAccruedCurrentYear"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateCIFDetailV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>", typeof(ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>))]
        public async Task<IActionResult> GetCorporateCIFDetailV2([FromBody] RimInquiryV2 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2> response = new ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateCIFDetailV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp")?.Value.ToString();
                    if (reaons != null)
                        channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    if (response.Content.Cust.KYCReviewDateCorp != null)
                    {
                        DateTime d = Convert.ToDateTime(response.Content.Cust.KYCReviewDateCorp);
                        response.Content.Cust.KYCReviewDateCorp = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
                    }

                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (Model.SignOnRq.ChannelId == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Cust.Name = response.Content.Cust.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Name, specialChar) : null;
                        response.Content.Cust.RManager = response.Content.Cust.RManager != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RManager, specialChar) : null;
                        response.Content.Cust.EntityNameCorp = response.Content.Cust.EntityNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityNameCorp, specialChar) : null;
                        response.Content.Cust.EntityTypeCorp = response.Content.Cust.EntityTypeCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityTypeCorp, specialChar) : null;
                        response.Content.Cust.RegisteredAddressCorp = response.Content.Cust.RegisteredAddressCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegisteredAddressCorp, specialChar) : null;
                        response.Content.Cust.ContactName1Corp = response.Content.Cust.ContactName1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName1Corp, specialChar) : null;
                        response.Content.Cust.ContactID1Corp = response.Content.Cust.ContactID1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID1Corp, specialChar) : null;
                        response.Content.Cust.Address1Corp = response.Content.Cust.Address1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address1Corp, specialChar) : null;
                        response.Content.Cust.ContactName2Corp = response.Content.Cust.ContactName2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName2Corp, specialChar) : null;
                        response.Content.Cust.ContactID2Corp = response.Content.Cust.ContactID2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID2Corp, specialChar) : null;
                        response.Content.Cust.Address2Corp = response.Content.Cust.Address2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address2Corp, specialChar) : null;
                        response.Content.Cust.BusinessActivity = response.Content.Cust.BusinessActivity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.BusinessActivity, specialChar) : null;
                        response.Content.Cust.TradeLicenseNumberCorp = response.Content.Cust.TradeLicenseNumberCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseNumberCorp, specialChar) : null;
                        response.Content.Cust.CountryofIncorporationCorp = response.Content.Cust.CountryofIncorporationCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.CountryofIncorporationCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.SponsorNameCorp = response.Content.Cust.SponsorNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.SponsorNameCorp, specialChar) : null;
                        response.Content.Cust.NFFECorp = response.Content.Cust.NFFECorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.NFFECorp, specialChar) : null;
                        response.Content.Cust.RegAddCityCorp = response.Content.Cust.RegAddCityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCityCorp, specialChar) : null;
                        response.Content.Cust.RegAddCountryCorp = response.Content.Cust.RegAddCountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCountryCorp, specialChar) : null;
                        response.Content.Cust.RegAddStateCorp = response.Content.Cust.RegAddStateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddStateCorp, specialChar) : null;
                        response.Content.Cust.Add1CountryCorp = response.Content.Cust.Add1CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CountryCorp, specialChar) : null;
                        response.Content.Cust.Add1StateCorp = response.Content.Cust.Add1StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1StateCorp, specialChar) : null;
                        response.Content.Cust.Add1CityCorp = response.Content.Cust.Add1CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CityCorp, specialChar) : null;
                        response.Content.Cust.Add2CountryCorp = response.Content.Cust.Add2CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CountryCorp, specialChar) : null;
                        response.Content.Cust.Add2StateCorp = response.Content.Cust.Add2StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2StateCorp, specialChar) : null;
                        response.Content.Cust.Add2CityCorp = response.Content.Cust.Add2CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;

                        for (int i = 0; i < response.Content.ShareHolders.Count; i++)
                        {
                            response.Content.ShareHolders[i].ShareHolderName = response.Content.ShareHolders[i].ShareHolderName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderName, specialChar) : null;
                            response.Content.ShareHolders[i].ShareHolderAddress = response.Content.ShareHolders[i].ShareHolderAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderAddress, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofIncorporation = response.Content.ShareHolders[i].CountryofIncorporation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofIncorporation, specialChar) : null;
                            response.Content.ShareHolders[i].AuthorisedSignatory = response.Content.ShareHolders[i].AuthorisedSignatory != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].AuthorisedSignatory, specialChar) : null;
                            response.Content.ShareHolders[i].BeneficialOwner = response.Content.ShareHolders[i].BeneficialOwner != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].BeneficialOwner, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofResidence = response.Content.ShareHolders[i].CountryofResidence != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofResidence, specialChar) : null;
                            response.Content.ShareHolders[i].SHCity = response.Content.ShareHolders[i].SHCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCity, specialChar) : null;
                            response.Content.ShareHolders[i].SHState = response.Content.ShareHolders[i].SHState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHState, specialChar) : null;
                            response.Content.ShareHolders[i].SHCountry = response.Content.ShareHolders[i].SHCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.Authignatories.Count; i++)
                        {
                            response.Content.Authignatories[i].AuthorisedsignatoriesName = response.Content.Authignatories[i].AuthorisedsignatoriesName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].AuthorisedsignatoriesName, specialChar) : null;
                            response.Content.Authignatories[i].CorrespondenceAddress = response.Content.Authignatories[i].CorrespondenceAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].CorrespondenceAddress, specialChar) : null;
                            response.Content.Authignatories[i].Remarks = response.Content.Authignatories[i].Remarks != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].Remarks, specialChar) : null;
                            response.Content.Authignatories[i].ASCity = response.Content.Authignatories[i].ASCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCity, specialChar) : null;
                            response.Content.Authignatories[i].ASState = response.Content.Authignatories[i].ASState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASState, specialChar) : null;
                            response.Content.Authignatories[i].ASCountry = response.Content.Authignatories[i].ASCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BoardDirectors.Count; i++)
                        {
                            response.Content.BoardDirectors[i].BoardDirectorName = response.Content.BoardDirectors[i].BoardDirectorName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorName, specialChar) : null;
                            response.Content.BoardDirectors[i].Designation = response.Content.BoardDirectors[i].Designation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].Designation, specialChar) : null;
                            response.Content.BoardDirectors[i].BoardDirectorAddress = response.Content.BoardDirectors[i].BoardDirectorAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorAddress, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCity = response.Content.BoardDirectors[i].BDCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCity, specialChar) : null;
                            response.Content.BoardDirectors[i].BDState = response.Content.BoardDirectors[i].BDState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDState, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCountry = response.Content.BoardDirectors[i].BDCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BeneficialOwners.Count; i++)
                        {
                            response.Content.BeneficialOwners[i].BeneficialName = response.Content.BeneficialOwners[i].BeneficialName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialName, specialChar) : null;
                            response.Content.BeneficialOwners[i].BeneficialAddress = response.Content.BeneficialOwners[i].BeneficialAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialAddress, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCity = response.Content.BeneficialOwners[i].BOCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCity, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOState = response.Content.BeneficialOwners[i].BOState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOState, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCountry = response.Content.BeneficialOwners[i].BOCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCountry, specialChar) : null;
                        }
                    }
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("InquireFIKycLiteCustomerV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>", typeof(ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>))]
        public async Task<IActionResult> InquireFIKycLiteCustomerV2([FromBody] InquireFIKycRimInquiryV2 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2> response = new ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireFIKycLiteCustomerV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp").Value.ToString();
                    channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }




        [Route("AddUpdateShareHolderV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddUpdateShareHolderV2([FromBody] CIFShareHolderDetiilDTOV2 Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolderV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetPreAccountClosure")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<GetPreAccountClosureResponse>))]
        public async Task<IActionResult> GetPreAccountClosure([FromBody] GetPreAccountClosure Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<GetPreAccountClosureResponse> response = new ActiveResponseSucces<GetPreAccountClosureResponse>();


            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetPreAccountClosure"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.RequestDateTime = Model.SignOnRq.DateTime;
            response.LogId = Model.SignOnRq.LogId;
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetPreAccountClosureResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null,response);
                }
            }

            return BadRequest(response);
        }



        [Route("GetCustomerIDExpiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>", typeof(ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>))]
        public async Task<IActionResult> GetCustomerIDExpiry([FromBody] GetCustomerIDExpiryDto Model)
        {
            var dto = Utility.ReadFromHeaders(this.HttpContext);
            Model.SignOnRq = dto.SignOnRq;
            ActiveResponseSucces<List<GetCustomerIDExpiryResponse>> response = new ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCustomerIDExpiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            response.LogId = Model.SignOnRq.LogId;
            response.RequestDateTime = Model.SignOnRq.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        #endregion
    }
}
