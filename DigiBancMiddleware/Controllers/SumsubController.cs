﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SumsubController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        public readonly IConfiguration _config;
        private Level level = Level.Info;

        public SumsubController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [Route("CreateApplicant")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<SumsubHandler.CreateApplicant.ResponseTokenGen>", typeof(ActiveResponseSucces<SumsubHandler.CreateApplicant.ResponseCreateApplicant>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SumsubHandler.CreateApplicant.ErrorResponse>", typeof(ActiveResponseSucces<SumsubHandler.CreateApplicant.ErrorResponse>))]
        public async Task<IActionResult> CreateApplicant([FromBody] SumsubHandler.CreateApplicant.RequestCreateApplicant Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                       _config["SumsubAPI:BaseUrl"].ToString(),
                                                                        _config["SumsubAPI:CreateApplicant"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 22);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.httpResult?.StatusCode == HttpStatusCode.Created)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);
            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };

            return BadRequest(response);
        }


        [Route("AddIDDocument")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<SumsubHandler.AddIdDocument.ResponseAddIdDocument>", typeof(ActiveResponseSucces<SumsubHandler.AddIdDocument.ResponseAddIdDocument>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SumsubHandler.AddIdDocument.ErrorResponseAddIdDocument>", typeof(ActiveResponseSucces<SumsubHandler.AddIdDocument.ErrorResponseAddIdDocument>))]
        public async Task<IActionResult> AddIDDocument([FromBody] SumsubHandler.AddIdDocument.RequestAddIdDocument Model)
        {
            var formContent = new MultipartFormDataContent();
            formContent.Add(new StringContent(JsonConvert.SerializeObject(Model.metadata)), "\"metadata\"");
            var binaryImage = System.Convert.FromBase64String(Model.documentData);
            formContent.Add(new StreamContent(new MemoryStream(binaryImage)), "content", Model.documentName);


            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, formContent,
                                                                       _config["SumsubAPI:BaseUrl"].ToString(),
                                                                       string.Format(_config["SumsubAPI:AddIDDocument"].ToString(), Model.applicantId),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 33,55);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;

            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);

            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };
            return BadRequest(response);
        }

        [Route("TokenGen")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(201, "ActiveResponseSucces<SumsubHandler.TokenGen.ResponseTokenGen>", typeof(ActiveResponseSucces<SumsubHandler.TokenGen.ResponseTokenGen>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SumsubHandler.TokenGen.ErrorResponse>", typeof(ActiveResponseSucces<SumsubHandler.TokenGen.ErrorResponse>))]
        public async Task<IActionResult> TokenGen([FromBody] SumsubHandler.TokenGen.RequestTokenGen Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                       _config["SumsubAPI:BaseUrl"].ToString(),
                                                                       string.Format(_config["SumsubAPI:TokenGen"].ToString(), Model.applicantId),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 22);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return CreatedAtAction(null, response);

            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };

            return BadRequest(response);
        }

        /// <summary>
        /// Get Applicant Status
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     Following are parameter and values
        ///     
        ///        applicantId : 6079cf8d5a8c28000b80f42c
        ///     
        ///
        /// </remarks>
        [Route("GetApplicantStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpGet]
        [SwaggerResponse(200, "ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ResponseGetApplicantStatus>", typeof(ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ResponseGetApplicantStatus>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ErrorResponse>", typeof(ActiveResponseSucces<SumsubHandler.GetApplicantStatus.ErrorResponse>))]
        public async Task<IActionResult> GetApplicantStatus([FromQuery] SumsubHandler.GetApplicantStatus.RequestGetApplicantStatus Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                       _config["SumsubAPI:BaseUrl"].ToString(),
                                                                       string.Format(_config["SumsubAPI:GetApplicantStatus"].ToString(), Model.applicantId),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 22, 2);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return Ok(response);

            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };

            return BadRequest(response);
        }



        [Route("UpdateApplicantData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPatch]
        [SwaggerResponse(200, "ActiveResponseSucces<SumsubHandler.UpdateApplicantData.UpdateApplicantDataResponse>", typeof(ActiveResponseSucces<SumsubHandler.UpdateApplicantData.UpdateApplicantDataResponse>))]
        [SwaggerResponse(400, "ActiveResponseSucces<SumsubHandler.UpdateApplicantData.UpdateApplicantDataErrorResponse>", typeof(ActiveResponseSucces<SumsubHandler.UpdateApplicantData.UpdateApplicantDataErrorResponse>))]
        public async Task<IActionResult> UpdateApplicantData([FromBody] SumsubHandler.UpdateApplicantData.UpdateApplicantDataRequest Model)
        {
            ActiveResponseSucces<object> response = new ActiveResponseSucces<object>();
            InternalResult result = await APIRequestHandler.GetResponse(null, Model,
                                                                       _config["SumsubAPI:BaseUrl"].ToString(),
                                                                       string.Format(_config["SumsubAPI:UpdateApplicantData"].ToString(), Model.applicantId),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 22,36);

            response.RequestDateTime = Convert.ToDateTime(this.HttpContext.Request.Headers["DateTime"]);
            response.LogId = this.HttpContext.Request.Headers["LogId"];
            var obj = JsonConvert.DeserializeObject<object>(result.httpResult.httpResponse);
            response.Content = obj;
            if (result.httpResult?.StatusCode == HttpStatusCode.OK)
            {
                response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };
                return Ok(response);
            }

            response.Status = new DBHandler.Helper.Status() { Code = "ERROR-01", Severity = Severity.Error, StatusMessage = "Error" };


            return BadRequest(response);
        }
    }
}
