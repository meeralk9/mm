﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler.Model;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MoneyThorHandler.Model;
using MoneyThorHandler.Model.Promotion;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using VendorApi.Helper;

namespace DigiBancMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoneyThorController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private IHttpClientFactory _httpClientFactory;

        public MoneyThorController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
            _httpClientFactory = httpClientFactory;
        }
        [Route("GetBalance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetBalance([FromBody] GetBalance Model)
        {
            DBHandler.Helper.ActiveResponseSucces<RootObjectBalance> response = new DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
           
            body.Add("min_date", Model.min_date == null ? null : Model.min_date.Value.ToString("YYYY-MM-DD"));
            body.Add("max_date", Model.max_date == null ? null : Model.max_date.Value.ToString("YYYY-MM-DD"));
            body.Add("accounts", Model.AccountKeys == null ? null : string.Join(",", Model.AccountKeys));
            body.Add("account_type", Model.AccountType);
            body.Add("currency", Model.Currency);
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:GetBalance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66, "", Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<RootObjectBalance>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }

        [Route("SearchTransactions")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> SearchTransactions([FromBody] SearchTransactionsDto Model)
        {
            DBHandler.Helper.ActiveResponseSucces<RootObjectSearchtransaction> response = new DBHandler.Helper.ActiveResponseSucces<RootObjectSearchtransaction>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("Key", Model.Key);
            body.Add("similar", Model.similar == null ? "" : Model.similar.ToString());
            body.Add("account_type", Model.account_type);
            body.Add("accounts", Model.accounts == null ? "" : string.Join(",", Model.accounts));
            body.Add("movement", Model.movement);
            body.Add("status", Model.status);
            body.Add("query", Model.query);
            body.Add("currency", Model.currency);
            body.Add("min_date", Model.min_date == null ? "" : Model.min_date.Value.ToString("YYYY-MM-DD"));
            body.Add("max_date", Model.max_date == null ? "" : Model.max_date.Value.ToString("YYYY-MM-DD"));
            body.Add("min_debit_amount", Model.min_debit_amount == null ? "" : Model.min_debit_amount.ToString());
            body.Add("max_debit_amount", Model.max_debit_amount == null ? "" : Model.max_debit_amount.ToString());
            body.Add("min_credit_amount", Model.min_credit_amount == null ? "" : Model.min_credit_amount.ToString());
            body.Add("max_credit_amount", Model.max_credit_amount == null ? "" : Model.max_credit_amount.ToString());
            //body.Add("days_of_week", Model.days_of_week==null?null: string.Join(",", Model.days_of_week));
            body.Add("min_authorization_date", Model.min_authorization_date == null ? "" : Model.min_authorization_date.Value.ToString("YYYY-MM-DD"));
            body.Add("max_authorization_date", Model.max_authorization_date == null ? "" : Model.max_authorization_date.Value.ToString("YYYY-MM-DD"));
            body.Add("min_authorization_time", Model.min_authorization_time == null ? "" : Model.min_authorization_time.Value.ToString("hh:mm:ss.sssZ"));
            body.Add("max_authorization_time", Model.max_authorization_time == null ? "" : Model.max_authorization_time.Value.ToString("hh:mm:ss.sssZ"));
            body.Add("sort", Model.sort == null ? "" : Model.sort.ToString());
            body.Add("order", Model.order);
            body.Add("page", Model.page == null ? "" : Model.page.ToString()); ;
            body.Add("count", Model.count == null ? "" : Model.count.ToString());
            body.Add("expand_tip", Model.expand_tip == null ? "" : Model.expand_tip.ToString());
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:searchtransactions"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17,66,"",Model.customerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<RootObjectSearchtransaction>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("TrackEvents")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> TrackEvents([FromBody] TrackEventDto Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TrackEventResponse.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TrackEventResponse.RootObject>();
            #region body
         
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.Root,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:TrackEvents"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16,13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TrackEventResponse.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }



        }
        [Route("synccustomfield")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> synccustomfield([FromBody] synccustomfieldDto Model)
        {
            DBHandler.Helper.ActiveResponseSucces<responsesynccustomField> response = new DBHandler.Helper.ActiveResponseSucces<responsesynccustomField>();
            #region body

            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.synccustomfield,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:SyncCustomfields"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<responsesynccustomField>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("GetTimeLine")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetTimeLine([FromBody] TimeLineDTO Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TimeLine.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TimeLine.RootObject>();
            DateTime endDate = DateTime.Today;
            DateTime startDate = new DateTime();
            var currentMonthStart = DateTime.Today.AddDays(-((DateTime.Now.Date.Day) - 1));
            if (string.IsNullOrEmpty(Model.Months))
            {
                startDate = currentMonthStart.AddMonths(-6);
            }
            else
            {
                int month = int.Parse(Model.Months);
                startDate = currentMonthStart.AddMonths(-month);
            }
            if (Model.StartDate != null && Model.EndDate != null)
            {
                startDate = DateTime.Parse(Model.StartDate.ToString());
                endDate = DateTime.Parse(Model.EndDate.ToString());
            }
            var charRounding = int.Parse(_config["GlobalSettings:DescriptionLimit"]);
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("count", 9999.ToString());
            body.Add("min_date", startDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", endDate.ToString("yyyy-MM-dd"));
            body.Add("accounts", Model.CustomerAccount);

            if (!string.IsNullOrEmpty(Model.query))
            {
                body.Add("query", Model.query);
            }

            if (!string.IsNullOrEmpty(Model.minAmount) && !string.IsNullOrEmpty(Model.maxAmount))
            {
                body.Add("min_debit_amount", Model.minAmount);
                body.Add("max_debit_amount", Model.maxAmount);
                body.Add("min_credit_amount", Model.minAmount);
                body.Add("max_credit_amount", Model.maxAmount);
            }
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:TimeLine"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TimeLine.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("CreateCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> CreateCustomer([FromBody] MoneyThorCustomer Model)
        {
            DBHandler.Helper.ActiveResponseSucces<CreateCustomerResponse> response = new DBHandler.Helper.ActiveResponseSucces<CreateCustomerResponse>();
            #region body

            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.CustomerData,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:CreateCustomer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 18, 13);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<CreateCustomerResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


            return BadRequest(response);


        }
        [Route("GetBudgets")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetBudgets([FromBody] BudgetRequest Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.RootObject>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.StartDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.EndDate.ToString("yyyy-MM-dd"));
            if (!string.IsNullOrEmpty(Model.budgetKey))
                body.Add("key", Model.budgetKey);
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:GetBudgets"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66, "", Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }

        }
        [Route("AddTransaction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> AddTransaction([FromBody] MoneyThorTransfer Model)
        {
            DBHandler.Helper.ActiveResponseSucces<CreateCustomerResponse> response = new DBHandler.Helper.ActiveResponseSucces<CreateCustomerResponse>();
            #region body
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.transfer,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:CreateCustomer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 18, 13);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<CreateCustomerResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }

        [Route("CreateBudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> CreateBudget([FromBody] CreateBudgetModel Model)
        {
            DBHandler.Helper.ActiveResponseSucces<BudgetResponse> response = new DBHandler.Helper.ActiveResponseSucces<BudgetResponse>();
            #region body
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.budget,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:createbudget"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<BudgetResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }



        }

        [Route("DeleteBudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> DeleteBudget([FromBody] DeleteBudgetModel Model)
        {
            DBHandler.Helper.ActiveResponseSucces<BudgetResponse> response = new DBHandler.Helper.ActiveResponseSucces<BudgetResponse>();
            #region body
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.budget,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:deletebudget"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<BudgetResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }



        }

        [Route("Updatebudget")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> Updatebudget([FromBody] UpdateBudgetModel Model)
        {
            DBHandler.Helper.ActiveResponseSucces<BudgetResponse> response = new DBHandler.Helper.ActiveResponseSucces<BudgetResponse>();
            #region body
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.budget,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:Updatebudget"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<BudgetResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("AddTransactionDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> AddTransactionDetail([FromBody] TransactionDetailDTO Model)
        {
            DBHandler.Helper.ActiveResponseSucces<CustomFieldResponse> response = new DBHandler.Helper.ActiveResponseSucces<CustomFieldResponse>();
            #region body
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.transactionDetail,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:AddTransactionDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<CustomFieldResponse>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }
        }

        [Route("GetPromotions")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetPromotions([FromBody] PromotionRequest Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.Promotion.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.Promotion.RootObject>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.min_date.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.max_date.ToString("yyyy-MM-dd"));
            body.Add("type", Model.type);
            body.Add("family", Model.family);
            body.Add("match", ".*");
            body.Add("target", "statement");
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:GetPromotions"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.Promotion.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("GetClientCategories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetClientCategories([FromBody] CategoryModel Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.Category.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.Category.RootObject>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
  
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:getcategories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.Category.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }

        [Route("GetTotalsByCategory")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetTotalsByCategory([FromBody] TotalCategoryRequest Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TotalCategory.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TotalCategory.RootObject>();
            #region body
            //List<string> accounts = new List<string>();
            //accounts.Add(Model.MtAccount);
            //var accountbody = JsonConvert.SerializeObject(accounts);

            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.StartDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.EndDate.ToString("yyyy-MM-dd"));
            body.Add("movement", Model.movement);
            body.Add("accounts", Model.MtAccount);
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:gettotalsbycategory"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TotalCategory.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("GetTransactionDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetTransactionDetails([FromBody] TransactionSearchModel Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.SingleTransactionDetail.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.SingleTransactionDetail.RootObject>();
            #region body
          
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.GetTransactionDetails,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:GetTransactionDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 16, 13,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.SingleTransactionDetail.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }
        }
        [Route("GetTipsByPattern")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject> ", typeof(DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject>))]
        public async Task<IActionResult> GetTipsByPattern([FromBody] TotalCategoryRequest Model)
        {
            DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject> response = new DBHandler.Helper.ActiveResponseSucces<MoneyThorHandler.Model.TipsByPattern.RootObject>();
            #region body
            Dictionary<string, string> body = new Dictionary<string, string>();
            body.Add("min_date", Model.StartDate.ToString("yyyy-MM-dd"));
            body.Add("max_date", Model.EndDate.ToString("yyyy-MM-dd"));
            body.Add("type", "regexp");
            body.Add("match", ".*");
            body.Add("target", Model.taget);
            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : body,
                                                                        _config["MoneyThor:BaseUrl"].ToString(),
                                                                        _config["MoneyThor:gettipsbypattern"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 66,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = JsonConvert.DeserializeObject<MoneyThorHandler.Model.TipsByPattern.RootObject>(result.httpResult.httpResponse);
            if (result != null && response.Content.header.success == true)
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = response.Content.header.message?.ToString(), Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }
        [Route("GetTip")] 
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ECSFinHandler.validateSingleDebitSingleCreditModel.RootObjectResponse> ", typeof(DBHandler.Helper.ActiveResponseSucces<RootObjectBalance>))]
        public async Task<IActionResult> GetTip([FromBody] TipRequest Model)
        {
            DBHandler.Helper.ActiveResponseSucces<string> response = new DBHandler.Helper.ActiveResponseSucces<string>();
            #region body

            #endregion body
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model,
                                                                        _config["MoneyThor:BaseUrl"].ToString() + "tip?tip_key=" + Model.tip,
                                                                        "",
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 17, 2,"",Model.CustomerId);
            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;
            if (result.Status.Code != null)
            {
                response.Status = result.Status;
                return BadRequest(response);
            }
            response.Content = result.httpResult.httpResponse;
            if (result != null && result.httpResult.severity == "Success")
            {
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };
                return Ok(response);
            }
            else
            {
                level = Level.Error;
                response.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "", Code = "ERROR-03" };
                response.Content = null;
                return BadRequest(response);
            }


        }

    }
}