﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperationsPortalController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;

        private Exception excetionForLog;

        public OperationsPortalController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }
        
        [Route("GetGLPostingAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetGLPostingAccount([FromBody] GetGLPostingAccountDto Model)
        {
            ActiveResponseSucces<DBHandler.Model.OperationsPortal.GetGLPostingAccountResponse> response = new ActiveResponseSucces<DBHandler.Model.OperationsPortal.GetGLPostingAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetGLPostingAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                         httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.OperationsPortal.GetGLPostingAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("inc_F2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> inc_F2([FromBody] inc_F2Dto Model)
        {
            ActiveResponseSucces<DBHandler.Model.OperationsPortal.inc_F2Response> response = new ActiveResponseSucces<DBHandler.Model.OperationsPortal.inc_F2Response>();

            InternalResult result = await APIRequestHandler.GetResponse(Model.signOnRq == null ? null : Model.signOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:inc_F2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 19);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.OperationsPortal.inc_F2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
    }
}