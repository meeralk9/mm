﻿using RailsBankHandler.AddBeneficiaryAcct;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.AddBeneficiaryAcctRequestExample
{
    public class AddBeneficiaryAcctRequest : IExamplesProvider<RailsBankHandler.AddBeneficiaryAcct.RequestAddBeneficiaryAcct>
    {
        public RequestAddBeneficiaryAcct GetExamples()
        {
            return new RequestAddBeneficiaryAcct()
            {
                beneficiary_id = "60321116-227b-4219-bf2e-cd7096d6385b",
                iban = "SK4402005678901234567890",
                account_number = "45564658",
                bank_country = "GB",
                bank_code = "45564658",
                bank_name = "Bank of America",
                account_type = "checking",
                bic_swift = "SPSRSKBAXXX",
                asset_type = "eur",
                asset_class = "currency",
                bank_code_type = "bsb",
            };
        }
    }
}
