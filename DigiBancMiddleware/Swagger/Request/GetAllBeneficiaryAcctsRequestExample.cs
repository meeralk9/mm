﻿using RailsBankHandler.GetAllBeneficiaryAccts;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetAllBeneficiaryAcctsRequestExample
{
    public class GetAllBeneficiaryAcctsRequest : IExamplesProvider<RailsBankHandler.GetAllBeneficiaryAccts.RequestGetAllBeneficiaryAccts>
    {
        public RequestGetAllBeneficiaryAccts GetExamples()
        {
            return new RequestGetAllBeneficiaryAccts()
            {
                beneficiary_id = "602911b8-76e9-4ed3-9ae5-02bb92765734"
            };
        }
    }
}
