﻿using RailsBankHandler.UpdateBeneficiary;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.UpdateBeneficiaryRequestExample
{
    public class UpdateBeneficiaryRequest : IExamplesProvider<RailsBankHandler.UpdateBeneficiary.RequestUpdateBeneficiary>
    {
        public RequestUpdateBeneficiary GetExamples()
        {
            return new RequestUpdateBeneficiary()
            {
                beneficiary_id = "60290040-cef8-4cb6-a02b-17198ca358d1",
                    holder_id = "",
                    person = new Person()
                    {
                        country_of_residence = new List<string> { "USA" },
                        document_number = "000 000 000",
                        document_type = "passport",
                        income = new Income()
                        {
                            amount = "0",
                            frequency = "annual",
                            currency = "EUR"
                        },
                        pep_type = "",
                        pep_notes = "",
                        address_history = new List<object>() { },
                        pep = true,
                        date_onboarded = "2011-11-21",
                        email = "alice@short.com",
                        tin = "541571",
                        name = "Bob",
                        address = new Address()
                        {
                            address_iso_country = "GB"
                        },

                        social_security_number = "090606",
                        telephone = "0012345678912",
                        date_of_birth = "2020-12-03",
                        tin_type = "type",
                        citizenship = new List<string>() { "USA" },
                        nationality = new List<string>() { "Hungarian" },
                        document_issuer = "USA",
                        country_of_birth = "USA"
                    },

                    asset_type = "eur",
                    uk_account_number = "45564658",
                    asset_class = "currency",
                    default_account = new DefaultAccount()
                    {
                        iban = "SK4402005678901234567890",
                        account_number = "45564658",
                        bank_country = "US",
                        bank_code = "45564658",
                        bank_name = "Bank of America",
                        account_type = "checking",
                        bic_swift = "SPSRSKBAXXX",
                        asset_type = "eur",
                        asset_class = "currency",
                        bank_code_type = "bsb"
                    }
            };
        }
    }
}

