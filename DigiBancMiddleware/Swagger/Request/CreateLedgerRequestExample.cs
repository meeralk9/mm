﻿using RailsBankHandler.LegdersPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.CreateLedgerRequestExample
{
    public class CreateLedgerRequest : IExamplesProvider<RailsBankHandler.LegdersPost.RequestLegdersPost>
    {
        public RequestLegdersPost GetExamples()
        {
            return new RequestLegdersPost()
            {
                ledger_primary_use_types = new List<string>()
                    {
                        "ledger-primary-use-types-payments",
                        "ledger-primary-use-types-payments",
                        "ledger-primary-use-types-payments"
                    },

                //credit_details_id = null,
                ledger_who_owns_assets = "ledger-assets-owned-by-me",
                holder_id = "602392ca-b7dd-4b03-bcda-a47208a307fc",
                ledger_t_and_cs_country_of_jurisdiction = "GB",
                partner_product = "ExampleBank-EUR-1",
                asset_type = "eur",
                asset_class = "currency",
                ledger_type = "ledger-type-single-user",

                ledger_meta = new LedgerMeta()
                {
                    foo = "bar"
                }
            };
        }
    }
}
