﻿using DBHandler.Helper;
using RailsBankHandler.CardPost;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.CreateCardRequestExample
{
    public class CreateCardRequest : IExamplesProvider<RailsBankHandler.CardPost.RequestPostCard>
    {
        public RequestPostCard GetExamples()
        {
            return new RequestPostCard()
            {
                    ledger_id = "602178eb-6a99-49ee-a279-02e622721af0",
                    name_on_card="short",
                    card_carrier_type= "standard",
                    card_delivery_name="short",
                    qr_code_content="Any reasonably long string, e.g. description of something",
                    telephone="0012345678912",
                    card_type="virtual",

                    card_rules = new List<string>()
                    {
                        "c91b339e-57d7-41ea-a805-8966ce8fe4ed",
            "c91b339e-57d7-41ea-a805-8966ce8fe4ed",
            "bb8b2428-f94c-41df-8e82-a895ab4d6ac8"
                    },

                    card_delivery_method="dhl",
                    card_design= "some-chars",
                    card_delivery_address = new CardDeliveryAddress()
                    {
                        address_number = "47",
                        address_refinement="Apartment 17",
                        address_region="Oregon",
                        address_iso_country="US",
                        address_street="Long Street",
                        address_city="Los Angeles",
                        address_postal_code="123456"

                    },

                    card_programme="602177f7-6e3e-4dac-b69b-2ef1a6f9ec71"
            };
        }

        public class CreateCardResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.CardPost.ResponseObject>>
        {
            public ActiveResponseSucces<ResponseObject> GetExamples()
            {
                return new ActiveResponseSucces<ResponseObject>() {
                    LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                    Status = new Status()
                    {
                        Code = "MSG-000000",
                        Severity = "Success",
                        StatusMessage = "Success"
                    },
                    Content = new RailsBankHandler.CardPost.ResponseObject()
                    {
                        card_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                    }
                };
            }
        }
    }
}
