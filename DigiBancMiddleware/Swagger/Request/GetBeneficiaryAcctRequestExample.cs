﻿using RailsBankHandler.GetBeneficiaryAcct;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetBeneficiaryAcctRequestExample
{
    public class GetBeneficiaryAcctRequest : IExamplesProvider<RailsBankHandler.GetBeneficiaryAcct.RequestGetBeneficiaryAcct>
    {
        public RequestGetBeneficiaryAcct GetExamples()
        {
            return new RequestGetBeneficiaryAcct() { account_id = "602911b8-76e9-4ed3-9ae5-02bb92765734", beneficiary_id = "602911b8-00c2-494d-b242-422e71798054" };
        }
    }
}
