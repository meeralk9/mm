﻿using RailsBankHandler.EndUserPut;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class UpdateEndUserRequestExample : IExamplesProvider<RailsBankHandler.EndUserPut.RequestEndUsersPut>
    {
        public RequestEndUsersPut GetExamples()
        {
            return new RequestEndUsersPut()
            {
                enduser_id = "6023832f-abed-4981-95c1-29a351824515",
                    person = new Person()
                    {
                        country_of_residence = new List<string>() { "USA" },
                        document_number = "000 000 000",
                        document_type = "passport",

                        income = new Income()
                        {
                            frequency = "annual",
                            currency = "EUR",
                            amount = "100",
                        },
                        //pep_type = "direct",
                        //pep_notes = "comment",

                        address_history = new List<AddressHistory>()
                            {
                                new AddressHistory()
                                {
                                 address_end_date = "2099-12-31",
                                 address_start_date = "2000-01-01",
                                 address_iso_country = "GB",
                                },

                            },

                        //pep = true,
                        date_onboarded = "2011-11-21",
                        email = "alice@short.com",
                        tin = "541571",
                        document_issue_date = "2000-01-01",
                        name = "Bob",

                        address = new Address()
                        {
                            address_region = "California",
                            address_iso_country = "GB",
                            address_number = "47",
                            address_postal_code = "123456",
                            address_refinement = "Apartment 42",
                            address_street = "Riverside Drive",
                            address_city = "Bratislava"
                        },

                        social_security_number = "090606",
                        telephone = "0012345678912",
                        date_of_birth = "1981-02-03",
                        document_expiration_date = "2020-12-03",
                        tin_type = "type",
                        citizenship = new List<string>()
                            {
                                "USA"
                            },
                        nationality = new List<string>()
                            { "Hungarian"},
                        document_issuer = "USA",
                        country_of_birth = "USA"
                    },

                    enduser_meta = new EnduserMeta()
                    {
                        foo = "bar"
                    }
            };
        }
    }
 }
