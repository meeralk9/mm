﻿using RailsBankHandler.GetBeneficiaries;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetBeneficiariesRequestExample
{
    public class GetBeneficiariesRequest : IExamplesProvider<RailsBankHandler.GetBeneficiaries.RequestGetBeneficiaries>
    {
        public RequestGetBeneficiaries GetExamples()
        {
            return new RequestGetBeneficiaries()
            {
                holder_id = null,
                items_per_page = "2",
                offset = "2",
                from_date = null,
                to_date = null,
                order = null
            };
        }
    }
}
