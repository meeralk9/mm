﻿using DBHandler.Helper;
using RailsBankHandler.End;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.EndUserWaitRequestExample
{
    public class EndUserWaitRequest : IExamplesProvider<RailsBankHandler.End.RequestEnd>
    {
        //EndUserWait
        public RailsBankHandler.End.RequestEnd GetExamples()
        {
            return new RailsBankHandler.End.RequestEnd()
            {
                clientID = "6023832f-abed-4981-95c1-29a351824515",               
            };

        }
    }
}
