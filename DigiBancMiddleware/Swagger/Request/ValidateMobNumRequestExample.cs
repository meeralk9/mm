﻿using PostCoderHandler.GetMobileValidate;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.ValidateMobNumRequestExample
{
    public class ValidateMobNumRequest : IExamplesProvider<PostCoderHandler.GetMobileValidate.RequestGetMobileValidate>
    {
        public RequestGetMobileValidate GetExamples()
        {
            return new RequestGetMobileValidate()
            { mobilevalidate = "07500123456" };
        }
    }
}
