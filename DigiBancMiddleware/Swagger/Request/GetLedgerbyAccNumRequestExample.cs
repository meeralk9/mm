﻿using RailsBankHandler.GetLedgerbyAcctNum;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request
{
    public class GetLedgerbyAccNumRequestExample : IExamplesProvider<RailsBankHandler.GetLedgerbyAcctNum.RequestLedgerbyAcctNum>
    {
        public RequestLedgerbyAcctNum GetExamples()
        {
            return new RequestLedgerbyAcctNum() { 
                holder_id = "",
                partner_product="",
                account_number="",
                items_per_page="2",
                offset="2",
                from_date="",
                to_date="",
                order = ""
             };
        }
    }
}
