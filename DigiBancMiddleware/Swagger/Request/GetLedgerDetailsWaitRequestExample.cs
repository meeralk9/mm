﻿using RailsBankHandler.GetLedgersWait;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Request.GetLedgerDetailsWaitRequestExample
{
    public class GetLedgerDetailsWaitRequest : IExamplesProvider<RailsBankHandler.GetLedgersWait.RequestGetLegdersWait>
    {
        public RequestGetLegdersWait GetExamples()
        {
            return new RequestGetLegdersWait()
            {
               ledger_id = "60238d5a-2deb-4ced-93b2-4555b35e76fa"
            };
        }
    }
}
