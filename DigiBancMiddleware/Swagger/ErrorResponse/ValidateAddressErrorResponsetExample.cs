﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.ValidateAddressErrorResponsetExample
{
    public class ValidateAddressErrorResponset : IExamplesProvider<ActiveResponseSucces<PostCoderHandler.GetAddress.ErrorResponse>>
    {
        public ActiveResponseSucces<PostCoderHandler.GetAddress.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<PostCoderHandler.GetAddress.ErrorResponse>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = "No data found"
                },
                Content = new PostCoderHandler.GetAddress.ErrorResponse()
                {
                    
                }
            };
        }
    }
}
