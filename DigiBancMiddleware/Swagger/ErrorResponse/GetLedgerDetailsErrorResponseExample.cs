﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.GetLedgerDetailsErrorResponseExample
{
    public class GetLedgerDetailsErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgers.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetLedgers.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetLedgers.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.GetLedgers.ErrorResponse()
                {
                    error = "Invalid ledger id"
                }
            };
        }
    }
}
