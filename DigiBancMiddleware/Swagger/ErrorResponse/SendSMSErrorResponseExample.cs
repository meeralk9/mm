﻿using CommonModel.Twilio;
using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.SendSMSErrorResponseExample
{
    public class SendSMSErrorResponse : IExamplesProvider<ActiveResponseSucces<CommonModel.Twilio.ErrorResponse>>
    {
        public ActiveResponseSucces<CommonModel.Twilio.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<CommonModel.Twilio.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = "The 'To' number +72 is not a valid phone number"
                },

                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new CommonModel.Twilio.ErrorResponse()
                {

                }
            };
        }
    }
}
