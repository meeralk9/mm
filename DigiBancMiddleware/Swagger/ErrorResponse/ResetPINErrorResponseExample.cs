﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse
{
    public class ResetPINErrorResponseExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.ResetPIN.ErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.ResetPIN.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.ResetPIN.ErrorResponse>()
            {
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                LogId = "495e2695-82ba-4427-8870-f9a03f31ce4d",
                Content = new RailsBankHandler.ResetPIN.ErrorResponse()
                {
                    error = "field-is-malformed",
                    detail = "field-is-malformed",
                    field = "items_per_page",
                    type = "path"

                }
            };
        }
    }
}
