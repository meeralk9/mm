﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse
{
    public class UpdtDefaultBenAcctErrorResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse>>
    {
        public ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                Content = new RailsBankHandler.UpdtBeneficiaryAcct.UpdtDefaultBenAcctErrorResponse()
                {
                    error = "Invalid account id."
                }
            };
        }
    }
}
