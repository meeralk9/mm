﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.ErrorResponse.ValidateEmailErrorResponseExample
{
    public class ValidateEmailErrorResponse : IExamplesProvider<ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ErrorResponse>>
    {
        public ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ErrorResponse> GetExamples()
        {
            return new ActiveResponseSucces<PostCoderHandler.GetEmailValidate.ErrorResponse>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "ERROR-01",
                    Severity = "Error",
                    StatusMessage = ""
                },
                Content = new PostCoderHandler.GetEmailValidate.ErrorResponse()
                {
                    warning = "Requires Email Address Parameter",
                    state = "No Input Supplied",
                    valid = false
                }
            };
        }
    }
}
