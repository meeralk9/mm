﻿using DBHandler.Helper;
using RailsBankHandler.GetLedgerEntries;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.RailsBank_Examples.GetLedgerEntriesExample
{
    public class RequestGetLedgerEntriesExample : IExamplesProvider<RailsBankHandler.GetLedgerEntries.RequestGetLedgerEntries>
    {
        public RequestGetLedgerEntries GetExamples()
        {
            return new RequestGetLedgerEntries()
            {
                ledger_id = "60238bb5-901d-487a-8787-364405bca218",
                items_per_page = 1,
                offset = 1,
                from_date = null,
                to_date = null,
                order=null,
                ledger_entry_event_type = null
            };
        }
    }

    public class ResponseGetLedgerEntriesExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ResponseGetLedgerEntries>>
    {
        public ActiveResponseSucces<ResponseGetLedgerEntries> GetExamples()
        {
            return new ActiveResponseSucces<ResponseGetLedgerEntries>()
            {
                LogId = "",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new ResponseGetLedgerEntries()
                {
                    ledger_entry_event_type = "financial",
                    //created_at= "2000-01-01T00:00:00.000Z",
                    ledger_entry_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                    transaction_id = "bb8b2428-f94c-41df-8e82-a895ab4d6ac8",
                    amount = "0",
                    ledger_entry_type = "credit"
                }
            };
        }
    }

    public class ErrorResponseGetLedgerEntriesExample : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgerEntries.ErrorResponseGetLedgerEntries>>
    {
        public ActiveResponseSucces<ErrorResponseGetLedgerEntries> GetExamples()
        {
            return new ActiveResponseSucces<ErrorResponseGetLedgerEntries>()
            {
                Status = new Status()
                {
                    Code = "Error-01",
                    Severity = "Error",
                    StatusMessage = "Error"
                },

                Content = new ErrorResponseGetLedgerEntries()
                {
                    error = "Invalid ledger id."
                }
            };
        }
    }
}
