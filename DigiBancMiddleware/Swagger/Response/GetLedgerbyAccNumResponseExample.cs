﻿using DBHandler.Helper;
using RailsBankHandler.GetLedgerbyAcctNum;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetLedgerbyAccNumResponseExample
{
    public class GetLedgerbyAccNumResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject>>
    {
        public ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.GetLedgerbyAcctNum.ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetLedgerbyAcctNum.ResponseObject()
                {
                    ledger_primary_use_types = new List<string>()
                    {
                        "ledger-primary-use-types-payments",
                    },
                    ledger_id = "60238bb5-901d-487a-8787-364405bca218",
                    ledger_holder = new LedgerHolder()
                    {
                        enduser_id = "6021237c-be4b-40c5-abde-532f72518e4b",
                        Person = new Person()
                        {
                            name = "Bob"
                        }
                    },

                    partner_ref = "examplebank",
                    asset_type = "eur",
                    asset_class = "currency",
                    ledger_type = "ledger-type-single-user",

                    ledger_meta = new LedgerMeta()
                    {
                        foo = "bar"
                    }

                }
            };
        }
    }
}
