﻿using DBHandler.Helper;
using RailsBankHandler.UpdtBeneficiaryAcct;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.UpdtBeneficiaryAcctResponseExample
{
    public class UpdtBeneficiaryAcctResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.UpdtBeneficiaryAcct.ResponseObject()
                {
                    account_id = "6035f08e-794c-407f-a179-4d4621f94312",
                }
            };
        }
    }
}
