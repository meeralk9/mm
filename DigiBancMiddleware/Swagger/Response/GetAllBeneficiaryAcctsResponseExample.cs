﻿using DBHandler.Helper;
using RailsBankHandler.GetAllBeneficiaryAccts;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetAllBeneficiaryAcctsResponseExample
{
    public class GetAllBeneficiaryAcctsResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetAllBeneficiaryAccts.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {

                LogId = "fc8a69d1-bbcc-43d2-a30f-30e8c6f621c8",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetAllBeneficiaryAccts.ResponseObject()
                {
                    account_id = "602911b8-00c2-494d-b242-422e71798054"
                }
            };
        }
    }
}
