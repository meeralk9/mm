﻿using DBHandler.Helper;
using RailsBankHandler.GetLedgers;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.GetLedgerDetailsResponseExample
{
    public class GetLedgerDetailsResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.GetLedgers.ResponseObject>>
    {
        public ActiveResponseSucces<ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObject>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.GetLedgers.ResponseObject()
                {
                    ledger_primary_use_types = new List<string>()
                    {
                        "ledger-primary-use-types-payments",
                        "ledger-primary-use-types-payments",
                        "ledger-primary-use-types-payments"
                    },

                    ledger_id = "60238bb5-901d-487a-8787-364405bca218",
                    ledger_holder = new LedgerHolder()
                    {
                        enduser_id = "6021237c-be4b-40c5-abde-532f72518e4b",
                        person = new Person()
                        {
                            name = "Bob"
                        }
                    },

                    ledger_who_owns_assets = "ledger-assets-owned-by-me",
                    holder_id = "602392ca-b7dd-4b03-bcda-a47208a307fc",
                    partner_id= "58fe2ce0-3def-4e08-b778-c121c7f98334",
                    ledger_t_and_cs_country_of_jurisdiction = "GB",
                    ledger_status= "ledger-status-ok",
                    amount = "0",
                    partner_product = "ExampleBank-EUR-1",

                    partner = new Partner()
                    {
                        partner_id  = "58fe2ce0-3def-4e08-b778-c121c7f98334",
                        company = new Company()
                        {
                            name = "Example Bank"
                        }
                       
                    },

                    partner_ref = "examplebank",
                    asset_type = "eur",
                    asset_class = "currency",
                    ledger_type = "ledger-type-single-user",

                    ledger_meta = new LedgerMeta()
                    {
                        foo = "bar"
                    }
                }
            };
        }
    }
}
