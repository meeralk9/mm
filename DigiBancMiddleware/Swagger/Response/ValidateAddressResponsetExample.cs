﻿using DBHandler.Helper;
using PostCoderHandler.GetAddress;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.ValidateAddressResponsetExample
{
    public class ValidateAddressResponse : IExamplesProvider<ActiveResponseSucces<PostCoderHandler.GetAddress.ResponseObj>>
    {
        public ActiveResponseSucces<ResponseObj> GetExamples()
        {
            return new ActiveResponseSucces<ResponseObj>()
            {
                LogId = "1740ecf1-f54e-4fa3-8056-3889157b5bbd",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },

                Content = new PostCoderHandler.GetAddress.ResponseObj()
                {
                    responseObject = new List<ResponseObject>()
                    {
                        new ResponseObject()
                        {
                            summaryline = "Allies Computing Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Allies Computing Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "B 2 B Cashflow Solutions Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "B 2 B Cashflow Solutions Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "Conker Interiors Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Conker Interiors Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "East Anglias Childrens Hospices, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "East Anglias Childrens Hospices",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "Eastern Chauffeurs Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Eastern Chauffeurs Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                         new ResponseObject()
                        {
                            summaryline = "Equal Lives, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Equal Lives",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "Estate Office, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Estate Office",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                        new ResponseObject()
                        {
                            summaryline = "Genesis Lifts Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Genesis Lifts Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                         new ResponseObject()
                        {
                            summaryline = "Heritage Developments Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Heritage Developments Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                         new ResponseObject()
                        {
                            summaryline = "Mancroft International, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Mancroft International",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },


                         new ResponseObject()
                        {
                            summaryline = "Norfolk Wealth Managemant, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Norfolk Wealth Managemant",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                    new ResponseObject()
                        {
                            summaryline = "Paradox, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Paradox",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                    new ResponseObject()
                        {
                            summaryline = "Quadmark Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Quadmark Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                     new ResponseObject()
                        {
                            summaryline = "Serenity Training Ltd, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Serenity Training Ltd",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },

                                          new ResponseObject()
                        {
                            summaryline = "Teach, Manor Farm Barns, Fox Road, Framingham Pigot, Norwich, Norfolk, NR14 7PZ",
                            organisation = "Teach",
                            buildingname = "Manor Farm Barns",
                            premise = "Manor Farm Barns",
                            street = "Fox Road",
                            dependentlocality = "Framingham Pigot",
                            posttown= "Norwich",
                            county = "Norfolk",
                            postcode = "NR14 7PZ",
                            latitude = "52.5859730797",
                            longitude ="1.3491603533",
                            grideasting = "626976",
                            gridnorthing= "303951"
                        },


                }
                }
            };
        }
    }
}
