﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Swagger.Response.CreateLedgerResponseExample
{
    public class CreateLedgerResponse : IExamplesProvider<ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject>>
    {
        public ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject> GetExamples()
        {
            return new ActiveResponseSucces<RailsBankHandler.LegdersPost.ResponseObject>()
            {
                LogId = "fc8a69d1-bbcc-43d2-a30f-30e8c6f621c8",
                Status = new Status()
                {
                    Code = "MSG-000000",
                    Severity = "Success",
                    StatusMessage = "Success"
                },
                Content = new RailsBankHandler.LegdersPost.ResponseObject()
                {
                    ledger_id = "603f3f0a-a654-4ef9-bf65-86fddb3321c2"
                }
            };
        }
    }
}
