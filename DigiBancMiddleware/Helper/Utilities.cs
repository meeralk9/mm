﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using VendorApi.Helper;

namespace DigiBancMiddleware.Helper
{
    public static class Utilities
    {
      
        public static bool IsDMSError(string result)
        {
            List<string> errorlist = new List<string> { "400", "401", "404", "405", "500", "501", "502", "503", "415" };

            if (result.Length>3)
            {
                if(errorlist.Contains(result.Substring(0, 3)) && result.Substring(3, 1) == " ")
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public static bool IsBase64(this string base64String)
        {
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
            || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n"))
            {
                return false;
            }
            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception exception)
            {
                // Handle the exception
            }
            return false;
        }

    }
}
