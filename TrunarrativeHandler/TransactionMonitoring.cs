﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrunarrativeHandler.TransactionMonitoring
{
    public class TransactionMonitoringRequest
    {
        public int accountStrategyId { get; set; }
        public string accountReference { get; set; }
        public FinancialAccount financialAccount { get; set; }
    }
    public class FinancialTransaction
    {
        public string clientReference { get; set; }
        public string effectiveDateTime { get; set; }
        public int Amount { get; set; }
        public string sortCode { get; set; }
        public string accountNumber { get; set; }
        public string creditOrDebit { get; set; }
        public string currencyCode { get; set; }
        public string dateOpened { get; set; }
        public string productName { get; set; }
    }

    public class FinancialAccount
    {
        public List<FinancialTransaction> financialTransaction { get; set; }
    }

    //Response
    public class TransactionMonitoringResponse
    {
        public int accountId { get; set; }
        public Status status { get; set; }
        public Decision decision { get; set; }
        public List<ApplicationResp> Applications { get; set; }
    }

    public class Status
    {
        public string code { get; set; }
        public string label { get; set; }
    }

    public class Decision
    {
        public string code { get; set; }
        public string label { get; set; }
    }

    public class JourneyId
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class OrganisationId
    {
        public int id { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class JourneyType
    {
        public int id { get; set; }
        public string label { get; set; }
    }

    public class ProgressStatus
    {
        public string code { get; set; }
        public string label { get; set; }
    }

    public class RuleOutcome
    {
        public bool outcome { get; set; }
        public int score { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public int versionNumber { get; set; }
        public int revisionNumber { get; set; }
        public int numberOfMatchRecords { get; set; }
    }

    public class RulesetOutcome
    {
        public string reference { get; set; }
        public int score { get; set; }
        public List<RuleOutcome> ruleOutcomes { get; set; }
        public string riskLevel { get; set; }
    }

    public class ApplicationResp
    {
        public string uid { get; set; }
        public string auditReference { get; set; }
        public int runId { get; set; }
        public JourneyId journeyId { get; set; }
        public OrganisationId organisationId { get; set; }
        public string country { get; set; }
        public User user { get; set; }
        public string nameReference { get; set; }
        public string riskLevel { get; set; }
        public JourneyType journeyType { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
        public ProgressStatus progressStatus { get; set; }
        public Decision decision { get; set; }
        public List<RulesetOutcome> rulesetOutcomes { get; set; }
    }

    public class TransactionMonitoringErrorResponse
    {
    }
}
