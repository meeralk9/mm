﻿using CommonModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace ShipaHandler.AWBValidation
{
    public class AWBValidationModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public EIDAndAWB EIDAndAWB { get; set; }
    }

    public class EIDAndAWB
    {
        public string EID { get; set; }
        public string AWB{ get; set; }
    }

    public class Content
    {
        public string message { get; set; }
        public string code { get; set; }
    }

    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public Content content { get; set; }
        public object exceptionMessage { get; set; }
        public object id { get; set; }
        public object showRating { get; set; }
    }

    public class RootObject
    {
        public Response response { get; set; }
    }
}
