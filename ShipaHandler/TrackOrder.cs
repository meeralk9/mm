﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ShipaHandler.TrackOrder
{
    public class TrackOrderModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }
    public class RootObject
    {
        public int userid { get; set; }
        public string action { get; set; }
        public string externalid { get; set; }
    }



    public class Details
    {
        public string estimated_pickup_time { get; set; }
        public string estimated_dropoff_time { get; set; }
        public string delivery_code { get; set; }
        public string package_name { get; set; }
        public string description { get; set; }
        public string confirmation_time { get; set; }
        public string order_place_time { get; set; }
        public string payment_method { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }

    public class From
    {
        public string name { get; set; }
        public string contact_no { get; set; }
        public string address { get; set; }
        public string address_extra_details { get; set; }
        public string coordinates { get; set; }
        public string city { get; set; }
    }

    public class To
    {
        public string name { get; set; }
        public string contact_no { get; set; }
        public string address { get; set; }
        public string address_extra_details { get; set; }
        public string coordinates { get; set; }
        public string city { get; set; }
    }

    public class Driver
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string profile_image { get; set; }
    }

    public class Vehicle
    {
        public string make { get; set; }
        public string plate_no { get; set; }
        public string type { get; set; }
        public string model { get; set; }
        public string color { get; set; }
    }

    public class Track
    {
        public string is_started { get; set; }
        public string is_pickedup { get; set; }
        public string is_droppedoff { get; set; }
        public string is_returned { get; set; }
        public string cancelation_reason { get; set; }
        public string order_status { get; set; }
        public string actual_pickup_time { get; set; }
        public string actual_dropoff_time { get; set; }
        public string driver_location { get; set; }
        public int driver_eta { get; set; }
        public string start_pickup_time { get; set; }
        public string start_dropoff_time { get; set; }
    }

    public class Invoice
    {
        public string carry_invoice_no { get; set; }
        public string invoice_date { get; set; }
        public string business_invoice_no { get; set; }
        public string invoice_status { get; set; }
    }

    public class History
    {
        public string order_status { get; set; }
        public string status_date { get; set; }
    }

    public class Order
    {
        public Details details { get; set; }
        public From from { get; set; }
        public To to { get; set; }
        public Driver driver { get; set; }
        public Vehicle vehicle { get; set; }
        public Track track { get; set; }
        public Invoice invoice { get; set; }
        public string pick_up_collection_method { get; set; }
        public string drop_off_collection_method { get; set; }
        public string pick_up_payment_info { get; set; }
        public string pick_up_payment_status { get; set; }
        public string pick_up_payment_response_code { get; set; }
        public string pick_up_payment_response_code_meaning { get; set; }
        public string drop_off_payment_info { get; set; }
        public string drop_off_payment_status { get; set; }
        public string drop_off_payment_response_code { get; set; }
        public string drop_off_payment_response_code_meaning { get; set; }
        public List<History> history { get; set; }
    }

    public class ResponseRootObject
    {
        public string status { get; set; }
        public Order order { get; set; }
        public int code { get; set; }
        public string error { get; set; }
    }
}
