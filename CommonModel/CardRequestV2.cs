﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.CardRequestV2
{
    public class CardRequestModel
    {
        public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }

    public class RootObject
    {
        public string Address { get; set; }
        public string CompleteAddress { get; set; }
        public string City { get; set; }
        public string UserCardName { get; set; }
        public string NotesToDriver { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CardDeliveryDate { get; set; }
        public string AccountNumber { get; set; }
        public string ReasonCode { get; set; }
    }


    public class Msg
    {
        public string msg { get; set; }
        public int code { get; set; }
        public string awbnumber { get; set; }
        public string cardToken { get; set; }
    }

    public class Content
    {
        public Msg msg { get; set; }
    }

    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public object content { get; set; }
        public string exceptionMessage { get; set; }
        public string id { get; set; }
    }

    public class ResponseRootObject
    {
        public Response response { get; set; }
    }
}
