﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel.Email
{
    public class EmailModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class To
    {
        public string Email { get; set; }
    }

    public class Cc
    {
        public string Email { get; set; }
    }

    public class Bcc
    {
        public string Email { get; set; }
    }

    public class RootObject
    {
        public string senderId { get; set; }
        public string messageId { get; set; }
        public string priority { get; set; }
        public string from { get; set; }
        public List<To> to { get; set; }
        public List<Cc> cc { get; set; }
        public List<Bcc> bcc { get; set; }
        public string subject { get; set; }
        public string type { get; set; }
        public string body { get; set; }
        public string templateId { get; set; }
        public List<CommonModel.DucontEmail.TemplateVariable> templateVariables { get; set; }
        public string attachment { get; set; }
        public string attachementId { get; set; }
        public string attachmentBase64 { get; set; }
        public string attachmentFileName { get; set; }
    }

  
}
