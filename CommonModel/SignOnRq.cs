﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel
{
    /// <summary>
    /// This is default header payload for each  request 
    /// </summary>
    public class SignOnRq
    {
        /// <summary>
        /// Log ID should be uniqe per call.
        /// </summary>
        [Required(ErrorMessage = " is a mandatory field.")]
        public string LogId { get; set; }

        /// <summary>
        /// Request Datetime.
        /// </summary>
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateTime { get; set; }

        /// <summary>
        /// Channel id is the id of the  channel who is accessing api.
        /// </summary>
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ChannelId { get; set; }

        /// <summary>
        /// Branch who is accessing this api.
        /// </summary>
        //[StringRangeAttribute(AllowableValues = new[] { "01" }, ErrorMessage = "valid value for this filed is 01")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string OurBranchId { get; set; }
    }

    public class DTO
    {
        public SignOnRq SignOnRq { get; set; }
    }

    public class vkeyDTO
    {
        public SignOnRq SignOnRq { get; set; }
        public object RootObject { get; set; }
    }
}
