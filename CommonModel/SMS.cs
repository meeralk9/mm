﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.SMS
{
    public class Recipient
    {
        public string Number { get; set; }
    }

    public class TemplateVariable
    {
        public string varName { get; set; }
        public string varValue { get; set; }
    }

    public class RootObject
    {
        public string senderId { get; set; }
        public string messageId { get; set; }
        public string priority { get; set; }
        public List<Recipient> recipients { get; set; }
        public string statusCallbackURL { get; set; }
        public string confirmDelivery { get; set; }
        public string validityPeriod { get; set; }
        public string templateId { get; set; }
        public string languageCode { get; set; }
        public string body { get; set; }
        public List<TemplateVariable> templateVariables { get; set; }
        public int? messageCategory { get; set; } = 1;

    }


    public class SMSModel
    {
        public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }
}
