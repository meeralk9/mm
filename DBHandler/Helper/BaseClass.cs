﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Helper
{
    public class BaseClass
    {
        [Required(ErrorMessage = "Email Address Is Required")]
        [EmailAddress(ErrorMessage = "Wrong Email Format")]
        public string Email { get; set; }

     //   [Required(ErrorMessage = "IP Is Required")]
        public string Ip { get; set; }

        [Required(ErrorMessage = "DeviceID Is Required")]
        public string DeviceId { get; set; }

        [Required(ErrorMessage = "ChannelID Is Required")]
        public string ChannelType { get; set; }

        [Required(ErrorMessage = "UserID Is Required")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "OurBranchID Is Required")]
        public string OurBranchId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}
