﻿using DBHandler.Enum;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBHandler.Model;
using CommonModel;

namespace DBHandler.Helper
{
    public class APIHelper : ControllerBase
    {
        private ILogRepository _log;
        private readonly IUserChannelsRepository _userChannels;

        public APIHelper(IUserChannelsRepository userChannels, ILogRepository log)
        {
            _log = log;
            _userChannels = userChannels;
        }

        public string ValidateChannel(string id, string ControllerName, string ActionName)
        {
            if (!string.IsNullOrEmpty(ControllerName) && !string.IsNullOrEmpty(ActionName))
            {
                string FunctionName = ControllerName + "\\" + ActionName;
                return _userChannels.ValidateChannel(id, FunctionName);
            }
            else
            {
                return "";
            }

        }

        //public new IActionResult Response(    string message,
        //                                      Level levelpara, 
        //                                      object objcontent, 
        //                                      ActiveResponse.ACTVETErroCode code,
        //                                      DateTime Startime,
        //                                      ILogRepository _logs, 
        //                                      HttpContext contextobj, 
        //                                      IConfiguration configuration, 
        //                                      string RequestParameter, 
        //                                      string id,
        //                                      ActiveResponse.ReturnResponse responsecode, 
        //                                      Exception ex,
        //                                      SignOnRq signRequest)
        //{
        //    IActionResult res = null;
        //    ActiveResponse response = new ActiveResponse();
        //    Level obj = new Level();

        //    obj = levelpara;
        //    response.code = code;
        //    response.content = objcontent;
        //    response.message = message;
        //   // response.LogId = signRequest.LogId;
        //    //esponse.RequestDateTime = signRequest.DateTime;

        //    if (ex != null)
        //    {
        //        response.exceptionMessage = ex.InnerException.ToString();
        //    }

        //    int returnCode = Convert.ToInt32(responsecode);
        //    if (returnCode == 404)
        //    {
        //        res = BadRequest( response );
        //    }
        //    if (returnCode == 401)
        //    {
        //        res = Unauthorized( response);
        //    }
        //    else if (returnCode == 200)
        //    {
        //        res = Ok( response );
        //    }


        //    //var userID = HttpContext.User.Claims.First().Value;
        //    var userID = "";
        //    string resp = JsonConvert.SerializeObject(res);
        //    string req = JsonConvert.SerializeObject(response);
        //    DateTime Endtime = DateTime.Now;
        //    HttpContext Ctx = Request.HttpContext;
        //    _log.Logs(levelpara, "", Ctx, configuration, ex, Startime, Endtime, req, resp,userID, signRequest.ChannelId, signRequest.LogId, signRequest.DateTime);

        //    return res;
        //}

        public List<string> GetErrors(ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToList();

        }

        public string ReturnUnauthorizeErrorCode(string id, string user, string tuser)
        {
            string message = "Unauthorized";

            if (id == null)
            {
                message = "err-U";
                return message;
            }

            if (user == "Null")
            {
                message = "err-U";
                return message;
            }
            return message;
        }

        public bool ValidateUnauthorizeUser(string id, string user, string tuser)
        {
            bool check = false;
            user = user == "Null" ? null : user;
            tuser = tuser == "Null" ? null : tuser;
            return check = id == null || user == null ? false : true;
        }
    }
}