﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
//using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;

using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
//using Microsoft.AspNetCore.Routing;
using System.Text;
using Microsoft.AspNetCore.Routing;
using System.Web;
using System.Collections.Specialized;
using CommonModel;
using Microsoft.AspNetCore.Mvc.Controllers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using DBHandler.Model.SPLUNK;
using System.Net;
using System.Net.Sockets;
using System.Net.Http;

namespace DBHandler.Helper
{
    public class Utility
    {
        public static readonly string[] Functions = {
    "SendSMS",
    "SendEmail",
    "getAccounts",
    "token",
    "GetCustomerDetails",
    "dispSCFInfo",
    "getCard",
    "createDrawdownSCF",
    "CreateUser",
    "getBulkUpload",
    "GetBeneficiaries",
    "getBillPayment",
    "getActivities",
    "GetSplunkDetails",
    "getHold",
    "getSweep",
    "getTermDeposit",
    "getLoan",
    "getSalaryUpload",
    "StatusUpdate",
    "inquireFacilites",
    "StatementRequestApproval",
    "drawdowncreate",
    "Update002Response",
    "UpdateCorporateUser",
    "register",
    "getAccounts",
    "getCorpAccount",
    "ComplianceCaseManagementCreatePhoneCall",
    "ComplianceCaseManagementCreateCase",
    "ComplianceCaseManagementCloseCase",
    "EDCFraudCaseManagementUpdateCase",
    "IMTFCreateCase",
    "IMTFCreateQuestions",
    "IMTFUpdateCase",
    "IMTFUpdateAnswers",
    "AuthenticateSSO",
    "KillSSOToken",
    "token",
    "getSupplier",
    "getStandingInstruction",
    "getProductBorrowingLimit",
    "getBankGuarantee",
    "getOnlineSummary",
    "getSponsor",
    "TokenStatusUpdate",
    "RandomTokenAssignment",
    "AssignToken",
    "SendFireBaseMessage"
        };

        public static readonly string[] TokenRemoveFunctions = {"ComplianceCaseManagementCreatePhoneCall",
    "ComplianceCaseManagementCreateCase",
    "ComplianceCaseManagementCloseCase",
    "EDCFraudCaseManagementUpdateCase",
    "IMTFCreateCase",
    "IMTFCreateQuestions",
    "IMTFUpdateCase",
    "IMTFUpdateAnswers",
    "AuthenticateSSO",
    "KillSSOToken",
    "token",
    "inquireFacilites",
    "createDrawdownSCF",
    "drawdowncreate",
    "dispSCFInfo",
    "GetSplunkDetails",
    "GetBeneficiaries",
    "getCard",
    "GetCustomerDetails",
    "getActivities",
    "getAccounts",
    "getHold",
    "getSweep",
    "getLoan",
    "getTermDeposit",
    "getBulkUpload",
    "getBillPayment",
    "getSalaryUpload",
    "getSponsor",
    "getOnlineSummary",
    "getProductBorrowingLimit",
    "getBankGuarantee",
    "getCorpAccount",
    "getStandingInstruction",
    "getSupplier",
    "TokenStatusUpdate",
    "RandomTokenAssignment",
    "AssignToken",
    "SendFireBaseMessage" };

        public static string ModifyRequestBody(string function, string RequestParameter)
        {
            if (!string.IsNullOrEmpty(function))
            {
                if (Functions.Contains(function))
                {
                    var temp = JObject.Parse(RequestParameter);
                    temp.Descendants()
                        .OfType<JProperty>()
                        .Where(attr => attr.Name.Equals("password") || attr.Name.Equals("username") || attr.Name.Equals("userId") || attr.Name.Equals("UserName")
                        || attr.Name.Equals("userName")
                        || attr.Name.Equals("loginId") || attr.Name.Equals("Username") || attr.Name.Equals("Password") || attr.Name.Equals("client_secret") 
                        || attr.Name.Equals("token")
                        || attr.Name.Equals("Token")
                        || attr.Name.Equals("Token_No")
                        || attr.Name.Equals("User_Id"))
                        .ToList()
                        .ForEach(attr => attr.Remove());
                    RequestParameter = JsonConvert.SerializeObject(temp);
                }
            }
         
            return RequestParameter;

        }

        public static string ModifyResponseBody(string function, string ResponseParameter)
        {
            if (!string.IsNullOrEmpty(function))
            {
                if (TokenRemoveFunctions.Contains(function))
                {
                    var temp = JObject.Parse(ResponseParameter);                    
                    temp.Descendants()
                        .OfType<JProperty>()
                        .Where(
                        attr => attr.Name.Equals("access_token")
                        || attr.Name.Equals("Token_No") || attr.Name.Equals("refresh_token")
                        || attr.Name.Equals("id_token") || attr.Name.Equals("token"))
                        .ToList()
                        .ForEach(attr => attr.Remove());
                    ResponseParameter = JsonConvert.SerializeObject(temp);
                }
            }

            return ResponseParameter;

        }
        public static IConfiguration Configuration { get; }

        public static string SaveFile(IConfiguration configuration, string fileExtention, string file, string filepath, string folder)
        {
            try
            {

                string filename = Guid.NewGuid() + "." + fileExtention;

                File.WriteAllBytes(filepath + '\\' + folder + '\\' + filename, Convert.FromBase64String(file.Substring(file.IndexOf(',') + 1)));

                return filename;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetIP(HttpContext contaxt)
        {
            return contaxt.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
        }


        public static string GetFunctionName(HttpContext context)
        {
            var routeInfo = context.GetRouteData().Values;
            string currentController = routeInfo["controller"].ToString();
            string currentAction = routeInfo["action"].ToString();

            return currentController + "\\" + currentAction;
        }

        public static string GetUserIDFromToken(HttpContext contaxt)
        {
            return contaxt.User.Claims.Where(e => e.Type == "sub").FirstOrDefault().Value;
        }

        public static Uri BuildUri(string root, NameValueCollection query)
        {
            // omitted argument checking

            // sneaky way to get a HttpValueCollection (which is internal)
            var collection = HttpUtility.ParseQueryString(string.Empty);

            foreach (var key in query.Cast<string>().Where(key => !string.IsNullOrEmpty(query[key])))
            {
                collection[key] = query[key];
            }

            var builder = new UriBuilder(root) { Query = collection.ToString() };
            return builder.Uri;
        }

        public static DTO ReadFromHeaders(HttpContext httpContext)
        {
            DTO dto = new DTO();
            dto.SignOnRq = new SignOnRq()
            {
                LogId = httpContext.Request.Headers["LogId"],
                DateTime = Convert.ToDateTime(httpContext.Request.Headers["DateTime"]),
                ChannelId = httpContext.Request.Headers["ChannelId"],
                OurBranchId = httpContext.Request.Headers["OurBranchId"]
            };
            return dto;
        }
        public static bool IsTmsNotificationAction(Endpoint Endpoint, IConfiguration _config)
        {
            bool isTMS = false;
            var Actions = _config["GlobalSettings:IndepandentFunctions"].Split(",");
            var controllerActionDescriptor = Endpoint.Metadata.GetMetadata<ControllerActionDescriptor>();
            if (controllerActionDescriptor.ControllerName.Equals("TMS"))
            {
                if (Actions.Contains(controllerActionDescriptor.ActionName))
                {
                    isTMS = true;
                }
            }
            return isTMS;
        }

        public static void SplunkLogs(object splunkData,DateTime? startDateTime,string splunkBaseURL,string splunkFunctionName,string splunkAuth,string source)
        {

            try
            {
                SplunkConnectorModel splunk = new SplunkConnectorModel();
                if (startDateTime == null)
                {
                    splunk.time = ToUnixEpochDate(DateTime.Now);
                }
                else
                {
                    splunk.time = ToUnixEpochDate(DateTime.Parse( startDateTime.ToString()));

                }
                splunk.host = GetLocalIPAddress();
                splunk.source = source;

                splunk.@event = splunkData;


                var json = JsonConvert.SerializeObject(splunk);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var url = splunkFunctionName;
                var http = new HttpClient();
                http.BaseAddress = new Uri(splunkBaseURL);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", splunkAuth);
                var response = http.PostAsync(url, data);
            }
            catch (Exception ex)
            { }
        }

    
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        public static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}
