﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


using System.Transactions;
using Microsoft.AspNetCore.Mvc.Controllers;
using CommonModel;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Net.Http;
using DBHandler.Model.SPLUNK;
using Newtonsoft.Json;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace DBHandler.Repositories
{
    public class LogRepository : Repository<Log>, ILogRepository
    {
        private readonly VendorMWContext _ctx;
        public IConfiguration Configuration { get; }
        



        public LogRepository(IConfiguration configuration, VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
            Configuration = configuration;
           

        }

        private async void WriteLogs(string message, Level Level, string userid, string ip, string function, string Exception, string ControllerName, string status, DateTime Starttime, DateTime Endtime, string RequestParameter, string RequestResponse, string Channel, string logId, DateTime? rDate, System.Net.HttpStatusCode? code, string baseURL = "", string functionName = "",
            DateTime? ApiRequestStartTime = null, DateTime? ApiRequestEndTime = null, string SplunCollector = "", string SplunkBase = "", string SplunkAuth = "", string httpAction= "")
        {

            DBHandler.Common.Decrypt d = new DBHandler.Common.Decrypt(Configuration);
            // SqlConnection conn;
            DateTime reqdatetime = rDate.HasValue ? (DateTime)rDate : DateTime.Now;
            //string connstring = configuration.GetConnectionString("DefaultConnection");
            //string connstring = "Data Source=sql.dev.eradahcapital.com,55543;Initial Catalog=VendorMWdb;Persist Security Info=True;User ID=vendormwdbadm;Password=VendorAPI.2019;MultipleActiveResultSets=true;Connection Timeout=15;Connection Lifetime=0;Min Pool Size=0;Max Pool Size=1000;Pooling=true;";
            string connstring = d.DecryptString(Configuration.GetConnectionString("DefaultConnection"));

            Log model = new Log
            {
                Application = "",
                FunctionName = function,
                ControllerName = ControllerName,
                DeviceID = "",
                IP = ip,
                StartTime = Starttime,
                EndTime = Endtime,
                Exception = string.IsNullOrEmpty(Exception) ? "" : Exception,
                Message = message,
                Level = Level.ToString(),
                Status = "",
                UserID = userid,
                RequestParameters = RequestParameter,
                RequestResponse = RequestResponse,
                Channel = Channel,
                LogId = logId,
                RequestDateTime = rDate,
                ErrorCode = code.ToString(),
                APIbaseURL = baseURL,
                APIFunctionName = functionName,
                LocalServerIP = GetLocalIPAddress(),
                ApiRequestEndTime = (DateTime)ApiRequestEndTime,
                ApiRequestStartTime = (DateTime)ApiRequestStartTime,
                httpAction = httpAction
                
            };
            //db.Add(model);
            //db.SaveChanges();
            //var functionCheck = Functions;

            using (SqlConnection conn = new SqlConnection(connstring))
            {
                string query = $@"
INSERT INTO [dbo].[Logs]
           ([Application]
           ,[Level]
           ,[Message]
           ,[Logger]
           ,[Callsite]
           ,[Exception]
           ,[UserId]
           ,[Email]
           ,[DeviceID]
           ,[IP]
           ,[FunctionName]
           ,[ControllerName]
           ,[StartTime]
           ,[EndTime]
           ,[Status]
           ,[RequestParameters]
           ,[RequestResponse]
           ,[Channel]
           ,[LogId]
           ,[RequestDateTime]
           ,[ErrorCode]
           ,[APIbaseURL]
           ,[APIFunctionName]
           ,[LocalServerIP]
           ,[ApiRequestStartTime]
           ,[ApiRequestEndTime]
           ,[HTTPAction])
     VALUES
           (''
           ,'{model.Level}'
           ,'{model.Message}'
           ,''
           ,''
           ,'{model.Exception.Replace("'", "")}'
           ,'{model.UserID}'
           ,''
           ,'{model.DeviceID}'
           ,'{model.IP}'
           ,'{model.FunctionName}'
           ,'{model.ControllerName}'
           ,'{Starttime.ToString("yyyy-MM-dd HH:mm:ss.fff")}'
           ,'{Endtime.ToString("yyyy-MM-dd HH:mm:ss.fff")}'
           ,'{model.Status}'
           ,'{model.RequestParameters.Replace("'", "")}'
           ,'{model.RequestResponse.Replace("'", "")}'
           ,'{model.Channel}'
           ,'{model.LogId}'
           ,'{reqdatetime.ToString("yyyy-MM-dd HH:mm:ss.fff")}'
           ,'{model.ErrorCode}'
           ,'{model.APIbaseURL}'
           ,'{model.APIFunctionName}'
           ,'{model.LocalServerIP}'
           ,'{model.ApiRequestStartTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}'
           ,'{model.ApiRequestEndTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}'
           ,'{model.httpAction}')";

                try
                {
                    conn.Open();
                    SqlCommand comm = new SqlCommand(query, conn);
                    comm.ExecuteNonQuery();
                    conn.Close();

                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
                    
                }
            }



        }

        public async void Logs(Level Level, string status, HttpContext contaxt, Exception ex,
            DateTime Starttime, DateTime Endtime, string RequestParameter, string RequestResponse, string userid,
            string Channel, string logId, DateTime? RequestDateTime, System.Net.HttpStatusCode? code, string baseURL = "",
            string functionName = "", DateTime? ApiRequestStartTime = null, DateTime? ApiRequestEndTime = null, string clientIp = "", string currentController = "", string currentAction = "", string ErrorLog = "", string SuccessLog = "", string InfoLog = "", string SplunCollector = "", string SplunkBase = "", string SplunkAuth = "", string httpAction = "")
        {
            try
            {
                try
                {
                    RequestParameter = Utility.ModifyRequestBody(currentAction, RequestParameter);
                    RequestResponse = Utility.ModifyResponseBody(currentAction, RequestResponse);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth,"mw:log");
                    

                }

                var id = userid;
                string message = "";
                string except = null;
                if (ex != null)
                {
                    except = ex.Message;
                    if (ex.InnerException != null)
                    {
                        except += "  inner exception :" + ex.InnerException.ToString();
                        except += "  inner message:" + ex.InnerException.Message.ToString();
                    }
                }

                if (Level == Level.Error && Convert.ToBoolean(ErrorLog) == true
                    || Level == Level.Success && Convert.ToBoolean(SuccessLog) == true)
                {
                    WriteLogs(message, Level, id, clientIp, currentAction, except, currentController, status, Starttime, Endtime, RequestParameter, RequestResponse, Channel, logId, RequestDateTime, code, baseURL, functionName, ApiRequestStartTime, ApiRequestEndTime, SplunkBase, SplunCollector, SplunkAuth, httpAction);
                }
                else
                {
                    if (Convert.ToBoolean(InfoLog) == true)
                    {
                        message = "";
                        //$"User with id:{id} Access Function:{currentAction} and Controller:{currentController}";
                        Level = Level.Info;
                        WriteLogs(message, Level, id, clientIp, currentAction, except, currentController, status, Starttime, Endtime, RequestParameter, RequestResponse, Channel, logId, RequestDateTime, code, baseURL, functionName, ApiRequestStartTime, ApiRequestEndTime, SplunkBase, SplunCollector, SplunkAuth, httpAction);
                    }
                }

             
                Channel = string.IsNullOrEmpty(Channel) ? "0" : Channel;

                object req = RequestParameter;
                object res = RequestResponse;
                try
                {
                    JToken.Parse(RequestParameter);
                    req = JsonConvert.DeserializeObject(RequestParameter);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
                    
                }
                try
                {
                    JToken.Parse(RequestResponse);
                    res = JsonConvert.DeserializeObject(RequestResponse);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
                }

                object eventObj = new Event()
                {
                    Channel = Convert.ToInt32(Channel),
                    APIbaseURL = baseURL,
                    APIFunctionName = functionName,
                    ControllerName = currentController,
                    StartTime = Starttime,
                    EndTime = Endtime,
                    RequestParameters = req,
                    RequestResponse = res,
                    LogId = logId,
                    RequestDateTime = (DateTime)RequestDateTime,
                    ErrorCode = status,
                    FunctionName = currentAction,
                    LocalServerIP = GetLocalIPAddress()
                };
                Utility.SplunkLogs(eventObj, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");


                //string result = response.Content.ReadAsStringAsync().Result;
                //var response = http.PostAsync(url, data).Result;
                //string result = response.Content.ReadAsStringAsync().Result;
                //int count = 0;
                //if (result == "" || response.StatusCode != HttpStatusCode.OK)
                //{

                //}
                //else
                //{
                //    count++;
                //    StringBuilder sb1 = new StringBuilder();
                //    sb1.Append(count.ToString());
                //    File.AppendAllText("SplunkReqCount.txt", sb1.ToString());
                //}
            }
            catch (Exception e)
            {
                Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
               
            }
        }

        public ErrorMessages GetErrorMessage(string errorCode)
        {
            ErrorMessages message = _ctx.TErrorMessages.FirstOrDefault(x => x.ErrorCode == errorCode);
            return message;
        }

        public bool GetTversion()
        {
            var res = _ctx.TVersion.FirstOrDefault();
            if (res != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetLogId(string Id)
        {
            var res = _ctx.TLog.FirstOrDefault(x => x.LogId == Id);
            if (res != null)
            {
                return true;
            }
            return false;
        }

        public List<TVendorApiHeaders> GetHeaders(string VendorId, int Type)
        {

            var list = _ctx.TVendorApiHeaders.Where(x => x.VendorId.Equals(VendorId) && x.Type.Equals(Type)).ToList();
            return list;
        }

        //public VendorAPIAccess GetHeaders(int ChannelId)
        //{
        //    VendorAPIAccess obj = _ctx.TVendorApiHeaders.FirstOrDefault(x=>x.VendorId = ChannelId);
        //    return obj;
        //}

        private static string ConvertAuthorization(string username, string password)
        {
            return Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password)));
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }





    }
}
