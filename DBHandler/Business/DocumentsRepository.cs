﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CommonModel;
using System.Transactions;
using System.Data.Entity;

namespace DBHandler.Repositories
{
    public class DocumentsRepository : Repository<Documents>, IDocumentsRepository
    {
        private readonly VendorMWContext _ctx;
        // private readonly TestMWContext _ctxtest;

        public DocumentsRepository(VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
            //  _ctxtest = ctxtest;
        }

        public ErrorMessages DMSSaveDocument(Documents documents, List<DocumentMetaData> data, SignOnRq SignOnRq)
        {
            //  DMSSaveDocument(documents, data, SignOnRq, true);
            ErrorMessages err = new ErrorMessages();
            if (documents.documentId == "" || documents.documentId == null)
            {
                documents.documentId = documents.documentId == null ? "" : documents.documentId;
                documents.documentId = documents.documentId !="" ? documents.documentId :Guid.NewGuid().ToString();
                documents.CreatedOn = DateTime.Now;
                documents.OurBranchID = SignOnRq.OurBranchId;
                documents.CreatedBy = SignOnRq.ChannelId;
                documents.ChannelID = SignOnRq.ChannelId;
                documents.Version = 1;
                _ctx.TDocuments.Add(documents);
            }
            else
            {
                var doc = _ctx.TDocuments.Where(x => x.OurBranchID == SignOnRq.OurBranchId && x.documentId == documents.documentId)?.OrderByDescending(x => x.Version)?.ToList();
                if (doc != null && doc.Count > 0)
                {
                    documents.documentId = documents.documentId;
                    documents.CreatedOn = DateTime.Now;
                    documents.OurBranchID = SignOnRq.OurBranchId;
                    documents.CreatedBy = SignOnRq.ChannelId;
                    documents.ChannelID = SignOnRq.ChannelId;
                    documents.Version = documents.Version != 0 ? documents.Version : doc[0].Version + 1;
                    _ctx.TDocuments.Add(documents);
                }
                else
                {
                    err.isError = true;
                    err.ErrorMessage = "Document is not found";

                    return err;
                }
            }
            foreach (DocumentMetaData m in data)
            {
                m.documentID = documents.documentId;
                m.version = documents.Version;
            }
            _ctx.TDocumentMetaData.AddRange(data);
            _ctx.SaveChanges();
            DocumentSaveResponse response = new DocumentSaveResponse();
            response.Version = documents.Version;
            response.documentId = documents.documentId;
            err.obj = response;

            return err;
        }


        public ErrorMessages DMSSaveDocument(Documents documents, List<DocumentMetaData> data, SignOnRq SignOnRq, bool withTransactions)
        {
            // DbContext cont = (DbContext)_ctx;

            ErrorMessages err = new ErrorMessages();



            //using (var scope = new TransactionScope())
            //{
            //    try
            //    {
            //        documents.documentId = Guid.NewGuid().ToString();
            //        documents.CreatedOn = DateTime.Now;
            //        documents.OurBranchID = SignOnRq.OurBranchId;
            //        documents.CreatedBy = SignOnRq.ChannelId;
            //        documents.ChannelID = SignOnRq.ChannelId;
            //        documents.Version = 1;
            //        _ctx.TDocuments.Add(documents);
            //        _ctx.SaveChanges();

            //        _ctxtest.TDocuments.Add(documents);
            //        _ctxtest.SaveChanges();
            //        // _ctxtest.Database.BeginTransaction().

            //        err.obj = documents.documentId;
            //    }
            //    catch(Exception ex)
            //    {
            //        scope.Dispose();
            //    }
            //    scope.Complete();
            //}
            return err;
        }

        public ErrorMessages ReadDocument(string documentID, int? version, SignOnRq SignOnRq)
        {
            List<DMSHandler.DMSSaveDocument.RootObject> l = new List<DMSHandler.DMSSaveDocument.RootObject>();
            ErrorMessages err = new ErrorMessages();
            Documents doc = new Documents();
            if (version == null)
            {
                doc = _ctx.TDocuments.Where(x => x.OurBranchID == SignOnRq.OurBranchId && x.documentId == documentID)?.OrderByDescending(x => x.Version)?.Take(1).FirstOrDefault();
            }
            else
            {
                doc = _ctx.TDocuments.Where(x => x.OurBranchID == SignOnRq.OurBranchId && x.documentId == documentID && x.Version == version)?.OrderByDescending(x => x.Version)?.Take(1).FirstOrDefault();
            }
            if (doc == null)
            {
                err.isError = true;
                err.ErrorCode = "ERR-0470";
                err.ErrorMessage = "Record not found.";
            }
            else
            {
                var list = _ctx.TDocumentMetaData.Where(x => x.documentID == documentID && x.version == doc.Version)?.ToList();
                DMSHandler.DMSSaveDocument.RootObject root = new DMSHandler.DMSSaveDocument.RootObject();
                root.Document = doc;
                root.metadata = list;
                l.Add(root);
                err.obj = l;
            }
            if (l.Count == 0)
            {
                err.isError = true;
                err.ErrorCode = "ERR-0470";
                err.ErrorMessage = "Record not found.";
            }
            return err;
        }
    }
}
