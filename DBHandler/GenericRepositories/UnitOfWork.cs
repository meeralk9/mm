﻿using AutoMapper.Configuration;
using DBHandler.Data;
using DBHandler.Modal;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.GenericRepositories
{

    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        public ApplicationDbContext ApplicationDbContext { get; set; }
        public IUnitOfWork UunitOfWork { get; set; }
        public readonly IConfiguration _config;
  
        public UserManager<ApplicationUser> _userManager;

      

       


        public int Complete()
        {

            try
            {
                return ApplicationDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                var mssg = ex.Message;
                if (ex.InnerException != null)
                    mssg += " inner Exception " + ex.InnerException.Message;
                throw new Exception(mssg);

            }


        }

        public void Dispose()
        {
            try
            {
                ApplicationDbContext.Dispose();
            }
            catch (Exception ex)
            {
                int i = 0;
            }

        }
    }
}
