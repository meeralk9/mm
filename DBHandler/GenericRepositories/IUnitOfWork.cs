﻿using DBHandler.Modal;
using DBHandler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.GenericRepositories
{
    public interface IUnitOfWork : IDisposable
    {

        ILinkAccountRepository LinkAccountRepository { get; }
     
        ILogRepository LogRepository { get; }
      
        int Complete();

    }

  
}
