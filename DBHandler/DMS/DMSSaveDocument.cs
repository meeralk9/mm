﻿using CommonModel;
using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DMSHandler.DMSSaveDocument
{
    public class DMSSaveDocumentModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class RootObject
    {
        public Documents Document { get; set; }
        public List<DocumentMetaData> metadata { get; set; }
    }

    public class ResponseObject
    {
        public string documentId{ get; set; }
    }
}
