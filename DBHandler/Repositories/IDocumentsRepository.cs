﻿using CommonModel;
using DBHandler.Model;
using System.Collections.Generic;

namespace DBHandler.Repositories
{
    public interface IDocumentsRepository : IRepository<Documents>
    {
        ErrorMessages DMSSaveDocument(Documents documents, List<DocumentMetaData> data, SignOnRq SignOnRq);
        ErrorMessages ReadDocument(string documentID,int? version, SignOnRq SignOnRq);
    }
}
