﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DBHandler.Repositories
{
    public interface ILogRqChRepository : IRepository<LogRqCh>
    {
        public Task<bool> LogData(string logid, string channelid, DateTime? requestdate, string body,string response,string actionName, string ControllerName, string headers);
    }
}
