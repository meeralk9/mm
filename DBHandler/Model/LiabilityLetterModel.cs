﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.LiabilityLetterModel
{
    public class LiabilityLetterModel
    {
        public SignOnRq SignOnRq { get; set; }
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public decimal DealId { get; set; }
    }
    public class LiabilityLetterResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
        public List<OverDraftFacilitiesModel> overdraftFacilities { get; set; }
        public List<loanFacilitiesModel> loanFacilities { get; set; }
        public List<bankGuaranteeFacilitiesModel> bankGuaranteeFacilities { get; set; }
    }
    public class OverDraftFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string totalOutstanding { get; set; }
    }
    public class loanFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string totalOutstanding { get; set; }
    }
    public class bankGuaranteeFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string totalOutstanding { get; set; }
    }
}
