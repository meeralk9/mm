﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("Logs")]
    public class Log
    {
        public string Application { get; set; }
        public string FunctionName { get; set; }
        public string ControllerName { get; set; }
        public string DeviceID { get; set; }
        public string IP { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Exception { get; set; }
        public string Message { get; set; }
        public string Level { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }
        public string RequestParameters { get; set; }
        public string RequestResponse { get; set; }
        public DateTime ApiRequestStartTime { get; set; }
        public DateTime ApiRequestEndTime { get; set; }
        public string Channel { get; set; }
        public string LogId { get; set; }
        public DateTime? RequestDateTime { get; set; }
        public string ErrorCode { get; set; }
        public string APIbaseURL { get; set; }
        public string APIFunctionName { get; set; }
        public string LocalServerIP { get; set; }
        public string httpAction { get; set; }
    }
}
