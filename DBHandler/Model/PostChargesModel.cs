﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.PostChargesModel
{
    public class PostChargesModel
    {
       
        public SignOnRq SignOnRq { get; set; }

        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string accountId { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string chargeId { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string channelRefId { get; set; }
    }
    public class PostChargesResponse
    {
        public string channelRefId { get; set; }
        public string referenceNo { get; set; }
        public decimal amountCharged { get; set; }
    }
}
