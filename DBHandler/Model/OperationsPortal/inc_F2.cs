﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.OperationsPortal
{
    public class inc_F2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MinLength(10, ErrorMessage = "- Valid Min length should not be less than 10.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string Alias { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public Dictionary<string, object> Values { get; set; }
    }
    public class inc_F2Response
    {
        public List<object> incF2 { get; set; }
    }
}