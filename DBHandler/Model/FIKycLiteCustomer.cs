﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.FIKycLiteCustomer
{

    public class InsertCustomerCorporate
    {
        public string OurBranchID { get; set; }
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }

        [MaxLength(30, ErrorMessage = "Category Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Corporate", "FIS", "KYCLite" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string CategoryId { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GroupCode { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string RiskProfile { get; set; }

        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string EntityTypeCorp { get; set; }

        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        public string ContactName1Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID1Corp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }


        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }



        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }


        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailIDCorp { get; set; }

        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public string PhoneNumber2Corp { get; set; }

        public string AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string PresenceinCISCountryCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string DealingwithCISCountryCorp { get; set; }


        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }


        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }

        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string TradeLicenseNumberCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TRNNoCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string FATCARelevantCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string USAEntityCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }

        public string ReasonACorp { get; set; }
        public string CreateBy { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string CompCashIntensive { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string PubLimCompCorp { get; set; }



        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }



        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }


        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }


        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }

        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        public System.DateTime? VATRegistered { get; set; }

        public string JurEmiratesID { get; set; }
        public string JurTypeID { get; set; }
        public string JurAuthorityID { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }
    }

    public class ShareHolder
    { 
        public decimal? ShareHolderID { get; set; }

       
        public string ShareHolderName { get; set; }


        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string ShareHolderAddress { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "COM", "IND" }, ErrorMessage = "- Valid value is either COM or IND.")]
        public string ShareholderType { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string ShareholdersIDtype { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
       
        public string ShareholdersIDNo { get; set; }

       
        public DateTime? ShareholdersIDExpirydate { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporation { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BoardMember { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AuthorisedSignatory { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BeneficialOwner { get; set; }

       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

       
        public decimal? OwnershipPercentage { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USCitizen { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAEntity { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFI { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "PFI", "CFI", "PFFI" }, ErrorMessage = "- Valid value is either PFI,CFI or PFFI.")]
        public string FFICategory { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNo { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "001", "002", "003", "004", "005" }, ErrorMessage = "- Valid value is either 001,002, 003, 004 or 005")]
        public string GIINNA { get; set; }

        public string SponsorName { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "001", "002", "003", "004", "005", "006" }, ErrorMessage = "- Valid value is either 001,002, 003, 004,005 or 006 ")]
        public string NFFE { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchange { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string TradingSymbol { get; set; }

       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string SHIDIssuanceCountry { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
        //
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

      //  [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
       
        public string SHCity { get; set; }
       
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string SHState { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string SHCountry { get; set; }

        public string JurEmiratesId { get; set; }
        public string JurTypeId { get; set; }
        public string JurAuthorityId { get; set; }
        public string POBox { get; set; }
        public string JurOther { get; set; }
        public string ParentShareHolderId { get; set; }
    }

    public class AuthorisedSignatories
    {
        public decimal? AuthorisedSignatoriesID { get; set; }

       
        public string AuthorisedsignatoriesName { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumber { get; set; }

       
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

       
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string CorrespondenceAddress { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype1 { get; set; }

       
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber1 { get; set; }

       
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }

       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype2 { get; set; }

       
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber2 { get; set; }

       
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }

       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }

       
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaIssuedby { get; set; }

       
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaNumber { get; set; }

       
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }

        [MaxLength(500, ErrorMessage = "- Character length should not be more than 500.")]
        public string Remarks { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
       
        public string IDGender { get; set; }
       
        public DateTime? DateOfBirth { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
       
        public string ASCity { get; set; }
       
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string ASState { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string ASCountry { get; set; }
    }

    public class BoardDirectors
    {
        public decimal? BoardDirector { get; set; }
       
        public string BoardDirectorName { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string Designation { get; set; }

       
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BoardDirectorAddress { get; set; }

       
        public Nullable<System.DateTime> BoardDirectorIDExpirydate { get; set; }

       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BoardDirectorIDtype { get; set; }
       
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BoardDirectorIDNo { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }

       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
       
        public string IDGender { get; set; }
       
        public DateTime? DateOfBirth { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
       
        public string BDCity { get; set; }
       
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BDState { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BDCountry { get; set; }
    }

    public class BeneficialOwners
    {
        public decimal? BeneficialOwnersID { get; set; }
       
        public string BeneficialName { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BeneficialAddress { get; set; }
       
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BeneficialIDNo { get; set; }
       
        public Nullable<System.DateTime> BeneficialIDExpirydate { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryOfResidence { get; set; }
       
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BeneficialIDtype { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
       
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
       
        public string IDGender { get; set; }
       
        public DateTime? DateOfBirth { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
       
        public string BOCity { get; set; }
       
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BOState { get; set; }
       
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BOCountry { get; set; }
    }

}
