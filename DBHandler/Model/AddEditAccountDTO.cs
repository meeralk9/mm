﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditAccountDTO
    {
      

        public SignOnRq SignOnRq { get; set; }
        public AddEditAccountModel Account { get; set; }
    }

    public class AddEditAccountModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ProductID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string StatementAddress { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Country { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string State { get; set; }
        
        public string City { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y.")]
        public string StatementFrequency { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureOfAccount { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string RelationshipFunction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroAccountNumber { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroIBAN { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroSWIFT { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroAddress { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroCountry { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string MinBalanceRequirement { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroBankName { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroBranchName { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string NostroSortCode { get; set; }
        public string AccountId { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Phone { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Email { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string MobileNo { get; set; }
    }
}
