﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Host
{
    public class FullReversalDto
    {
        public string RefNo { get; set; }
        public string VISATrxID { get; set; }
        public string ProcCode { get; set; }
        public string CAVV { get; set; }
        public string ResponseCode { get; set; }
    }
}
