﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Host
{
    public class PurchaseRequestTrxResponse
    {
        public string RespCode { get; set; }
        public string ClearBalance { get; set; }
        public string AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string CurrencyCode { get; set; }
        public string AuthIDResp { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string CurrencyID { get; set; }
        public string ClientID { get; set; }
        public string NarrationID { get; set; }
    }
}
