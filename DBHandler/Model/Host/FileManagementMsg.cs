﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Host
{
    public class FileManagementMsg
    {
        public string RefNo { get; set; }
        public string CardNumber { get; set; }
        public DateTime DateTime { get; set; }
        public string STAN { get; set; }
        public string StatusType { get; set; }
        public string PreviousStatus { get; set; }
        public string CurrentStatus { get; set; }
    }
}
