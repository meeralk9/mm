﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Host
{
    public class AccountVerification
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
    }
}
