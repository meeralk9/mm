﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class CreateCorporateCIFV2Model
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer data require")]
        public CreateCorporateCIFCustomerV2 Cust { get; set; }
        public List<SicCodeCorpModelV2> SicCodeCorp { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<ShareHolderDTOV2> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        // public List<AuthorisedSignatoriesDTO> Authignatories { get; set; }
        public List<AuthorisedSignatoriesDTOModelV2> Authignatories { get; set; }

        public List<BoardDirectorsDTOV2> BoardDirectors { get; set; }


        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<BenificialOwnerDTOV2> BeneficialOwners { get; set; }
        public List<ClientSellerDTO> ClientSeller { get; set; }
        public List<ClientBuyerDTO> ClientBuyer { get; set; }
        public List<CreateCorporateCIFV2CompanyInFoV2> CompanyInFo { get; set; }
    }
    public class CreateCorporateCIFCustomerV2
    {
        public string ClientID { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "Corporate" }, ErrorMessage = "- Valid value is either Corporate only")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }

        public string GroupCode { get; set; }

        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ContactName1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than   15.")]
        public string ContactID1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailIDCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public decimal? PhoneNumber2Corp { get; set; }

        public decimal? AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PresenceinCISCountryCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string DealingwithCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string TradeLicenseNumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "TRNNoCorp must be numeric")]
        public string TRNNoCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevantCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either SUP,XUP or UPX.")]
        public string USAEntityCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }

        public string ReasonACorp { get; set; }
        public string CreateBy { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CompCashIntensive { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PubLimCompCorp { get; set; }


        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }
        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public System.DateTime? VATRegistered { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }

        // new fields
        public string LicenseType { get; set; }
        public string CountryID { get; set; }
        public DateTime? MOAMOU { get; set; }
        //public string ShareHolderAddress { get; set; }
        //public string ShareHolderCountryID { get; set; }
        //public string ResidenceShareHolderCountryID { get; set; }
        //public string ShareHolderStateID { get; set; }
        //public string ShareHolderCity { get; set; }
        public string SigningAuthority { get; set; }
        public string POAResidenceCountry { get; set; }
        public string POAAddress { get; set; }
        public string POACountry { get; set; }
        public string POAState { get; set; }
        public string POACity { get; set; }
        //public string DirectorAddress { get; set; }
        //public string DirectorCountry { get; set; }
        //public string DirectorState { get; set; }
        //public string DirectorCity { get; set; }
        //public string DirectoryResidenceCountry { get; set; }
        public string AuditedFinancials { get; set; }
        public decimal? Assets { get; set; }
        public decimal? Revenue { get; set; }
        public decimal? Profits { get; set; }
        public decimal? BusinessTurnOver { get; set; }
        public decimal? CompanyCapital { get; set; }
        public string CompanyCountry { get; set; }
        public decimal? ExpectedDebitTurnOver { get; set; }
        public decimal? ExpectedMaxDebitTurnOver { get; set; }
        public decimal? ExpectedCreditTurnOver { get; set; }
        public decimal? ExpectedMaxCreditTurnOver { get; set; }
        public string CompanyAddress { get; set; }
        public decimal? ExpectedCashVolume { get; set; }
        public decimal? ExpectedCashValue { get; set; }
        public decimal? ChequesVolume { get; set; }
        public decimal? ChequesValue { get; set; }
        public decimal? SalaryPaymentVolume { get; set; }
        public decimal? SalaryPaymentValue { get; set; }
        public decimal? IntraBankRemittancesVolume { get; set; }
        public decimal? IntraBankRemittancesValue { get; set; }
        public decimal? InternationalRemittancesVolume { get; set; }
        public decimal? InternationalRemittancesValue { get; set; }
        public decimal? DomesticRemittanceVolume { get; set; }
        public decimal? DomesticRemittanceValues { get; set; }
        public decimal? ExpectedCashDebitVolume { get; set; }
        public decimal? ExpectedCashDebitValue { get; set; }
        public decimal? ChequeDebitVolume { get; set; }
        public decimal? ChequeDebitValue { get; set; }
        public decimal? SalaryDVolume { get; set; }
        public decimal? SalaryDValue { get; set; }
        public decimal? IntraDVolume { get; set; }
        public decimal? IntraDValue { get; set; }
        public decimal? InternationalDVolume { get; set; }
        public decimal? InternationalDValue { get; set; }
        public decimal? DomesticDRemittanceValues { get; set; }
        public decimal? DomesticDRemittanceVolume { get; set; }
        public decimal? CreditTradeFinanceVolume { get; set; }
        public decimal? CreditTradeFinanceValue { get; set; }
        public decimal? TradeFinanceVolumeD { get; set; }
        public decimal? TradeFinanceValueD { get; set; }
        public decimal? NoOfStaff { get; set; }
        public string CompanyActivites { get; set; }
        public string BusinessNatureRelation { get; set; }
        public string RelationalCompanyStructure { get; set; }
        public string CompanyGeoGraphicCourage { get; set; }
        public string UBOBackground { get; set; }
        public string ProductAndServicesOffered { get; set; }
        public int? IsDisclosureActivity { get; set; }
        public string DisclosureDetails { get; set; }
        public int? IsSanctionCountry { get; set; }
        public string SanctionRemarks { get; set; }
        public int? IsOfficeInSactionCountry { get; set; }
        public string SanctionCountries { get; set; }
        public string BusinessNatureSanctionCountry { get; set; }
        public int? IsProductOrigninatedCountry { get; set; }
        public string OriginatedProductCountry { get; set; }
        public string OriginatedCountryRemarks { get; set; }
        public int? IsExportGoods { get; set; }
        public string ExportGoodsCountry { get; set; }
        public string ExportGoodsRemarks { get; set; }
        public int? isRecievedTransFromSanctionCountry { get; set; }
        public string TransactionSanctionCountry { get; set; }
        public string TransactionSanctionRemarks { get; set; }
        public string CorporateStructureChart { get; set; }
        public string VisitConducted { get; set; }
        public string SanctionAssestment { get; set; }
        public string PepAssessment { get; set; }
        public string AdverseMedia { get; set; }
        public string AccountOpeningPurpose { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "0", "1" }, ErrorMessage = "- Valid value is either o or 1.")]
        public int? IsVip { get; set; }


        public string Status { get; set; }

        //New Cust Fields

        public string POAName { get; set; }
        public string POANationality { get; set; }
        public string POADateofBirth { get; set; }
        public string POAPlaceOfBirth { get; set; }
        public string POAGender { get; set; }
        public string selectionOfPayments { get; set; }
        public Nullable<decimal> DepositValue { get; set; }
        public Nullable<decimal> PaymentValue { get; set; }
        public Nullable<decimal> LoansValue { get; set; }
        public string PlaceofIncorporation { get; set; }
        public string Designation1Corp { get; set; }
        public string Department1Corp { get; set; }
        public string IDExpiryDateCorp { get; set; }
        public string Designation2Corp { get; set; }
        public string Department2Corp { get; set; }
        public string IDExpiryDate2Corp { get; set; }
        public string DealingwithCISTextarea { get; set; }
        public string IsOfficeInSactionCountrylist { get; set; }
        public string IsProductOrigninatedCountrylist { get; set; }
        public string ExportGoodsCountrylist { get; set; }
        public string TransactionSanctionCountrylist { get; set; }
    }

   

    public class ShareHolderDTOV2
    {
        public decimal? ShareHolderID { get; set; }

        public string ShareHolderName { get; set; }

        public string ShareHolderAddress { get; set; }

        public string ShareholderType { get; set; }

        public string ShareholdersIDtype { get; set; }

        public string ShareholdersIDNo { get; set; }

        public DateTime? ShareholdersIDExpirydate { get; set; }

        public string CountryofIncorporation { get; set; }
        public string BoardMember { get; set; }
        public string AuthorisedSignatory { get; set; }
        public string BeneficialOwner { get; set; }

        public string ResidentNonResident { get; set; }

        public string CountryofResidence { get; set; }

        public decimal? OwnershipPercentage { get; set; }
        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string USCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }

        public string WereyoubornintheUS { get; set; }

        public string USAEntity { get; set; }
        public string FFI { get; set; }

        public string FFICategory { get; set; }

        public string GIINNo { get; set; }

        public string GIINNA { get; set; }

        public string SponsorName { get; set; }

        public string SponsorGIIN { get; set; }

        public string NFFE { get; set; }

        public string StockExchange { get; set; }

        public string TradingSymbol { get; set; }

        public string USAORUAE { get; set; }

        public string TINAvailable { get; set; }

        public string TinNo1 { get; set; }

        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }

        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }

        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }

        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }

        public string TaxCountry5 { get; set; }

        public string FATCANoReason { get; set; }

        public string ReasonB { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string SHIDIssuanceCountry { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        public string IDNationality { get; set; }

        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string SHCity { get; set; }
        public string SHState { get; set; }
        public string SHCountry { get; set; }
        public string JurEmiratesId { get; set; }
        public string JurTypeId { get; set; }
        public string JurAuthorityId { get; set; }
        public string POBox { get; set; }
        public string JurOther { get; set; }
        public string ParentShareHolderId { get; set; }
        public string EntityType { get; set; }

        //New fields

        public string ResidenceShareHolderCountryID { get; set; }
        public string PlaceofBirth { get; set; }
        public string GroupCodeType { get; set; }
    }

    public class AuthorisedSignatoriesDTOModelV2
    {
        public decimal? AuthorisedSignatoriesID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string AuthorisedsignatoriesName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string CorrespondenceAddress { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber1 { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }
        // public string GroupID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }
        // public string SignUpload { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AuthorisedsignatoryIDtype2 { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string AuthorisedsignatoryIDNumber2 { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }

        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }

        //  [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaIssuedby { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string AuthorisedsignatoryVisaNumber { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }

        [MaxLength(500, ErrorMessage = "- Character length should not be more than 500.")]
        public string Remarks { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        // [Required(ErrorMessage = " is a mandatory field.")]
        public string ASCity { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string ASState { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string ASCountry { get; set; }

        //New fields 

        public string ResidentNonResident { get; set; }
        public string GroupID { get; set; }

        public string Designation { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        //public string SignUpload1 { get; set; }
    }

    public class BoardDirectorsDTOV2
    {
        public decimal? BoardDirector { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string BoardDirectorName { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string Designation { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BoardDirectorAddress { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        // [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> BoardDirectorIDExpirydate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BoardDirectorIDtype { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BoardDirectorIDNo { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string BDCity { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BDState { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BDCountry { get; set; }

        //New fields
        public string BODPlaceOfBirth { get; set; }
        public string BODGender { get; set; }
        public string BODMobileNo { get; set; }

        public string BODEmail { get; set; }
    }

    public class BenificialOwnerDTOV2
    {
        public decimal? BeneficialOwnersID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string BeneficialName { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BeneficialAddress { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BeneficialIDNo { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> BeneficialIDExpirydate { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryOfResidence { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BeneficialIDtype { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDGender { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string BOCity { get; set; }
        // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BOState { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BOCountry { get; set; }

        //New fields
        public string PlaceOfBirth { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string IDIssuanceCountry { get; set; }
    }

    public class ClientSellerDTO
    {
        public string SellerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string Website { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }
    public class ClientBuyerDTO
    {
        public string ClientBuyerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string WebsiteURL { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }

    public class CreateCorporateCIFV2Response
    {
        public string ClientID { get; set; }
        public List<IDName> ShareHolderID { get; set; }
        public List<IDName> AuthorisedSignatorieID { get; set; }
        public List<IDName> BeneficialOwner { get; set; }
        public List<IDName> BoardDirectors { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "0", "1" }, ErrorMessage = "- Valid value is either 0 or 1.")]
        public int? IsVip { get; set; }
    }
    public class CreateCorporateCIFV2CompanyInFoV2
    {
        public string ClientID { get; set; }
        public decimal SerialID { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string BankCountry { get; set; }
    }
    public class SicCodeCorpModelV2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }
}