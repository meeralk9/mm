﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class RetailCustomerInquiryEBossModel
    {

        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }


    public class BalanceInquiryEBossResult
    {
        public decimal? TotalBalance { get; set; }
        public decimal? AvailableBalance { get; set; }
        public decimal? DrawableBalance { get; set; }
        public string AccountId { get; set; }
        public string ProductID { get; set; }
        // public string ProductName { get; set; }
        public string OurBranchID { get; set; }
        public string Name { get; set; }
        public string AccountType { get; set; }
        public string CurrencyID { get; set; }
        public string Status { get; set; }
        public string ClientID { get; set; }
        public string IBAN { get; set; }
        public string OpenDate { get; set; }
        public string ProductType { get; set; }
        public string ProductDesc { get; set; }
        public string GroupCode { get; set; }
        public string CountryCode { get; set; }
        public string EconoActCode { get; set; }
        public int DebitFrozen { get; set; }
        public int CreditFrozen { get; set; }

        public bool IsSuccess { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

}
