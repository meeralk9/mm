﻿
using CommonModel;
using DBHandler.Helper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model
{
    public class CustomerCorporateResposne
    {
        public string ClientID { get; set; }
        //public int? IsVip { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "Corporate" }, ErrorMessage = "- Valid value is either Corporate only")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GroupCode { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "High", "Medium", "Low" }, ErrorMessage = "valid value for this filed is F,M or T")]
        public string RiskProfile { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ContactName1Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID1Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string EmailIDCorp { get; set; }

        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public string PhoneNumber2Corp { get; set; }

        public string AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PresenceinCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string DealingwithCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string TradeLicenseNumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [TradeExpiryDate(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TRNNoCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevantCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAEntityCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }


        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CompCashIntensive { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PubLimCompCorp { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> VATRegistered { get; set; }


        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }

    }
    

    public class CustomerCorporateRequest
    {
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "Corporate" }, ErrorMessage = "- Valid value is either Corporate only")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }

        public string GroupCode { get; set; }

        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ContactName1Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than   15.")]
        public string ContactID1Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailIDCorp { get; set; }

        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public decimal? PhoneNumber2Corp { get; set; }

        public decimal? AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PresenceinCISCountryCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string DealingwithCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string TradeLicenseNumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [TradeExpiryDate(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TRNNoCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevantCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP","UPX" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAEntityCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }

        public string ReasonACorp { get; set; }
        public string CreateBy { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CompCashIntensive { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PubLimCompCorp { get; set; }


       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }
        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public System.DateTime? VATRegistered { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
       // [Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
       // [Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
       // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }
    }

    public class InsertCustomerCorporate
    {
        public string OurBranchID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GroupCode { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RManager { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityNameCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string EntityTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string RegisteredAddressCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ContactName1Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID1Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDTypeCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address1Corp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string PhoneNumberCorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string AlternatePhonenumberCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailIDCorp { get; set; }

        public string ContactName2Corp { get; set; }


        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ContactID2Corp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IDType2 { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address2Corp { get; set; }

        public string PhoneNumber2Corp { get; set; }

        public string AlternatePhonenumber2Corp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID2Corp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BusinessActivity { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SICCodesCorp { get; set; }


        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string IndustryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PresenceinCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string DealingwithCISCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporationCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [TradeExpiryDate(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TRNNoCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEPCorp { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BlackListCorp { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevantCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevantCorp { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAEntityCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFICorp { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string FFICategoryCorp { get; set; }
        [MaxLength(21, ErrorMessage = "- Character length should not be more than 21.")]
        public string GIINNoCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string NFFECorp { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchangeCorp { get; set; }
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string TradingSymbolCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAECorp { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailableCorp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3Corp { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4Corp { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5Corp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5Corp { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReasonCorp { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonBCorp { get; set; }

        public string ReasonACorp { get; set; }
        public string CreateBy { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CompanyWebsite { get; set; }


        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CompCashIntensive { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PubLimCompCorp { get; set; }


       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string RegAddCityCorp { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RegAddCountryCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string RegAddStateCorp { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add1CountryCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add1StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add1CityCorp { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string Add2CountryCorp { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string Add2StateCorp { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string Add2CityCorp { get; set; }

        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public System.DateTime? VATRegistered { get; set; }

        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
       // [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [MinLength(3, ErrorMessage = "- Character length should not be less than 3.")]
        public string POBox { get; set; }


        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string JurOther { get; set; }
           
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string GroupID { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string ParentClientID { get; set; }
    }

    public class Customer
    {
        public string idno { get; set; }
        public Nullable<System.DateTime> IDExpirydate { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreateTerminal { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public string UpdateTerminal { get; set; }
        public string AuthStatus { get; set; }
        public string SuperviseBy { get; set; }
        public Nullable<System.DateTime> SuperviseTime { get; set; }
        public string SuperviseTerminal { get; set; }

        public string OurBranchID { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string CategoryId { get; set; }
        public string Address { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string NIC { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> NicExpirydate { get; set; }
        public string NTN { get; set; }
        public string CIBCode { get; set; }
        public string RelationShipCode { get; set; }
        public string frmName { get; set; }
        public string VerifyBy { get; set; }
        public Nullable<System.DateTime> VerifyTime { get; set; }
        public string VerifyTerminal { get; set; }
        public string VerifyStatus { get; set; }
        public string CID { get; set; }
        public string NameOfFather { get; set; }
        public string NameOfMother { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string POBCountry { get; set; }
        public string POBCity { get; set; }
        public string OldClientID { get; set; }

        public string FATCARelevantInd { get; set; }
        public string CRSRelevantInd { get; set; }
        public string AreYouAuSCitizenInd { get; set; }
        public string AreYouAUSTaxResidentInd { get; set; }
        public string WereYouBornInTheUSInd { get; set; }
        public string CountryUSOrUAEInd { get; set; }
        public string TinInd { get; set; }

        public string Tinno1Ind { get; set; }
        public string TaxCountry1Ind { get; set; }
        public string TinNo2Ind { get; set; }
        public string TaxCountry2Ind { get; set; }
        public string TinNo3Ind { get; set; }
        public string? TaxCountry3Ind { get; set; }
        public string TinNo4Ind { get; set; }
        public string TaxCountry4Ind { get; set; }
        public string TinNo5Ind { get; set; }
        public string TaxCountry5Ind { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonBind { get; set; }

        public string GroupCode { get; set; }
        public string MDMID { get; set; }
        public string Groupid { get; set; }
        public string RManager { get; set; }
        public string RiskProfile { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public string IDIssueCountry { get; set; }
        public string PassportNumber { get; set; }
        public Nullable<System.DateTime> PassportExpiry { get; set; }
        public string PassportIssueCountry { get; set; }
        public string PassportGender { get; set; }
        public string OnboardingCountry { get; set; }
        public string Profession { get; set; }
        public string EmployerName { get; set; }
        public string IncomeBracket { get; set; }
        public Nullable<System.DateTime> KYCReviewDate { get; set; }
        public string DualCitizenship { get; set; }
        public string CitizenCountry2 { get; set; }
        public string PEP { get; set; }
        public string BlackList { get; set; }
        public string HNI { get; set; }
        public string PromoCode { get; set; }

        public string EntityNameCorp { get; set; }
        public string EntityTypeCorp { get; set; }
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }
        public string RegisteredAddressCorp { get; set; }
        public string ContactName1Corp { get; set; }
        public string ContactID1Corp { get; set; }
        public string IDTypeCorp { get; set; }
        public string Address1Corp { get; set; }
        public decimal? PhoneNumberCorp { get; set; }
        public decimal? AlternatePhonenumberCorp { get; set; }
        public string EmailIDCorp { get; set; }
        public string ContactName2Corp { get; set; }
        public string ContactID2Corp { get; set; }
        public string IDType2 { get; set; }
        public string Address2Corp { get; set; }
        public string PhoneNumber2Corp { get; set; }
        public string AlternatePhonenumber2Corp { get; set; }
        public string EmailID2Corp { get; set; }
        public string BusinessActivity { get; set; }
        public string SICCodesCorp { get; set; }
        public string IndustryCorp { get; set; }
        public string PresenceinCISCountryCorp { get; set; }
        public string DealingwithCISCountryCorp { get; set; }
        public string TradeLicenseNumberCorp { get; set; }
        public string CountryofIncorporationCorp { get; set; }
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        [TradeExpiryDate(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }
        public string TradeLicenseIssuanceAuthorityCorp { get; set; }
        public string TRNNoCorp { get; set; }
        public string PEPCorp { get; set; }
        public string BlackListCorp { get; set; }
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        //fATCA Fields
        public string FATCARelevantCorp { get; set; }
        public string CRSRelevantCorp { get; set; }
        public string USAEntityCorp { get; set; }
        public string FFICorp { get; set; }
        public string FFICategoryCorp { get; set; }
        public string GIINNoCorp { get; set; }
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        public string SponsorGIIN { get; set; }
        public string NFFECorp { get; set; }
        public string StockExchangeCorp { get; set; }
        public string TradingSymbolCorp { get; set; }
        public string USAORUAECorp { get; set; }
        public string TINAvailableCorp { get; set; }
        public decimal? TinNo1Corp { get; set; }
        public string TaxCountry1Corp { get; set; }
        public decimal? TinNo2Corp { get; set; }
        public string TaxCountry2Corp { get; set; }
        public decimal? TinNo3Corp { get; set; }
        public string TaxCountry3Corp { get; set; }
        public decimal? TinNo4Corp { get; set; }
        public string TaxCountry4Corp { get; set; }
        public decimal? TinNo5Corp { get; set; }
        public string TaxCountry5Corp { get; set; }
        public decimal? ReasonACorp { get; set; }

        public string JurEmiratesID { get; set; }
        public string JurTypeID { get; set; }
        public string JurAuthorityID { get; set; }
        public string POBox { get; set; }
        public string JurOther { get; set; }

         public string GroupID { get; set; }
        public string ParentClientID { get; set; }

    }

    public class InsertCustomerRetail
    {
        [MaxLength(30, ErrorMessage = "Category Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string AddressCors { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCors { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryCors { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateCors { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AddressCurr { get; set; }


        //[MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCurr { get; set; }


        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryCurr { get; set; }


        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string StateCurr { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string AddressHomeCountry { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryHome { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateHome { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityHome { get; set; }

        [MaxLength(1, ErrorMessage = "Resident non resident length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ResidentNonResident { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryofResidence { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "FATCA relevent length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string FATCARelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "CRS length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CRSRelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you a US citizen length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAuSCitizenInd { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you A US tax resident length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAUSTaxResidentInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Were you born in the US length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string WereYouBornInTheUSInd { get; set; }

        //this need to be  uncomment once bs field fix
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Country US or UAE length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryUSOrUAEInd { get; set; }

        [MaxLength(30, ErrorMessage = "PHone1 length should be less than 30")]
        public string Phone1 { get; set; }

        [MaxLength(30, ErrorMessage = "PHone2 length should be less than 30")]
        public string Phone2 { get; set; }

        [MaxLength(30, ErrorMessage = "Mobile Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string MobileNo { get; set; }


        [MaxLength(100, ErrorMessage = "Email Max length should be less than 100")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string customerEmail { get; set; }
        public string Email { get; set; }


        [MaxLength(15,ErrorMessage ="ID no Max length should be less than 15")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string idno { get; set; }

        public string NIC { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> IDExpirydate { get; set; }
        public Nullable<System.DateTime> NicExpirydate { get; set; }

        [BirthdateDateAttribute(ErrorMessage = " is not a valid date")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> DOB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F","T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Gender { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Tin length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string TinInd { get; set; }

        public string Tinno1Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 1 length should be less than 3")]
        public string TaxCountry1Ind { get; set; }

        public string TinNo2Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 2 length should be less than 3")]
        public string TaxCountry2Ind { get; set; }

        public string TinNo3Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 3 length should be less than 3")]
        public string? TaxCountry3Ind { get; set; }

        public string TinNo4Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 4 length should be less than 3")]
        public string TaxCountry4Ind { get; set; }

        public string TinNo5Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 5 length should be less than 3")]
        public string TaxCountry5Ind { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "Reason B length should be less than 100")]
        public string ReasonBind { get; set; }


        [MaxLength(5, ErrorMessage = "Group code length should be less than 5")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string GroupCode { get; set; }

        [MaxLength(15, ErrorMessage = "MDMID length should be less than 15")]
        public string MDMID { get; set; }


        [MaxLength(30, ErrorMessage = "RM (Relationship Mgr)length should be less than 30")]
        public string RManager { get; set; }


        [MaxLength(15, ErrorMessage = "Risk Profile length should be less than 15")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "High", "Medium", "Low" }, ErrorMessage = "valid value for this filed is F,M or T")]
        public string RiskProfile { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string LastName { get; set; }

        [MaxLength(3, ErrorMessage = "Nationality length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Nationality { get; set; }


        [MaxLength(3, ErrorMessage = "ID issue Country length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDIssueCountry { get; set; }

        [MaxLength(20, ErrorMessage = "Passport number length should be less than 20")]
        public string PassportNumber { get; set; }

        public System.DateTime? PassportExpiry { get; set; }

        [MaxLength(3, ErrorMessage = "Passport issue Country length should be less than 3")]
        public string PassportIssueCountry { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F","T" }, ErrorMessage = "valid value for this filed is F,M or T")]
        [MaxLength(1, ErrorMessage = "Passpot gender length should be less than 1")]
        public string PassportGender { get; set; }

        [MaxLength(3, ErrorMessage = "Onboarding Country length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string OnboardingCountry { get; set; }

        [MaxLength(50, ErrorMessage = "Profession length should be less than 50")]
        public string Profession { get; set; }

        public string EmployerName { get; set; }

        [MaxLength(30, ErrorMessage = "Income bracket length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IncomeBracket { get; set; }

        [Required(ErrorMessage = "KYC review date is required")]
        public Nullable<System.DateTime> KYCReviewDate { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Dual citizenship length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string DualCitizenship { get; set; }

        [MaxLength(3, ErrorMessage = "Citizen Country length should be less than 3")]
        public string CitizenCountry2 { get; set; }


        [MaxLength(1, ErrorMessage = "PEP length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string PEP { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Black List length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string BlackList { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "HNI length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string HNI { get; set; }

        [MaxLength(30, ErrorMessage = "Referer Promo Code length should be less than 30")]
        public string RefererPromoCode { get; set; }

        public string OurBranchID { get; set; }
        public string CreateBy { get; set; }


        public string ClientId { get; set; }
    }

    public class InsertCustomerRetailView
    {
        [MaxLength(30, ErrorMessage = "Category Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string AddressCors { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCors { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryCors { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateCors { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AddressCurr { get; set; }


       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCurr { get; set; }


        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryCurr { get; set; }


        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string StateCurr { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string AddressHomeCountry { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryHome { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateHome { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityHome { get; set; }

        [MaxLength(1, ErrorMessage = "Resident non resident length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ResidentNonResident { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryofResidence { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "FATCA relevent length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string FATCARelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "CRS length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CRSRelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you a US citizen length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAuSCitizenInd { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you A US tax resident length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAUSTaxResidentInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Were you born in the US length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string WereYouBornInTheUSInd { get; set; }

        //this need to be  uncomment once bs field fix
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Country US or UAE length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryUSOrUAEInd { get; set; }

        [MaxLength(30, ErrorMessage = "PHone1 length should be less than 30")]
        public string Phone1 { get; set; }

        [MaxLength(30, ErrorMessage = "PHone2 length should be less than 30")]
        public string Phone2 { get; set; }

        [MaxLength(30, ErrorMessage = "Mobile Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string MobileNo { get; set; }

        [MaxLength(100, ErrorMessage = "Email Max length should be less than 100")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string customerEmail { get; set; }


        [MaxLength(15, ErrorMessage = "ID no Max length should be less than 15")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string idno { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> IDExpirydate { get; set; }


        [BirthdateDateAttribute(ErrorMessage = " is not a valid date")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> DOB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Gender { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Tin length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string TinInd { get; set; }

        public string Tinno1Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 1 length should be less than 3")]
        public string TaxCountry1Ind { get; set; }

        public string TinNo2Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 2 length should be less than 3")]
        public string TaxCountry2Ind { get; set; }

        public string TinNo3Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 3 length should be less than 3")]
        public string? TaxCountry3Ind { get; set; }

        public string TinNo4Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 4 length should be less than 3")]
        public string TaxCountry4Ind { get; set; }

        public string TinNo5Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 5 length should be less than 3")]
        public string TaxCountry5Ind { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "Reason B length should be less than 100")]
        public string ReasonBind { get; set; }


        [MaxLength(5, ErrorMessage = "Group code length should be less than 5")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string GroupCode { get; set; }

        [MaxLength(15, ErrorMessage = "MDMID length should be less than 15")]
        public string MDMID { get; set; }


        [MaxLength(30, ErrorMessage = "RM (Relationship Mgr)length should be less than 30")]
        public string RManager { get; set; }


        [MaxLength(15, ErrorMessage = "Risk Profile length should be less than 15")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "High", "Medium", "Low" }, ErrorMessage = "valid value for this filed is F,M or T")]
        public string RiskProfile { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string LastName { get; set; }

        [MaxLength(3, ErrorMessage = "Nationality length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Nationality { get; set; }


        [MaxLength(3, ErrorMessage = "ID issue Country length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDIssueCountry { get; set; }

        [MaxLength(20, ErrorMessage = "Passport number length should be less than 20")]
        public string PassportNumber { get; set; }

        public Nullable<System.DateTime> PassportExpiry { get; set; }

        [MaxLength(3, ErrorMessage = "Passport issue Country length should be less than 3")]
        public string PassportIssueCountry { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is F,M or T")]
        [MaxLength(1, ErrorMessage = "Passpot gender length should be less than 1")]
        public string PassportGender { get; set; }

        [MaxLength(3, ErrorMessage = "Onboarding Country length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string OnboardingCountry { get; set; }

        [MaxLength(50, ErrorMessage = "Profession length should be less than 50")]
        public string Profession { get; set; }

        public string EmployerName { get; set; }

        [MaxLength(30, ErrorMessage = "Income bracket length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IncomeBracket { get; set; }

        [Required(ErrorMessage = "KYC review date is required")]
        public Nullable<System.DateTime> KYCReviewDate { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Dual citizenship length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string DualCitizenship { get; set; }

        [MaxLength(3, ErrorMessage = "Citizen Country length should be less than 3")]
        public string CitizenCountry2 { get; set; }


        [MaxLength(1, ErrorMessage = "PEP length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string PEP { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Black List length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string BlackList { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "HNI length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string HNI { get; set; }

        [MaxLength(30, ErrorMessage = "Referer Promo Code length should be less than 30")]
        public string RefererPromoCode { get; set; }

        public string ClientId { get; set; }

    }

    public class UpdateCustomerRetail
    {
        [MaxLength(9, ErrorMessage = "Category Max length should be less than 9")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }

        [MaxLength(30, ErrorMessage = "Category Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CategoryId { get; set; }

        [MaxLength(140, ErrorMessage = "Address Max length should be less than 140")]
        public string AddressCors { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCors { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryCors { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateCors { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AddressCurr { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityCurr { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryCurr { get; set; }


        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string StateCurr { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string AddressHomeCountry { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        public string CountryHome { get; set; }

        [MaxLength(6, ErrorMessage = "State Max length should be less than 6")]
        public string StateHome { get; set; }

       // [MaxLength(30, ErrorMessage = "City length should be less than 30")]
        public string CityHome { get; set; }

        [MaxLength(1, ErrorMessage = "Resident non resident length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ResidentNonResident { get; set; }

        [MaxLength(3, ErrorMessage = "Country Max length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryofResidence { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "FATCA relevent length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string FATCARelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "CRS length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CRSRelevantInd { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you a US citizen length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAuSCitizenInd { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Are you A US tax resident length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string AreYouAUSTaxResidentInd { get; set; }

        //this need to be  uncomment once bs field fix
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Country US or UAE length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string CountryUSOrUAEInd { get; set; }

        [MaxLength(30, ErrorMessage = "PHone1 length should be less than 30")]
        public string Phone1 { get; set; }

        [MaxLength(30, ErrorMessage = "PHone2 length should be less than 30")]
        public string Phone2 { get; set; }

        [MaxLength(30, ErrorMessage = "Mobile Max length should be less than 30")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string MobileNo { get; set; }


        [MaxLength(100, ErrorMessage = "Email Max length should be less than 100")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string customerEmail { get; set; }
        public string Email { get; set; }

        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public Nullable<System.DateTime> IDExpirydate { get; set; }
        public Nullable<System.DateTime> NicExpirydate { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Gender { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Tin length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string TinInd { get; set; }

        public string Tinno1Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 1 length should be less than 3")]
        public string TaxCountry1Ind { get; set; }

        public string TinNo2Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 2 length should be less than 3")]
        public string TaxCountry2Ind { get; set; }

        public string TinNo3Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 3 length should be less than 3")]
        public string? TaxCountry3Ind { get; set; }

        public string TinNo4Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 4 length should be less than 3")]
        public string TaxCountry4Ind { get; set; }

        public string TinNo5Ind { get; set; }

        [MaxLength(3, ErrorMessage = "Tin Country 5 length should be less than 3")]
        public string TaxCountry5Ind { get; set; }


        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "Reason B length should be less than 100")]
        public string ReasonBind { get; set; }


        [MaxLength(5, ErrorMessage = "Group code length should be less than 5")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string GroupCode { get; set; }


        [MaxLength(30, ErrorMessage = "RM (Relationship Mgr)length should be less than 30")]
        public string RManager { get; set; }


        [MaxLength(15, ErrorMessage = "Risk Profile length should be less than 15")]
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "High", "Medium", "Low" }, ErrorMessage = "valid value for this field is High,Medium or Low")]
        public string RiskProfile { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string LastName { get; set; }

        [MaxLength(3, ErrorMessage = "Nationality length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string Nationality { get; set; }


        [MaxLength(3, ErrorMessage = "ID issue Country length should be less than 3")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string IDIssueCountry { get; set; }

        [MaxLength(20, ErrorMessage = "Passport number length should be less than 20")]
        public string PassportNumber { get; set; }

        public Nullable<System.DateTime> PassportExpiry { get; set; }

        [MaxLength(3, ErrorMessage = "Passport issue Country length should be less than 3")]
        public string PassportIssueCountry { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is F,M or T")]
        [MaxLength(1, ErrorMessage = "Passpot gender length should be less than 1")]
        public string PassportGender { get; set; }

        [MaxLength(50, ErrorMessage = "Profession length should be less than 50")]
        public string Profession { get; set; }

        public string EmployerName { get; set; }

        [Required(ErrorMessage = "KYC review date is required")]
        public Nullable<System.DateTime> KYCReviewDate { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Dual citizenship length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string DualCitizenship { get; set; }

        [MaxLength(3, ErrorMessage = "Citizen Country length should be less than 3")]
        public string CitizenCountry2 { get; set; }

        [MaxLength(1, ErrorMessage = "PEP length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string PEP { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Black List length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string BlackList { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "HNI length should be less than 1")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string HNI { get; set; }

        public string OurBranchID { get; set; }
        public string CreateBy { get; set; }
    }

    public class CustomerBasicDetailModel
    {
        public string ClientID { get; set; }

        public string CategoryId { get; set; }

        public string Name { get; set; }
    }



    public class ExpiredCIFResponse
    {
        public string idno { get; set; }
        public Nullable<System.DateTime> IDExpirydate { get; set; }
        public string OurBranchID { get; set; }
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public string IDIssueCountry { get; set; }
        public string PassportNumber { get; set; }
        public Nullable<System.DateTime> PassportExpiry { get; set; }
        public string PassportIssueCountry { get; set; }
        public string PassportGender { get; set; }
        public string OnboardingCountry { get; set; }
        public Nullable<System.DateTime> KYCReviewDate { get; set; }
    }
}
