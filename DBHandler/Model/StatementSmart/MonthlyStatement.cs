﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class MonthlyStatement
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should not be less than 16.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? FromDate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "O", "E", }, ErrorMessage = "- Valid values are O - On Screen or E - Email")]
        public string OutputType { get; set; }
    }
}
