﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class MonthlyStatementStatusResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public List<MonthlyStatementStatusDetails> MonthlyStatementStatus { get; set; }
    }

    public class MonthlyStatementStatusResponseOut
    {
        public List<MonthlyStatementStatusDetails> MonthlyStatementStatus { get; set; }
    }

    public class MonthlyStatementStatusDetails
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public string GUID { get; set; }

    }
}
