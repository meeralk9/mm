﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class VATAdvice
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public DateTime? WorkingDate { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public decimal? TrxRefNo { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "O", "E", }, ErrorMessage = "- Valid values are O - On Screen or E - Email")]
        public string OutputType { get; set; }
    }
}
