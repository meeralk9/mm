﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace DBHandler.Model
{
    public class AccountView
    {
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public string ProductID { get; set; }
        public string Name { get; set; }
        public string IBAN { get; set; }
        public string NatureID { get; set; }
        public string AccountType { get; set; }
        public string MobileNo { get; set; }
        public string Status { get; set; }
        public string CurrencyID { get; set; }
        public string OpenDate { get; set; }

        public DateTime? LastStatusChangeDate { get; set; }
    }

    public class Content
    {
        public string accountID { get; set; }
        public string name { get; set; }
        public string iban { get; set; }
        public string status { get; set; }
        public string currencyID { get; set; }
    }

    public class Account
    {
        public string CreateBy { get; set; }
        public string OurBranchID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(9, ErrorMessage = "- Valid Min length should be less than 9.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ProductID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
       // [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ZakatExemption { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Singly", "Jointly", "Either or Survivor", "Anyone can Operate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]

        public int? AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]

        public int? AllowDebitTransaction { get; set; }
       
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]

        public int? NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]

        public int? NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]

        public int? NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M", "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public decimal? NoOfDrTrx { get; set; }
        public decimal? NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }


        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductClearing { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCollection { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductRemittance { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCross { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }

        public string NameKin { get; set; }

        public string FNameKin{ get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDType { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDNoKin { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKin{ get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCountryID{ get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinStateID { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCityID { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoResKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoOffKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string MobNoKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string FaxKin { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string EmailKin { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string RelationshipKin{ get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SOCID { get; set; }
    }

    public class UpdateAccount
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        public string CreateBy { get; set; }
        public string OurBranchID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ZakatExemption { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Singly", "Jointly", "Either or Survivor", "Anyone can Operate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowDebitTransaction { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        //     [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M", "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public decimal? NoOfDrTrx { get; set; }
        public decimal? NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductClearing { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCollection { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductRemittance { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCross { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }

        public string NameKin { get; set; }

        public string FNameKin { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDType { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDNoKin { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKin { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCountryID { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinStateID { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCityID { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoResKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoOffKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string MobNoKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string FaxKin { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string EmailKin { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string RelationshipKin { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SOCID { get; set; }
    }

    public class AccountViewDetail
    {

        public int? IsCharity { get; set; }
        public decimal? CharityAmount { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(9, ErrorMessage = "- Valid Min length should be less than 9.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ProductID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ZakatExemption { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Company Mandate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowDebitTransaction { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M", "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public decimal? NoOfDrTrx { get; set; }
        public decimal? NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductClearing { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCollection { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductRemittance { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCross { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }

        public string NameKin { get; set; }

        public string FNameKin { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDType { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IDNoKin { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKin { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCountryID { get; set; }

        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinStateID { get; set; }

        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string AddressKinCityID { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoResKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string PhNoOffKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string MobNoKin { get; set; }

        [MaxLength(18, ErrorMessage = "- Character length should not be more than 18.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string FaxKin { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string EmailKin { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string RelationshipKin { get; set; }

        public string status { get; set; }

        public string iban { get; set; }
        public string OpenDate { get; set; }
        public string ActivationDate { get; set; }
        public string SOCID { get; set; }
    }

    public class AccountCorporate
    {
        public string CreateBy { get; set; }
        public string OurBranchID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(9, ErrorMessage = "- Valid Min length should be less than 9.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ProductID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string Name { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] {"N","D","W","M","Q","H","Y"}, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency{ get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ZakatExemption { get; set; }

        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ContactPersonName { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Designation { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string MobileNo { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Fax { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Company Mandate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowDebitTransaction { get; set; }
      
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M" , "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public decimal? NoOfDrTrx { get; set; }
        public decimal? NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }

       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductClearing { get; set; }
      [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCollection { get; set; }
       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductRemittance { get; set; }
       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCross { get; set; }
       [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }
    }


    public class UpdateAccountCorporate
    {
        [JsonIgnore]
        public string CreateBy { get; set; }

        [JsonIgnore]
        public string OurBranchID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(9, ErrorMessage = "- Valid Min length should be less than 9.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ZakatExemption { get; set; }

        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ContactPersonName { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Designation { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string MobileNo { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Fax { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Company Mandate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? AllowDebitTransaction { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M", "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public decimal? NoOfDrTrx { get; set; }
        public decimal? NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductClearing { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCollection { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductRemittance { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductCross { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int? ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }
    }

    public class AccountCorporateView
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(9, ErrorMessage = "- Valid Min length should be less than 9.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ProductID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Name { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Address { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CountryID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string StateID { get; set; }

        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string CityID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string StatementFrequency { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string HoldMail { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ZakatExemption { get; set; }

        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string ContactPersonName { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string Designation { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Phone2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string MobileNo { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        //[MinLength(8, ErrorMessage = "- Valid Min length should be less than 8.")]
        public string Fax { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [Email(ErrorMessage = "- is not a valid email.")]
        public string EmailID { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(1, ErrorMessage = "- Valid Min length should be less than 1.")]
        public string IntroducerAccountNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducedBy { get; set; }
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerAddress { get; set; }
       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCityID { get; set; }
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerStateID { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //[MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        public string IntroducerCountryID { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Company Mandate" }, ErrorMessage = "- Valid value is Company Mandate")]
        public string ModeOfOperation { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Reminder { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string NatureID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //[MinLength(30, ErrorMessage = "- Valid Min length should be less than 30.")]
        public string RelationshipCode { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string AllowCreditTransaction { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string AllowDebitTransaction { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string NotServiceCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string NotStopPaymentCharges { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string NotChequeBookCharges { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "Below 1M", "1M to 5M", "5M to 10M", "Above 10M" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string TurnOver { get; set; }

        public string NoOfDrTrx { get; set; }
        public string NoOfCrTrx { get; set; }
        public decimal? DrThresholdLimit { get; set; }
        public decimal? CrThresholdLimit { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductCash { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductClearing { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductCollection { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductRemittance { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductCross { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ProductOthers { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        //[MinLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ProductOthersDesc { get; set; }

        public string iban { get; set; }
        public string OpenDate { get; set; }
        public string ActivationDate { get; set; }

        public string status { get; set; }
    }
}