﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel.DataAnnotations;

namespace DBHandler.Model
{
    public class Publicdata 
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string IDNumber { get; set; }
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        [Required(ErrorMessage = " is a mandatory field.")]
        public string cardNo { get; set; }
        public string IssueDate { get; set; }
        public string ExpiryDate { get; set; }
        public string FullNameAr { get; set; }
        public string TitleEn { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string FullNameEn { get; set; }
        public string Sex { get; set; }
        public string Nationality { get; set; }
        public string NationalityCode { get; set; }
        public string  DOB { get; set; }
        public string MotherFullNameAr { get; set; }
        public string MotherFullNameEn { get; set; }
        public string CardholderPhotography { get; set; }
        public string OccupationCode { get; set; }
        public string MaritalStatusCode { get; set; }
        public string HusbandIDN { get; set; }
        public string SponsorTypeCode { get; set; }
        public string SponsorUnifiedNumber { get; set; }
        public string SponsorName { get; set; }
        public string ResidencyTypeCode { get; set; }
        public string ResidencyNumber { get; set; }
        public string IDTypeofDocument { get; set; }
        public string FamilyId { get; set; }
        public string PassportNumber { get; set; }
        public string PassportType { get; set; }
        public string PassportCountryDescrAr { get; set; }
        public string PassportCountryDescEn { get; set; }
        public string PassportIssueDate { get; set; }
        public string PassportExpiryDate { get; set; }
        public string PlaceOfBirthAr { get; set; }
        public string PlaceOfBirthEn { get; set; }
        public string QualificationLevel { get; set; }
        public string QualificationLevelDescrAr { get; set; }
        public string QualificationLevelDescrptionEn { get; set; }
        public string DegreeDescriptionAr { get; set; }
        public string DegreeDescriptionEn { get; set; }
        public string FieldOfStudy { get; set; }
        public string FieldOfStudyAr { get; set; }
        public string FieldOfStudyEn { get; set; }
        public string PlaceOfStudyAr { get; set; }
        public string PlaceOfStudyEn { get; set; }
        public string DateOfGraduation { get; set; }
        public string OccupationAr { get; set; }
        public string OccupationEn { get; set; }
        public string OccupationFieldCode { get; set; }
        public string OccupationTypeAr { get; set; }
        public string OccupationTypeEn { get; set; }
        public string CompanyNameAr { get; set; }
        public string CompanyNameEn { get; set; }
        public string OccupationCodeOcc { get; set; }
        public string OccupationTypeOccEn { get; set; }
        public string MotherFullNameOthAr { get; set; }
        public string MotherFullNameOthEn { get; set; }
        public string HolderSignatureImage { get; set; }
        public string SponsorUnifiedNumberOth { get; set; }
        public string EmiratesDesc { get; set; }
        public string EmiratesDescAr { get; set; }
        public string EmirateCode { get; set; }
        public string CityCode { get; set; }
        public string CityDescEn { get; set; }
        public string CityDescAr { get; set; }
        public string LabelOfTheField { get; set; }
        public string Street { get; set; }
        public string StreetAr { get; set; }
        public string AreaDesc { get; set; }
        public string AreaDescAr { get; set; }
        public string Building { get; set; }
        public string BuildingAr { get; set; }
        public string AreaCode { get; set; }
        public string FlatNo { get; set; }
        public string AddressTypeCode { get; set; }
        public string LocationCode { get; set; }
        public string ResidentPhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string POBox { get; set; }
        public string Email { get; set; }
        public string client { get; set; }
    }
}