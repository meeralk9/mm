﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class GetDepositRateModel
    {
        
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ProductId is a mandatory field.")]
        public string ProductId { get; set; }
        [Required(ErrorMessage = "SerialId is a mandatory field.")]
        [PosNumberNoZero(ErrorMessage = "need a positive number, bigger than 0")]
        public int SerialId { get; set; }
    }
    public class GetDepositRateResponse
    {
        public string ProductId { get; set; }
        public string SerialId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? MinSlabAmount { get; set; }
        public decimal? MaxSlabAmount { get; set; }
        public decimal? EffectiveRate { get; set; }
        public decimal? PreMatureRate { get; set; }
    }
}
