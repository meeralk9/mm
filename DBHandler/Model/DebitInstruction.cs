﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model.DebitInstruction
{
    public class DebitInstructionModel
    {

        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = "FacId is required")]
        public string FacId { get; set; }
        [Required(ErrorMessage = "ProdId is required")]
        public string ProdId { get; set; }
        [Required(ErrorMessage = "RefNo is required")]
        public string RefNo { get; set; }
        [Required(ErrorMessage = "FeeAmt is required")]

        public decimal FeeAmt { get; set; }
        [Required(ErrorMessage = "ApplyVAT is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ApplyVAT { get; set; }
        [Required(ErrorMessage = "FeeType is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Processing", "Miscellaneous" }, ErrorMessage = "- Valid value is either Processing or Miscellaneous.")]
        public string FeeType { get; set; }

        public string miscFeePurpose { get; set; }
        [Required(ErrorMessage = "DebitAccNo is required")]
        public string DebitAccNo { get; set; }

        public string ReversalType { get; set; }
        public string ReversalReason { get; set; }
    }

    public class DebitInstructionResponse
    {
        public string DebitInstRefNo { get; set; }

    }
}
