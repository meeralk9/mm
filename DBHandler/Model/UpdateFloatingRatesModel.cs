﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateFloatingRatesModel
    {  
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public List<FloatingRates> Rate { get; set; }
    }
    public class FloatingRates
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public string RateID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public decimal MarketRate { get; set; }

    }
}
