﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
   public class BuyerCCPInvoicesModel
    {
        
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Credit Program ID is required")]
        public string CreditProgramID { get; set; }
    }
    public class BuyerCCPInvoicesResponse
    {
        public string SupplierName { get; set; }
        public string TradeLicenseNum { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal InvoiceAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
    }
}
