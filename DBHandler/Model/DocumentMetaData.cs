﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    [Table("t_DocumentMetaData")]
    public class DocumentMetaData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ID { get; set; }
        public string documentID { get; set; }
        public string dataKey { get; set; }
        public string dataValue { get; set; }
        public int version { get; set; }
    }
}
