﻿using CommonModel;
using CoreHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AccountModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public Account Acc { get; set; }
    }

    public class AccountUpdateModel
    {
        
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string AccountID { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string Status { get; set; }
    }
}
