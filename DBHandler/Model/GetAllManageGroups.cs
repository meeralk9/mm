﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.GetAllManageGroups
{
    public class GetAllManageGroupsModel
    {

        public SignOnRq SignOnRq { get; set; }
    }

    public class GetAllManageGroupsResponse
    {
        public string GroupID { get; set; }
        public string ClientID { get; set; }
        public string GroupName { get; set; }
    }
}
