﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("t_AdditionalDocs")]
    public class AdditionalDoc
    {
        [Key]
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int ProductId { get; set; }
        public int AccountId { get; set; }
        public string UserId { get; set; }
        public string DrivingLicense { get; set; }
        public string PassportSignature { get; set; }
        public string BahrainResidentVisa { get; set; }
        public string BirthCerificate { get; set; }
        public string UtilityBill { get; set; }
        public string SalaryCertificate { get; set; }
        public string BankStatement { get; set; }
        public string GuardianVisa { get; set; }
        public string LegalDoc { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateTerminal { get; set; }
    }
}
