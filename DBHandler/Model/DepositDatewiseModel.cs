﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class GetRollOverRecieptModel
    {
        [Required(ErrorMessage = "Date is a mandatory field.")]
        public DateTime Date { get; set; }
        
        public SignOnRq SignOnRq { get; set; }

    }
    public class GetRollOverRecieptResponse
    {
        
        public string Name { get; set; }
        public string frmName { get; set; }
        public string OurBranchID { get; set; }
        public string RecieptID { get; set; }
        public string ProductID { get; set; }
        public string AccountID { get; set; }
        public string NAccountType { get; set; }
        public string NAccount { get; set; }
        public string NProductID { get; set; }
        public decimal Amount { get; set; }
        public decimal ExchangeRate { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal Rate { get; set; }
        public string AutoProfit { get; set; }
        public string PaymentPeriod { get; set; }
        public string Treatment { get; set; }
        public string AutoRollover { get; set; }
        public string OptionsAtMaturity { get; set; }
        public int? IsPremiumRate { get; set; }
        public string AdditionalData { get; set; }
        public string RefNo { get; set; }
        public int? IsUnderLien { get; set; }
        public int? IsLost { get; set; }
        public string LostRemarks { get; set; }
        public decimal? Interest { get; set; }
        public decimal? InterestPaid { get; set; }
        public DateTime? InterestPaidupTo { get; set; }
        public int? IsClosed { get; set; }
        public DateTime? CloseDate { get; set; }
        public decimal? Accrual { get; set; }
        public DateTime? AccrualUpto { get; set; }
        public string Penalty { get; set; }
        public string PenaltyRateType { get; set; }
        public decimal? PenaltyAmountRate { get; set; }
        public decimal? TotalPenaltyAmount { get; set; }
        public decimal? PartialAmount { get; set; }
        public decimal? TotalLienAmount { get; set; }
        public decimal? OldReceiptID { get; set; }
        public string ParentReceiptID { get; set; }
        public string ClientId { get; set; }
        public string Status { get; set; }

    }
}
