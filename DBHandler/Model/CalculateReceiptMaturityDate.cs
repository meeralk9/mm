﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.CalculateReceiptMaturityDate
{
    public class CalculateReceiptMaturityDateModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ReceiptID { get; set; }
    }
    public class CalculateReceiptMaturityDateResponse
    {
        public DateTime MaturityDate { get; set; }
        public decimal MaturityAmount { get; set; }
    }

}
