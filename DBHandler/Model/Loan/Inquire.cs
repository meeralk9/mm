﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DBHandler.Model.Loan
{


    public class InquireDisbursDetailsDTO
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Deal Ref No. is required")]
        public string DealRefNo { get; set; }
    }
    public class InquireDisbursDetailsFinal
    {
        public string accountID { get; set; }
        public string loanCategoryID { get; set; }
        public DateTime maturityDate { get; set; }
        public string method { get; set; }
        public string rePaymentMode { get; set; }
        public string gracePeriod { get; set; }
        public string rateType { get; set; }
        public string interestRate { get; set; }
        public string rateID { get; set; }
        public string margin { get; set; }
        public string loanAmount { get; set; }
        public string minEsFee { get; set; }
        public string esFeePercentage { get; set; }
        public string remarks { get; set; }
        public string disbursementMode { get; set; }
        public string disbAccountID { get; set; }
        public string feesAccountID { get; set; }
        public string trxAccountID { get; set; }
        public string interestAccountID { get; set; }
        public string recAccountID { get; set; }
        public string capAccountID { get; set; }
        public InquireCharges charges { get; set; }
        public List<InstalmentSchedule> instalmentSchedule { get; set; }
    }
    public class InquireCharges
    {
        public string chargeID { get; set; }
        public string chargeType { get; set; }
        public decimal charges { get; set; }
    }
    public class InstalmentSchedule
    {
        public int instNo { get; set; }
        public string paymentDate { get; set; }
        public decimal instAmount { get; set; }
        public decimal noOfDays { get; set; }
        public decimal principalAmount { get; set; }
        public decimal interestAmount { get; set; }
        public decimal outstandingAmount { get; set; }
        public string instamentOverdue { get; set; }
        public decimal overdueAmount { get; set; }
    }
    //public class InquireDisbursDetailsOut
    //{
    //    public string Status { get; set; }
    //    public string MessageCode { get; set; }
    //    public string accountID { get; set; }
    //    public string loanCategoryID { get; set; }
    //    public DateTime maturityDate { get; set; }
    //    public string method { get; set; }
    //    public string rePaymentMode { get; set; }
    //    public string gracePeriod { get; set; }
    //    public string rateType { get; set; }
    //    public string interestRate { get; set; }
    //    public string rateID { get; set; }
    //    public string margin { get; set; }
    //    public string loanAmount { get; set; }
    //    public string minEsFee { get; set; }
    //    public string esFeePercentage { get; set; }
    //    public string remarks { get; set; }
    //    public string disbursementMode { get; set; }
    //    public string disbAccountID { get; set; }
    //    public string feesAccountID { get; set; }
    //    public string trxAccountID { get; set; }
    //    public string interestAccountID { get; set; }
    //    public string recAccountID { get; set; }
    //    public string capAccountID { get; set; }
    //    public InquireCharges charges { get; set; }
    //    public List<InstalmentSchedule> instalmentSchedule { get; set; }
    //}
    //public class InquireCharges
    //{
    //    public string chargeID { get; set; }
    //    public string chargeType { get; set; }
    //    public decimal charges { get; set; }
    //}
    //public class InstalmentSchedule
    //{
    //    public int instNo { get; set; }
    //    public string paymentDate { get; set; }
    //    public decimal instAmount { get; set; }
    //    public decimal noOfDays { get; set; }
    //    public decimal principalAmount { get; set; }
    //    public decimal interestAmount { get; set; }
    //    public decimal outstandingAmount { get; set; }
    //    public string instamentOverdue { get; set; }
    //    public decimal overdueAmount { get; set; }
    //}


    public class InquireRefNumDebitInstCIFModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
    }

    public class InquireGuaranteeProductAvalIssuanceResponse
    {
        public string facID { get; set; }
        public string prodID { get; set; }
        public decimal guarAvailLimit { get; set; }
        public decimal guarReleasedLimit { get; set; }
        public string guarType { get; set; }
        public decimal margin { get; set; }
        public string currencyID { get; set; }
        public decimal apprTenor { get; set; }
        public string apprTenorCode { get; set; }
    }


    public class InquireIssGuaranteeAmedmentRefNumModel
    {

        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }

    }

    public class InquireIssGuaranteeAmedmentRefNumResponse
    {
        public string facID { get; set; }
        public string prodID { get; set; }
        public string guarRefNo { get; set; }
        public string guarStatus { get; set; }
        public decimal guarAvailLimit { get; set; }
        public decimal guarReleasedLimit { get; set; }
        public string guarType { get; set; }
        public decimal margin { get; set; }
        public string currencyID { get; set; }
        public string openCloseEnded { get; set; }
        public decimal apprTenor { get; set; }
        public string apprTenorCode { get; set; }
        public DateTime issuanceDate { get; set; }
        public DateTime expiryDate { get; set; }
        public string purposeOfGuar { get; set; }
        public string currAccID { get; set; }
        public string benefName { get; set; }
        public string benefTradeLicenseNo { get; set; }
        public string benefResidingAddr { get; set; }
        public string benefResidingCity { get; set; }
        public string benefResidingState { get; set; }
        public string benefResidingCountry { get; set; }
        public string commissionFee { get; set; }
        public string feeRecoveryAccount { get; set; }
    }

    public class InquireIssGuaranteeGuarRefNumModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Guarantee Ref Num is required")]
        public string GuarRefNo { get; set; }

    }

    public class InquireAvailableLimitModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Facility ID is required")]
        public decimal FacilityID { get; set; }

        [Required(ErrorMessage = "Product ID is required")]
        public string ProductID { get; set; }
        public string AccountID { get; set; }

    }


    public class InquireAvailableLimitResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public List<InquireAvailableLimitResponseOut> Limit { get; set; }
    }
    //public class InquireAvailableLimitResponseOut
    //{
    //    public string prodID { get; set; }
    //    public string accountLimitID { get; set; }
    //    public string currencyID { get; set; }
    //    public decimal availLimit { get; set; }
    //}

    public class InquireFeeRefNumModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Ref No is required")]
        public string refNo { get; set; }
        [Required(ErrorMessage = "Fee Type is required")]
        public string feeType { get; set; }
    }

    //newmodel
    public class InquireFeeRefNumResponse
    {
        public string clientID { get; set; }
        public string facID { get; set; }
        public string prodID { get; set; }
        public List<InquireFeeRefNumOut> Data { get; set; }
    }
    public class InquireFeeRefNumOut
    {
        public decimal feeAmt { get; set; }
        public decimal feeReverseAmt { get; set; }
        public decimal feeAvailReverseAmt { get; set; }
        public string feeChargeAccNo { get; set; }
    }
    public class InquireAvailableLimitResponseFinal
    {
        public List<InquireAvailableLimitResponseOut> Limit { get; set; }
    }
    public class InquireAvailableLimitResponseOut
    {
        public string prodID { get; set; }
        public string accountLimitID { get; set; }
        public string currencyID { get; set; }
        public decimal availLimit { get; set; }
    }

    //newmodel

    //public class InquireFeeRefNumResponse
    //{
    //    public string Status { get; set; }
    //    public string MessageCode { get; set; }
    //    public string clientID { get; set; }
    //    public string facID { get; set; }
    //    public string prodID { get; set; }
    //    public List<InquireFeeRefNumOut> Data { get; set; }
    //}
    //public class InquireFeeRefNumOut
    //{
    //    public decimal feeAmt { get; set; }
    //    public decimal feeReverseAmt { get; set; }
    //    public decimal feeAvailReverseAmt { get; set; }
    //    public string feeChargeAccNo { get; set; }
    //}
    public class InquireDealRefNumODPayCIFResponse
    {
        public List<InquireDealRefNumODPayCIFResponseOut> OverDue { get; set; }
    }
    public class InquireDealRefNumODPayCIFResponseOut
    {
        public string prodID { get; set; }
        public string dealRefNo { get; set; }
        public string accountID { get; set; }
        public decimal totalInitialAmt { get; set; }
        public decimal totalPaidAmt { get; set; }
        public decimal totalDuePrinciple { get; set; }
    }

    //public class InquireDealRefNumODPayCIFResponse
    //{
    //    public string prodID { get; set; }
    //    public string dealRefNo { get; set; }
    //    public string accountID { get; set; }
    //    public decimal totalInitialAmt { get; set; }
    //    public decimal totalPaidAmt { get; set; }
    //    public decimal totalDuePrinciple { get; set; }
    //}
    public class InquireDealRefNumODPayCIFOut
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public List<InquireDealRefNumODPayCIFResponse> Content { get; set; }
    }

    public class InquireODPaymentDealRefNumModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Deal Ref No is required")]
        public string dealRefNo { get; set; }
    }
    public class InquireODPaymentDealRefNumResponse
    {
        public string prodID { get; set; }
        public string accountID { get; set; }
        public decimal totalInitialAmt { get; set; }
        public decimal totalPaidAmt { get; set; }
        public decimal totalDuePrinciple { get; set; }
        public decimal totalDueInterest { get; set; }
        public decimal totalDuePenalty { get; set; }
        public decimal totalDueAmount { get; set; }
        public List<OverDuePayment> overduePayment { get; set; }
    }
    public class OverDuePayment
    {
        public decimal paymentNo { get; set; }
        public decimal principalAmount { get; set; }
        public decimal totalOverdueAmt { get; set; }
        public DateTime dueDate { get; set; }
    }
    //public class InquireODPaymentDealRefNumResponse
    //{
    //    public string Status { get; set; }
    //    public string MessageCode { get; set; }
    //    public string prodID { get; set; }
    //    public string accountID { get; set; }
    //    public decimal totalInitialAmt { get; set; }
    //    public decimal totalPaidAmt { get; set; }
    //    public decimal totalDuePrinciple { get; set; }
    //    public decimal totalDueInterest { get; set; }
    //    public decimal totalDuePenalty { get; set; }
    //    public decimal totalDueAmount { get; set; }
    //    public List<OverDuePayment> overduePayment { get; set; }
    //}

    //public class OverDuePayment
    //{
    //    public decimal paymentNo { get; set; }
    //    public decimal principalAmount { get; set; }
    //    public decimal totalOverdueAmt { get; set; }
    //    public DateTime dueDate { get; set; }
    //}

    public class InquireRefNumDebitInstCIFResponse
    {
        public string facID { get; set; }
        public string prodID { get; set; }
        public string refType { get; set; }
        public string refNo { get; set; }
    }

    public class InquireRefNumFeeChargesCIFResponse
    {
        public List<InquireRefNumFeeChargesCIFResponseOut> referenceInfo { get; set; }
    }
    public class InquireRefNumFeeChargesCIFResponseOut
    {
        public string facID { get; set; }
        public string prodID { get; set; }
        public string refType { get; set; }
        public string refNo { get; set; }
        public string feeType { get; set; }
        public decimal feeAmt { get; set; }
        public decimal feeAvailReverseAmt { get; set; }
        public decimal feeChargeAccNo { get; set; }
    }
    //public class InquireRefNumFeeChargesCIFResponse
    //{
    //    public string facID { get; set; }
    //    public string prodID { get; set; }
    //    public string refType { get; set; }
    //    public string refNo { get; set; }
    //    public string feeType { get; set; }
    //    public decimal feeAmt { get; set; }
    //    public decimal feeAvailReverseAmt { get; set; }
    //    public decimal feeChargeAccNo { get; set; }
    //}

    public class InquireDealRefNumEarlySettleCIFResponse
    {
        public List<InquireDealRefNumEarlySettleCIFOut> Deals { get; set; }
    }

    public class InquireDealRefNumEarlySettleCIFOut
    {
        public string prodID { get; set; }
        public string accountID { get; set; }
        public string dealRefNo { get; set; }
        public decimal principalBalance { get; set; }
    }
    //public class InquireDealRefNumEarlySettleCIFResponse
    //{
    //    public string Status { get; set; }
    //    public string MessageCode { get; set; }

    //    public List<InquireDealRefNumEarlySettleCIFOut> Data { get; set; }
    //}
    //public class InquireDealRefNumEarlySettleCIFOut
    //{
    //    public string prodID { get; set; }
    //    public string accountID { get; set; }
    //    public string dealRefNo { get; set; }
    //    public decimal principalBalance { get; set; }
    //}
}
