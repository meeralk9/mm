﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class GetAllOnlineUserModel
    {
       
        public SignOnRq SignOnRq { get; set; }
    }

    //Response:<list>
		
	public class GetAllOnlineUserResponse
    {
        public int? ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
