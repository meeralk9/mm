﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DBHandler.Model.LoanSimulatorV2
{
    public class LoanSimulatorDtoV2
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Loan Amount is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Loan Amount must be between 1 to 999,999,999,999.99")]
        public decimal LoanAmount { get; set; }
        //[Required(ErrorMessage = "Tenure Type is required")]
        public string TenureType { get; set; }
        //[Required(ErrorMessage = "Tenure is required")]
        //[Range(1, 30, ErrorMessage = "Tenure must be between 1 to 30")]
        public decimal Tenure { get; set; }
        [Required(ErrorMessage = "Interest Rate is required")]
        [Range(1, 100, ErrorMessage = "Interest Rate must be between 1 to 100")]
        public decimal InterestRate { get; set; }
        [Required(ErrorMessage = "Method is required")]
        public string Method { get; set; }
        [Required(ErrorMessage = "Re-Payment Mode is required")]
        public string RePaymentMode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        [AllowNull]
        public List<ManualScheduleD> ManualSchedule { get; set; }
        public int MoretoreumPeriod { get; set; }
    }
    public class ManualScheduleD
    {
        public int Percentage { get; set; }
        public DateTime DueDate { get; set; }
    }


    public class LoanSimulatorOutV2
    {
        public string ReferenceNo { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public string PayOffDate { get; set; }
        public List<ScheduleD> Schedule { get; set; }
    }
    public class ScheduleD
    {
        public int InstNo { get; set; }
        public string PaymentDate { get; set; }
        public decimal InstAmount { get; set; }
        public int NoOfDays { get; set; }
        public decimal PrincipalAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public string Type { get; set; }
    }
}
