﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.NoLiabilityLetterModel
{
    public class NoLiabilityLetterModel
    {
        public SignOnRq SignOnRq { get; set; }
        public string ClientID { get; set; }
    }
    public class NoLiabilityLetterResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
    }
}
