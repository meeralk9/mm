﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace DBHandler.Model.BuyerCCPDetails
{

    public class BuyerCCPDetailsModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Credit Program ID is required")]
        public string CreditProgramID { get; set; }
    }

    public class BuyerCCPDetailsResponse
    {
        public string CreditProgramID { get; set; }
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public string AccountTitle { get; set; }
        public string CurrencyID { get; set; }
        public decimal EffectiveRate { get; set; }
        public Supplier Supplier { get; set; }
    }
    public class Supplier
    {
        public string SupplierName { get; set; }
        public string TradeLicenseNo { get; set; }
        public decimal Limit { get; set; }
        public decimal AccountLimit { get; set; }
        public decimal AvailableLimit { get; set; }
    }
}
