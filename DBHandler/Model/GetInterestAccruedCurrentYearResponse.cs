﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class GetInterestAccruedCurrentYearResponse
    {
        public decimal TotalAccrual { get; set; }
    }
}
