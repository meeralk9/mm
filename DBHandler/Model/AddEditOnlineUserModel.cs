﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditOnlineUserModel
    {
       
        public SignOnRq SignOnRq { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        public string Email { get; set; }
        public string MobileNo { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public int UserType { get; set; }
        public DateTime? DOB { get; set; }
        public string ProfileImageUrl { get; set; }
        public byte[] UserImage { get; set; }
        public bool? IsAdmin { get; set; }
        public string TimeZone { get; set; }
        public string LanguageCode { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? Gender { get; set; }
        public int? UserID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string RolesAssigned { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ActionTypeAssigned { get; set; }
        public string PassportNo { get; set; }
        public string NationalIDNo { get; set; }
        public string Designation { get; set; }
        public int? CountryID { get; set; }
        public string Address { get; set; }
        public decimal? DailyLimit { get; set; }
        public decimal? WPSFileTotalAmount { get; set; }
        public decimal? WPSPerDayTotalAmount { get; set; }
        public string SubRolesAssigned { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int SerialNo { get; set; }

    }
    public class AddEditOnlineUserResponse
    {
        public int? ID { get; set; }
    }
}
