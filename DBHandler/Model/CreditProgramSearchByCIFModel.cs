﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.CreditProgramSearchByCIFModel
{
    public class CreditProgramSearchByCIFModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Credit Program ID is required")]
        public string ClientID { get; set; }
    }

    public class CreditProgramSearchByCIFResponse
    {
        public string CreditProgramName { get; set; }
        public decimal? CreditProgramLimit { get; set; }
        public DateTime? CreditProgramExpiryDate { get; set; }
        public string CreditProgramId { get; set; }
    }

}
