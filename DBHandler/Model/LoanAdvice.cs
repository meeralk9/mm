﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.LoanAdvice
{
    public class LoanAdviceModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public RootObject RootObject { get; set; }
    }

    public class SignOnRq
    {
        public string LogId { get; set; }
        public DateTime DateTime { get; set; }
        public string ChannelId { get; set; }
        public string OurBranchId { get; set; }
    }

    public class RootObject
    {
        public SignOnRq SignOnRq { get; set; }
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public string DealID { get; set; }
    }


    //response

    public class Status
    {
        public string code { get; set; }
        public string severity { get; set; }
        public string statusMessage { get; set; }
    }

    public class LoanDetails
    {
        public string loanAccount { get; set; }
        public string accountName { get; set; }
        public int approvedLimit { get; set; }
        public int availableForDisbursal { get; set; }
        public string loanType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string currency { get; set; }
        public int amount { get; set; }
        public string dealReferenceNumber { get; set; }
        public int totalNumberOfInstallments { get; set; }
        public double interestRate { get; set; }
        public string paymentFrequency { get; set; }
        public string fundingAccount { get; set; }
    }

    public class PaymentScheduler
    {
        public string valueDate { get; set; }
        public int amount { get; set; }
        public double interestRate { get; set; }
        public int principalAmountPaid { get; set; }
        public int interestPaid { get; set; }
        public int outstandingAmount { get; set; }
    }

    public class Content
    {
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string contactDetails { get; set; }
        public string adviceDate { get; set; }
        public LoanDetails loanDetails { get; set; }
        public List<PaymentScheduler> paymentScheduler { get; set; }
    }

    public class ResponseRootObject
    {
        public DateTime requestDateTime { get; set; }
        public string logId { get; set; }
        public Status status { get; set; }
        public Content content { get; set; }
    }
}
