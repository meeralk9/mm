﻿using CommonModel;
using DBHandler.Model.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class InquireFIKycCustomerCorporateViewV2
    {
        [Required(ErrorMessage = "Customer data require")]
        public InquireFIKycCustomerCorporateResposneV2 Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<InquireFIKycShareHolderV2> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<InquireFIKycAuthorisedSignatoriesVoewV2> Authignatories { get; set; }

        [Required(ErrorMessage = "BoardDirectors require")]
        public List<InquireFIKycBoardDirectorsV2> BoardDirectors { get; set; }

        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<InquireFIKycBeneficialOwnersV2> BeneficialOwners { get; set; }

        public List<InquireFIKycSicCodeCorpModelV2> SicCodeCorpModel { get; set; }
        public List<InquireFIKycClientSellerV2> ClientSeller { get; set; }
        public List<InquireFIKycClientBuyerV2> ClientBuyer { get; set; }
        public List<InquireFIKycCompanyInFoV2> CompanyInFo { get; set; }
    }
    public class InquireFIKycCustomerCorporateResposneV2
    {
        public string ReasonACorp { get; set; }
        public string ClientID { get; set; }

        public string Name { get; set; }

        public string CategoryId { get; set; }

        public string GroupCode { get; set; }

        public string RManager { get; set; }

        public string RiskProfile { get; set; }

        public string EntityNameCorp { get; set; }

        public string EntityTypeCorp { get; set; }

        public Nullable<System.DateTime> YearofestablishmentCorp { get; set; }

        public string RegisteredAddressCorp { get; set; }

        public string ContactName1Corp { get; set; }

        public string ContactID1Corp { get; set; }

        public string IDTypeCorp { get; set; }

        public string Address1Corp { get; set; }


        public string PhoneNumberCorp { get; set; }

        public string AlternatePhonenumberCorp { get; set; }

        public string EmailIDCorp { get; set; }

        public string ContactName2Corp { get; set; }


        public string ContactID2Corp { get; set; }

        public string IDType2 { get; set; }

        public string Address2Corp { get; set; }

        public string PhoneNumber2Corp { get; set; }

        public string AlternatePhonenumber2Corp { get; set; }

        public string EmailID2Corp { get; set; }

        public string BusinessActivity { get; set; }

        public string SICCodesCorp { get; set; }

        public string IndustryCorp { get; set; }

        public string PresenceinCISCountryCorp { get; set; }

        public string DealingwithCISCountryCorp { get; set; }

        public string TradeLicenseNumberCorp { get; set; }

        public string CountryofIncorporationCorp { get; set; }

        public Nullable<System.DateTime> TradeLicenseIssuedateCorp { get; set; }
        public Nullable<System.DateTime> TradeLicenseExpirydateCorp { get; set; }

        public string TradeLicenseIssuanceAuthorityCorp { get; set; }

        public string TRNNoCorp { get; set; }

        public string PEPCorp { get; set; }
        public string BlackListCorp { get; set; }

        public Nullable<System.DateTime> KYCReviewDateCorp { get; set; }

        public string FATCARelevantCorp { get; set; }
        public string CRSRelevantCorp { get; set; }
        public string USAEntityCorp { get; set; }
        public string FFICorp { get; set; }

        public string FFICategoryCorp { get; set; }
        public string GIINNoCorp { get; set; }
        public string GIINNACorp { get; set; }
        public string SponsorNameCorp { get; set; }
        public string SponsorGIIN { get; set; }
        public string NFFECorp { get; set; }
        public string StockExchangeCorp { get; set; }
        public string TradingSymbolCorp { get; set; }


        public string USAORUAECorp { get; set; }

        public string TINAvailableCorp { get; set; }

        public string TinNo1Corp { get; set; }

        public string TaxCountry1Corp { get; set; }
        public string TinNo2Corp { get; set; }

        public string TaxCountry2Corp { get; set; }
        public string TinNo3Corp { get; set; }

        public string TaxCountry3Corp { get; set; }

        public string TinNo4Corp { get; set; }

        public string TaxCountry4Corp { get; set; }
        public string TinNo5Corp { get; set; }

        public string TaxCountry5Corp { get; set; }


        public string FATCANoReasonCorp { get; set; }

        public string ReasonBCorp { get; set; }


        public string CompanyWebsite { get; set; }

        public string CompCashIntensive { get; set; }
        public string PubLimCompCorp { get; set; }

        public string RegAddCityCorp { get; set; }

        public string RegAddCountryCorp { get; set; }

        public string RegAddStateCorp { get; set; }

        public string Add1CountryCorp { get; set; }
        public string Add1StateCorp { get; set; }
        public string Add1CityCorp { get; set; }

        public string Add2CountryCorp { get; set; }
        public string Add2StateCorp { get; set; }
        public string Add2CityCorp { get; set; }

        public Nullable<System.DateTime> VATRegistered { get; set; }


        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurEmiratesID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurTypeID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string JurAuthorityID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        public string POBox { get; set; }

        public string JurOther { get; set; }

        public string GroupID { get; set; }
        public string ParentClientID { get; set; }
        public int? IsVip { get; set; }


        // new fields
        public string LicenseType { get; set; }
        public string CountryID { get; set; }
        public DateTime? MOAMOU { get; set; }
        //public string ShareHolderAddress { get; set; }
        //public string ShareHolderCountryID { get; set; }
        //public string ResidenceShareHolderCountryID { get; set; }
        //public string ShareHolderStateID { get; set; }
        //public string ShareHolderCity { get; set; }
        public string SigningAuthority { get; set; }
        public string POAResidenceCountry { get; set; }
        public string POAAddress { get; set; }
        public string POACountry { get; set; }
        public string POAState { get; set; }
        public string POACity { get; set; }
        //public string DirectorAddress { get; set; }
        //public string DirectorCountry { get; set; }
        //public string DirectorState { get; set; }
        //public string DirectorCity { get; set; }
        // public string DirectoryResidenceCountry { get; set; }
        public string AuditedFinancials { get; set; }
        public decimal? Assets { get; set; }
        public decimal? Revenue { get; set; }
        public decimal? Profits { get; set; }
        public decimal? BusinessTurnOver { get; set; }
        public decimal? CompanyCapital { get; set; }
        public string CompanyCountry { get; set; }
        public decimal? ExpectedDebitTurnOver { get; set; }
        public decimal? ExpectedMaxDebitTurnOver { get; set; }
        public decimal? ExpectedCreditTurnOver { get; set; }
        public decimal? ExpectedMaxCreditTurnOver { get; set; }
        public string CompanyAddress { get; set; }
        public decimal? ExpectedCashVolume { get; set; }
        public decimal? ExpectedCashValue { get; set; }
        public decimal? ChequesVolume { get; set; }
        public decimal? ChequesValue { get; set; }
        public decimal? SalaryPaymentVolume { get; set; }
        public decimal? SalaryPaymentValue { get; set; }
        public decimal? IntraBankRemittancesVolume { get; set; }
        public decimal? IntraBankRemittancesValue { get; set; }
        public decimal? InternationalRemittancesVolume { get; set; }
        public decimal? InternationalRemittancesValue { get; set; }
        public decimal? DomesticRemittanceVolume { get; set; }
        public decimal? DomesticRemittanceValues { get; set; }
        public decimal? ExpectedCashDebitVolume { get; set; }
        public decimal? ExpectedCashDebitValue { get; set; }
        public decimal? ChequeDebitVolume { get; set; }
        public decimal? ChequeDebitValue { get; set; }
        public decimal? SalaryDVolume { get; set; }
        public decimal? SalaryDValue { get; set; }
        public decimal? IntraDVolume { get; set; }
        public decimal? IntraDValue { get; set; }
        public decimal? InternationalDVolume { get; set; }
        public decimal? InternationalDValue { get; set; }
        public decimal? DomesticDRemittanceValues { get; set; }
        public decimal? DomesticDRemittanceVolume { get; set; }
        public decimal? CreditTradeFinanceVolume { get; set; }
        public decimal? CreditTradeFinanceValue { get; set; }
        public decimal? TradeFinanceVolumeD { get; set; }
        public decimal? TradeFinanceValueD { get; set; }
        public decimal? NoOfStaff { get; set; }
        public string CompanyActivites { get; set; }
        public string BusinessNatureRelation { get; set; }
        public string RelationalCompanyStructure { get; set; }
        public string CompanyGeoGraphicCourage { get; set; }
        public string UBOBackground { get; set; }
        public string ProductAndServicesOffered { get; set; }
        public int? IsDisclosureActivity { get; set; }
        public string DisclosureDetails { get; set; }
        public int? IsSanctionCountry { get; set; }
        public string SanctionRemarks { get; set; }
        public int? IsOfficeInSactionCountry { get; set; }
        public string SanctionCountries { get; set; }
        public string BusinessNatureSanctionCountry { get; set; }
        public int? IsProductOrigninatedCountry { get; set; }
        public string OriginatedProductCountry { get; set; }
        public string OriginatedCountryRemarks { get; set; }
        public int? IsExportGoods { get; set; }
        public string ExportGoodsCountry { get; set; }
        public string ExportGoodsRemarks { get; set; }
        public int? isRecievedTransFromSanctionCountry { get; set; }
        public string TransactionSanctionCountry { get; set; }
        public string TransactionSanctionRemarks { get; set; }
        public string CorporateStructureChart { get; set; }
        public string VisitConducted { get; set; }
        public string SanctionAssestment { get; set; }
        public string PepAssessment { get; set; }
        public string AdverseMedia { get; set; }
        public string AccountOpeningPurpose { get; set; }

        //public string Status { get; set; }

        //New Cust Fields

        public string POAName { get; set; }
        public string POANationality { get; set; }
        public string POADateofBirth { get; set; }
        public string POAPlaceOfBirth { get; set; }
        public string POAGender { get; set; }
        public string selectionOfPayments { get; set; }
        public Nullable<decimal> DepositValue { get; set; }
        public Nullable<decimal> PaymentValue { get; set; }
        public Nullable<decimal> LoansValue { get; set; }
        public string PlaceofIncorporation { get; set; }
        public string Designation1Corp { get; set; }
        public string Department1Corp { get; set; }
        public string IDExpiryDateCorp { get; set; }
        public string Designation2Corp { get; set; }
        public string Department2Corp { get; set; }
        public string IDExpiryDate2Corp { get; set; }
        public string DealingwithCISTextarea { get; set; }
        public string IsOfficeInSactionCountrylist { get; set; }
        public string IsProductOrigninatedCountrylist { get; set; }
        public string ExportGoodsCountrylist { get; set; }
        public string TransactionSanctionCountrylist { get; set; }

    }
    public class InquireFIKycShareHolderV2
    {
        public decimal? ShareHolderID { get; set; }

        public string ShareHolderName { get; set; }
        public string ShareHolderAddress { get; set; }
        public string ShareholderType { get; set; }
        public string ShareholdersIDtype { get; set; }
        public string ShareholdersIDNo { get; set; }
        public Nullable<System.DateTime> ShareholdersIDExpirydate { get; set; }
        public string CountryofIncorporation { get; set; }
        public string BoardMember { get; set; }
        public string AuthorisedSignatory { get; set; }
        public string BeneficialOwner { get; set; }
        public string ResidentNonResident { get; set; }
        public string CountryofResidence { get; set; }
        public decimal? OwnershipPercentage { get; set; }
        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string USCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAEntity { get; set; }
        public string FFI { get; set; }
        public string FFICategory { get; set; }
        public string GIINNo { get; set; }
        public string GIINNA { get; set; }
        public string SponsorName { get; set; }
        public string SponsorGIIN { get; set; }
        public string NFFE { get; set; }
        public string StockExchange { get; set; }
        public string TradingSymbol { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string SHIDIssuanceCountry { get; set; }
        public string IDGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNationality { get; set; }

        public string SHCity { get; set; }
        public string SHState { get; set; }
        public string SHCountry { get; set; }
        public string JurEmiratesID { get; set; }
        public string JurTypeID { get; set; }
        public string JurAuthorityID { get; set; }
        public string JurOther { get; set; }
        public string POBox { get; set; }
        public string ParentShareHolderId { get; set; }
        public string EntityType { get; set; }

        //new fields
        public string ResidenceShareHolderCountryID { get; set; }
        public string PlaceofBirth { get; set; }
        public string GroupCodeType { get; set; }
    }
    public class InquireFIKycAuthorisedSignatoriesVoewV2
    {
        public decimal? AuthorisedSignatoriesID { get; set; }
        public string AuthorisedsignatoriesName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string AuthorisedsignatoryIDtype1 { get; set; }
        public string AuthorisedsignatoryIDNumber1 { get; set; }
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate1 { get; set; }
        public string AuthorisedsignatoryIDIssueCountry1 { get; set; }
        public string AuthorisedsignatoryIDtype2 { get; set; }
        public string AuthorisedsignatoryIDNumber2 { get; set; }
        public Nullable<System.DateTime> AuthorisedsignatoryIDExpirydate2 { get; set; }
        public string AuthorisedsignatoryIDIssueCountry2 { get; set; }
        public string AuthorisedsignatoryVisaIssuedby { get; set; }
        public string AuthorisedsignatoryVisaNumber { get; set; }
        public Nullable<System.DateTime> AuthorisedsignatoryVisaExpiry { get; set; }
        public string Remarks { get; set; }
        public string GroupID { get; set; }
        //  public string SignUpload { get; set; }

        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string AreyouaUSCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string IDGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNationality { get; set; }

        public string ASCity { get; set; }
        public string ASState { get; set; }
        public string ASCountry { get; set; }
        //New fields 

        public string ResidentNonResident { get; set; }

        public string Designation { get; set; }
        //public string SignUpload1 { get; set; }
    }
    public class InquireFIKycBoardDirectorsV2
    {
        public decimal? BoardDirector { get; set; }
        public string BoardDirectorName { get; set; }
        public string Designation { get; set; }
        public string BoardDirectorAddress { get; set; }
        public Nullable<System.DateTime> BoardDirectorIDExpirydate { get; set; }
        public string ResidentNonResident { get; set; }
        public string CountryofResidence { get; set; }
        public string BoardDirectorIDtype { get; set; }
        public string BoardDirectorIDNo { get; set; }

        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string AreyouaUSCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string IDGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNationality { get; set; }

        public string BDCity { get; set; }
        public string BDState { get; set; }
        public string BDCountry { get; set; }

        public string BODPlaceOfBirth { get; set; }
        public string BODGender { get; set; }
        public string BODMobileNo { get; set; }

        public string BODEmail { get; set; }
    }
    public class InquireFIKycBeneficialOwnersV2
    {
        public decimal? BeneficialOwnersID { get; set; }
        public string BeneficialName { get; set; }
        public string BeneficialAddress { get; set; }
        public string BeneficialIDNo { get; set; }
        public Nullable<System.DateTime> BeneficialIDExpirydate { get; set; }
        public string ResidentNonResident { get; set; }
        public string CountryOfResidence { get; set; }
        public string BeneficialIDtype { get; set; }


        public string Blacklist { get; set; }
        public string PEP { get; set; }
        public string FATCARelevant { get; set; }
        public string CRSRelevant { get; set; }
        public string AreyouaUSCitizen { get; set; }
        public string AreyouaUSTaxResident { get; set; }
        public string WereyoubornintheUS { get; set; }
        public string USAORUAE { get; set; }
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        public string TaxCountry5 { get; set; }
        public string FATCANoReason { get; set; }
        public string ReasonB { get; set; }

        public string IDGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IDNationality { get; set; }

        public string BOCity { get; set; }
        public string BOState { get; set; }
        public string BOCountry { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string IDIssuanceCountry { get; set; }
    }
    public class InquireFIKycSicCodeCorpModelV2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }
    public class InquireFIKycClientSellerV2
    {
        public string SellerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string Website { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }
    public class InquireFIKycClientBuyerV2
    {
        public string ClientBuyerID { get; set; }
        public string Name { get; set; }
        public int? IsWebsite { get; set; }
        public string WebsiteURL { get; set; }
        public string Profile { get; set; }
        public string Country { get; set; }
        public decimal? TransactionVolume { get; set; }
        public decimal? TransactionValue { get; set; }
        public string AdverseInfo { get; set; }
    }

    public class InquireFIKycCompanyInFoV2
    {
        public string ClientID { get; set; }
        public string SerialID { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string BankCountry { get; set; }
    }
}
