﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class CorporateInfo
    {
        public string ClientID { get; set; }
        public List<IDName> ShareHolderID { get; set; }
        public List<IDName> AuthorisedSignatorieID { get; set; }
        public List<IDName> BeneficialOwner { get; set; }
        public List<IDName> BoardDirectors { get; set; }
    }

    public class IDName
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class CustomerInfo
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string ClientID { get; set; }
        public string idno { get; set; }
        public string PhoneNum1 { get; set; }
        public string PhoneNum2 { get; set; }

        public string MobileNum { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPromoCode { get; set; }
        public DateTime? KycReviewDate { get; set; }
        public DateTime? DOE { get; set; }

    }
}
