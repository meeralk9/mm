﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class TVkeyRegisteredUser
    {
        public int  id { get; set; }
        public string ts { get; set; }
        public string UserId { get; set; }
        public string DeviceId { get; set; }
        public string PnsToken { get; set; }
        public string Platform { get; set; }
        public string AdditionalData { get; set; }
    }
}
