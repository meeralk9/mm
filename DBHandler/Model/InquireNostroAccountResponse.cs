﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class InquireNostroAccountResponse
    {

        public string ClientID { get; set; }
        public string Name { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
        public string AccountType { get; set; }
        public string StatementAddress { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string StatementFrequency { get; set; }
        public string NatureOfAccount { get; set; }
        public string RelationshipFunction { get; set; }
        public string NostroAccountNumber { get; set; }
        public string NostroIBAN { get; set; }
        public string NostroSWIFT { get; set; }
        public string NostroAddress { get; set; }
        public string NostroCountry { get; set; }
        public decimal? MinBalanceRequirement { get; set; }
        public string NostroBankName { get; set; }
        public string NostroBranchName { get; set; }
        public string NostroSortCode { get; set; }
        public string OpenDate { get; set; }
        public string AccountStatus { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public DateTime? ActivationDate { get; set; }
    }
}
