﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;

namespace DBHandler.Model
{
    public class CIFDetailInquiry
    {
       
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }

    public class SignatureModel
    {
        public string Signature { get; set; }
        public string ClientID { get; set; }
        public string AuthID { get; set; }
    }

    public class CIFBoardDirectorsDTO
    {
      
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public BoardDirectors BoardDirectors { get; set; }
    }

    public class CIFBeneficialOwnersDTO
    {    
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public BeneficialOwners BeneficialOwners { get; set; }
    }

    public class CIFAuthorisedSignatoriesDTO
    {      
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public AuthorisedSignatories AuthorisedSignatories { get; set; }
    }
    public class CIFShareHolderDetiilDTO
    {
        
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public ShareHolder ShareHolder { get; set; }
    }

    public class UpdateSignatureModel
    {
       
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ID { get; set; }
        public string Signature { get; set; }
        public string GroupID { get; set; }
    }


    public class DeleteRequestForClientDetail
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ID { get; set; }
    }

    public class RimInquiry
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }

   
   




    public class CprInquiry
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }

    //public class CustomerModel
    //{
    //    [Required(ErrorMessage = " is a mandatory field.")]
    //    public SignOnRq SignOnRq { get; set; }
    //    public Customer Cust { get; set; }
    //}
}
