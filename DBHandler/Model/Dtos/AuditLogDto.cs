﻿using System;

namespace DBHandler.Model.Dtos
{
    public class AuditLogDto
    {
        public string Id { get; set; }
        public string ApiLogId { get; set; }
        public string RequestType { get; set; }
        public string RequestBy { get; set; }
        public DateTime? RequestTime { get; set; }
        public string RequestTerminal { get; set; }
        public string RequestMessage { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
    }
}
