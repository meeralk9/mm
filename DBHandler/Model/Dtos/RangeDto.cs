﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Dtos
{
   public class RangeDto
    {
        [Required]
        public decimal Value { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
