﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class MobileNumberDto
    {
        //+971 58 558 1283
        [Required(ErrorMessage = "Please enter Mobile Number.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid Mobile Number")]
        [StringLength(9, ErrorMessage = "Mobile Number must be at least {2} and at max {1} characters long.", MinimumLength = 7)]
        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Please enter PhoneCode")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid Phone Code")]
        public string PhoneCode { get; set; }

        [Required(ErrorMessage = "Please Provide IsoCode")]
        public string IsoCode3 { get; set; }

        public BaseClass BaseClass { get; set; }

    }
}
