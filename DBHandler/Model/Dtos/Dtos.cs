﻿

using DBHandler.Helper;
using DBHandler.Model;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CommonModel;


namespace DBHandler.Model.Dtos
{

    public class TransactionDescription
    {
        public string DescriptionID { get; set; }
        public string Description { get; set; }
        public bool IsCredit { get; set; }
    }

    public class CustomerModel
    {
        
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Customer data require")]
        public CustomerCorporateRequest Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<ShareHolder> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<AuthorisedSignatories> Authignatories { get; set; }

        //[Required(ErrorMessage = "BoardDirectors require")]
        public List<BoardDirectors> BoardDirectors { get; set; }

        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<BeneficialOwners> BeneficialOwners { get; set; }

        public List<SicCodeCorpModelV2> SicCodeCorp { get; set; }
    }

    public class SicCodeCorpModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }

    public class InquireFIKycSicCodeCorpModelV2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }
    public class SicCodeCorpModelV2
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string SicCodeCorp { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        [StringRangeAttribute(AllowableValues = new[] { "P", "S" }, ErrorMessage = "- Valid value is either P or S")]
        public string Type { get; set; }
    }

    public class CustomerCorporateView
    {
        [Required(ErrorMessage = "Customer data require")]
        public CustomerCorporateResposne Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<ShareHolder> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<AuthorisedSignatoriesVoew> Authignatories { get; set; }

        [Required(ErrorMessage = "BoardDirectors require")]
        public List<BoardDirectors> BoardDirectors { get; set; }

        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<BeneficialOwners> BeneficialOwners { get; set; }

        public List<SicCodeCorpModel> SicCodeCorpModel { get; set; }
    }
    
    
    


    public class CreateCIF
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public InsertCustomerRetail Cust { get; set; }
        public SignOnRq SignOnRq { get; set; }
    }

    public class UpdateCIF
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public UpdateCustomerRetail Cust { get; set; }
        public SignOnRq SignOnRq { get; set; }      
    }

    public class RetailCustomerInquiryModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public string ClientID { get; set; }
    }

    public class UpdateCorporateCIF
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public InsertCustomerCorporate Cust { get; set; }
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "SicCodeCorp data require")]
        public List<SicCodeCorpModel> SicCodeCorp { get; set; }
    }

    public class UpdateCorporateCifWithChildsModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Customer data require")]
        public InsertCustomerCorporate Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<ShareHolder> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<AuthorisedSignatories> Authignatories { get; set; }

        //[Required(ErrorMessage = "BoardDirectors require")]
        public List<BoardDirectors> BoardDirectors { get; set; }

        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<BeneficialOwners> BeneficialOwners { get; set; }

        public List<SicCodeCorpModel> SicCodeCorp { get; set; }

    }


    public class CreateFIKycLiteCustomerModel
    {   
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Customer data require")]
        public DBHandler.Model.FIKycLiteCustomer.InsertCustomerCorporate Cust { get; set; }

        [Required(ErrorMessage = "Shareholder require")]
        public List<DBHandler.Model.FIKycLiteCustomer.ShareHolder> ShareHolders { get; set; }

        [Required(ErrorMessage = "AuthorisedSignatories require")]
        public List<DBHandler.Model.FIKycLiteCustomer.AuthorisedSignatories> Authignatories { get; set; }

        public List<DBHandler.Model.FIKycLiteCustomer.BoardDirectors> BoardDirectors { get; set; }

        [Required(ErrorMessage = "BeneficialOwners require")]
        public List<DBHandler.Model.FIKycLiteCustomer.BeneficialOwners> BeneficialOwners { get; set; }

        public List<SicCodeCorpModel> SicCodeCorp { get; set; }
       
    }

    public class Riminquiryrim
    {
        public string ClientID { get; set; }
        public SignOnRq SignOnRq { get; set; }


    }

    public class Riminquiry
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "ID no is required.")]
        public string idno { get; set; }

    }

    public class CreateAccount
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public Account Acc { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string SignatureInstructions { get; set; }


        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public List<string> ClientIDS { get; set; }
    }

    public class UpdateAccountModel
    {
        
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public UpdateAccount Acc { get; set; }

        [MaxLength(200, ErrorMessage = "- Character length should not be more than 200.")]
        public string SignatureInstructions { get; set; }
    }

    public class CreateCorporateAccount
    {
       
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public AccountCorporate Acc { get; set; }
    }

    public class UpdateCorporateAccountModel
    {
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = " is a mandatory field.")]
        public UpdateAccountCorporate Acc { get; set; }
    }

    public class MobileData
    {
        public string IsoCode3 { get; set; }
        public string PhoneCode { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class MobileOtp
    {
        [Required(ErrorMessage = "Please enter Mobile OTP.")]
        public string Otp { get; set; }

        [Required(ErrorMessage = "Please enter Mobile IsoCode3.")]
        public string IsoCode3 { get; set; }

        [Required(ErrorMessage = "Please enter Mobile PhoneCode.")]
        public string PhoneCode { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class StandingOrderDto
    {
        public string Otp { get; set; }
        public string BeneficaryId { get; set; }
        public string Frequency { get; set; }
        public int NumberOfTransfer { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class ForgetPasswordDto
    {
        public string UserName { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class GenericOtp
    {
        public string OtpType { get; set; }
        public BaseClass BaseClass { get; set; }

    }

    public class ChangeForgetPassDto
    {
        public string Otp { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string ReEnterPassword { get; set; }

        public string UserName { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class VerifyOTPDto
    {

        [Display(Name = "ID No.")]
        [Required(ErrorMessage = "Please enter ID No.")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid ID No.")]
        public string IDNo { get; set; }

        [Required(ErrorMessage = "Please enter OTP Code.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid OTP Code.")]
        [StringLength(6, ErrorMessage = "OTP Code must be 6 characters long.", MinimumLength = 6)]
        public string OTPCode { get; set; }

        [Required(ErrorMessage = "Please enter OTP Type.")]
        public string OTPType { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class CheckDto
    {
        [Display(Name = "First name")]
        [Required(ErrorMessage = "Please enter First name.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FirstName { get; set; }
    }



    public class ChoiceOfExpressDTO
    {

        [Required(ErrorMessage = "Health Care Required.")]

        public bool HealthCare { get; set; }
        [Required(ErrorMessage = "Education Required.")]

        public bool Education { get; set; }
        [Required(ErrorMessage = "Environemnt Required.")]

        public bool Environment { get; set; }
        [Required(ErrorMessage = "Arts Required.")]

        public bool ArtsCulture { get; set; }
        [Required(ErrorMessage = "HumanService Required.")]

        public bool HumanServices { get; set; }
        [Required(ErrorMessage = "PublicService Required.")]

        public bool PublicServices { get; set; }
        [Required(ErrorMessage = "Device ID Required.")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class AccountDetailDto
    {
        [Display(Name = "First name")]
        [Required(ErrorMessage = "Please enter First name.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FirstName { get; set; }

        [Display(Name = "Middle name")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.")]
        public string MiddleName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "Please enter Last name.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Please enter Gender.")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Gender { get; set; }

       // [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter Address.")]
        [StringLength(140, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter Id Number.")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string IdNumber { get; set; }

        //[Display(Name = "Date of Birth")]
        [Required(ErrorMessage = "Start date and time cannot be empty")]
        [DataType(DataType.DateTime)]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please enter Expiry date.")]
        [DataType(DataType.DateTime)]
        public DateTime IdExpiry { get; set; }
    }

    public class AccountIdDto
    {
        [Display(Name = "Account ID")]
        [Required(ErrorMessage = "Please enter Account ID")]
        [RegularExpression("^[1-9]([0-9])+$", ErrorMessage = "Please enter a valid Account ID")]
        [StringLength(12, ErrorMessage = "Account ID must be 12 characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }
    }

    public class BahrainIdDto
    {
        [Display(Name = "ID No.")]
        [Required(ErrorMessage = "Please enter ID No.")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid ID No.")]
        public string IDNo { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class CustomerIdDto
    {
        [Display(Name = "Id No.")]
        [Required(ErrorMessage = "Please enter Id No.")]
        // [RegularExpression("^[1-9]([0-9])+$", ErrorMessage = "Please enter a valid Id No.")]
        //  [StringLength(9, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string IdNo { get; set; }

        public string ClientIp { get; set; }
        public string InquiryType { get; set; }
    }

    public class ClientAccountDto
    {
        [Display(Name = "Id No.")]
        [Required(ErrorMessage = "Please enter Id No.")]
        // [RegularExpression("^[1-9]([0-9])+$", ErrorMessage = "Please enter a valid Id No.")]
        //[StringLength(9, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string IdNo { get; set; }

        [Display(Name = "Account No.")]
        [Required(ErrorMessage = "Please enter Account No.")]
        [StringLength(12, ErrorMessage = "Account No. must be {1} characters long.", MinimumLength = 11)]
        public string AccountNo { get; set; }

        public string ClientIp { get; set; }
    }

    public class UserId
    {
        [Display(Name = "Username")]
        [Required(ErrorMessage = "Please enter Username")]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "Please enter a valid Username")]
        [StringLength(15, ErrorMessage = "{0} must be at least {2} and at max {1} characters long", MinimumLength = 6)]
        public string Username { get; set; }
    }
    public class DTO
    {
        [Required(ErrorMessage = "Please enter Data")]
        public string Data { get; set; }

        [Required(ErrorMessage = "Key Is Missing")]
        public string Data1 { get; set; }

    }
  

    public class GetBudgetsDTO
    {
        [Required(ErrorMessage = "CustomerId Is Missing")]
        public string CustomerId { get; set; }
        public string BudgetId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }


    }
    public class CreateCustomer
    {
        [Required(ErrorMessage = "Bank Name is missing")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "CustomerID is missing")]
        public string CustomerId { get; set; }
        [Required(ErrorMessage = "Date of birth is missing")]
        public string Dob { get; set; }
        [Required(ErrorMessage = "Account number is missing")]
        public string AccountNumber { get; set; }
        [Required(ErrorMessage = "Account description is missing")]

        public string AccountDescription { get; set; }
        [Required(ErrorMessage = "Account Currency is missing")]
        [StringLength(3, ErrorMessage = "Currency must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string AccountCurrency { get; set; }
        [Required(ErrorMessage = "Account opening date is missing")]
        public string AccountOpeningDate { get; set; }
    }

   
    public class UploadImageDTO
    {
        [Required(ErrorMessage = "Transaction key Is Missing")]
        public string TrxKey { get; set; }

        [Required(ErrorMessage = "Image Is Missing")]
        public string Image { get; set; }


        [StringLength(100, ErrorMessage = "Notes must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Required(ErrorMessage = "Notes Is Missing")]
        public string Note { get; set; }
        //Header
        [Required(ErrorMessage = "CustomerId Is Missing")]
        public string CustomerId { get; set; }
        [Required(ErrorMessage = "Channel id is required")]
        public string ChannelId { get; set; }
    }
    public class SendEmail
    {
        public string EventType { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public string ChangeBy { get; set; }

    }
    public class DeviceID
    {
        public BaseClass BaseClass { get; set; }
    }
    public class DTO1
    {
        [Required(ErrorMessage = "Please enter Data")]
        public string Data { get; set; }
        [Required(ErrorMessage = "Please enter Data1")]
        public string Data1 { get; set; }

    }
    public class GetIbanDetailDTO
    {
        [Required(ErrorMessage = "Please enter Account")]
        public string Account { get; set; }
        [Required(ErrorMessage = "Please enter Device")]
        public string DeviceID { get; set; }
        public BaseClass BaseClass { get; set; }
    }
    public class Email
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter Email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string EmailId { get; set; }

        public BaseClass BaseClass { get; set; }


    }
    public class TestDTO
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Please enter Email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string EmailId { get; set; }
        public Int32 Number { get; set; }
        public DateTime Data { get; set; }
    }

    public class ForgetPasswordotpDto
    {
        public string Otp { get; set; }
        public string UserName { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class DetailsDto
    {
        [Required(ErrorMessage = "Please enter Type.")]
        [StringLength(1, ErrorMessage = "Please enter a valid Type.", MinimumLength = 1)]
        public string Type { get; set; }
        [Required(ErrorMessage = "Please enter Device ID.")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class FatcaCrsField
    {
        public string LabelEn { get; set; }
        public string LabelAr { get; set; }
        public bool IsMandatory { get; set; }
        public int OrderNo { get; set; }
        public string Type { get; set; }
        public string Answer { get; set; }
        public string DocId { get; set; }
    }
    public class AccountClosedDTO
    {
        [Required(ErrorMessage = "AccountId is mandatory")]
        public string AccountId { get; set; }
        public string DeviceId { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class OpenWakalaDto
    {
        [Display(Name = "Transfer Type")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string TransferType { get; set; } // D for fawri , N for fawri+ , I for internal KFH account , O for own KFH account , S for international bank account

        [Display(Name = "From Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string FromAccountId { get; set; } // IBAN

        [Display(Name = "From Account Title")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromAccountTitle { get; set; } // full name

        [Display(Name = "From Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FromCurrency { get; set; }

        [Display(Name = "To Account No.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountId { get; set; } // IBAN

        [Display(Name = "To Account Title")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string ToAccountTitle { get; set; } // full name

        [Display(Name = "To Currency")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [StringLength(3, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string ToCurrency { get; set; }

        [Display(Name = "Beneficiary ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string BeneficiaryId { get; set; } // beneficiary ID - if new add new beneficiary and get ben. id

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double Amount { get; set; } // amount to transfer

        [Display(Name = "Exchange Rate")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public double ExRate { get; set; } // exchange rate - 1.00 if BHD

        [Display(Name = "Local Eq.")]
        [Required(ErrorMessage = "{0} is mandatory")]
        [Range(typeof(double), "0.1", "999999999999", ErrorMessage = "Plese enter a valid {0}")]
        public double LocalEq { get; set; } // amount in BHD

        [Required(ErrorMessage = "OTP is mandatory")]
        [Display(Name = "OTP Code")]
        public string OTPCode { get; set; }


        [Display(Name = "Device ID")]
        [Required(ErrorMessage = "{0} is mandatory")]
        public string DeviceID { get; set; }
        [Display(Name = "wakala Product Id")]
        [Required(ErrorMessage = "{0} is mandatory")]

        public int WakalaProduct { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class PdfMonth
    {
        public string Month { get; set; }
        public List<PdfDto> AccountList { get; set; }
    }
    public class PdfDto
    {
        public string RimNo { get; set; }
        public string IsExisting { get; set; }

        public string CprNo { get; set; }
        public string FullName { get; set; }
        public string AccountNo { get; set; }
        public DateTime? AccountDate { get; set; }
        public string Signature { get; set; }
        public string CprBack { get; set; }
    }
    public class HoldDto
    {
        [Required(ErrorMessage = "Please enter Device Id")]
        public string DeviceId { get; set; }

        [Required(ErrorMessage = "Please enter Card Account Id")]
        public string CardAccount { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class EmailSend
    {
        public int Id { get; set; }
        public string EventID { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public Dictionary<string, string> KeyValuePairss { get; set; }

    }
    public class WakalaGLDTO
    {
        [Required(ErrorMessage = "Please enter DeviceId")]
        public string DeviceId { get; set; }
        [Required(ErrorMessage = "Please enter ProductId")]
        public int ProductId { get; set; }
        [Required(ErrorMessage = "Please enter Currency")]
        public string CurrencyId { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class SetTrans
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter AccountID .")]
        public string AccountID { get; set; }

        public List<FatcaCrsField> Data { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class ProductIdDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class ActivateAccountDTO
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }
        [Required(ErrorMessage = "Please Enter Account No")]

        public string AccountNo { get; set; }

        [Required(ErrorMessage = "Please Enter Device Id")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class OpenAccountAM
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        //[StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        //[StringLength(8, ErrorMessage = "Please enter a valid Account Id.", MinimumLength = 5)]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Customer Id.")]
        //[StringLength(8, ErrorMessage = "Please enter a valid Customer Id.", MinimumLength = 5)]
        public string CustomerId { get; set; }
        [Required(ErrorMessage = "Please enter AML Remarks.")]
        //[StringLength(8, ErrorMessage = "Please enter a valid Customer Id.", MinimumLength = 5)]
        public string AMLRemarks { get; set; }
        public BaseClass BaseClass { get; set; }

    }
    public class ProductAccountIdDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }
    }
    public class OpenActualAccountDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Device Id.")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }



    }
    public class RequestCallDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }


        [Required(ErrorMessage = "Please enter NotificationID.")]

        public string RegistrationId { get; set; }
        [Required(ErrorMessage = "Please enter DeviceID.")]

        public string DeviceID { get; set; }

        public string Platform { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class GetGenderStatusDTO
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter DeviceID.")]

        public string DeviceID { get; set; }

        public string Platform { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class FireBaseDto
    {
        [Required(ErrorMessage = "Please enter User Id.")]

        public string UserID { get; set; }


        public string Node { get; set; }

    }

    public class GetImageDto
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Conference Link.")]
        public string ConferenceLink { get; set; }
        [Required(ErrorMessage = "Please enter Image.")]
        public string ImageName { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class GenericGetMethod
    {
        public string Product { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class UploadImageDto
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }
        [Required(ErrorMessage = "Please enter Call Id.")]
        public string CallId { get; set; }


        [Required(ErrorMessage = "Please enter Image Type.")]
        public string ImageType { get; set; }

        [Required(ErrorMessage = "Please enter Image.")]
        public string Image { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class VideoCallGetDto
    {
        [Required(ErrorMessage = "Please provide Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please provide Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please provide Account Id.")]
        public int AccountId { get; set; }

        // [Required(ErrorMessage = "Please enter Conference Link.")]
        public string ConferenceLink { get; set; }

        [Required(ErrorMessage = "Please provide Type ")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Please provide Call Id ")]
        public string CallId { get; set; }

        [Required(ErrorMessage = "Please provide  Id ")]
        public Int32 Id { get; set; }
        public BaseClass BaseClass { get; set; }

    }
    public class GetDetailbyCPRDto
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Cpr Number.")]
        public string Cpr { get; set; }

        [Required(ErrorMessage = "Please enter Call Id.")]
        public string CallId { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class GetImagebyCPRDto
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Cpr Number.")]
        public string Cpr { get; set; }
        [Required(ErrorMessage = "Please enter Image name.")]
        public string ImageName { get; set; }
        [Required(ErrorMessage = "Please enter Call Id.")]
        public string CallId { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class ChangePasswordDto
    {
        [Display(Name = "Current password")]
        [Required(ErrorMessage = "Please enter Current password.")]
        [DataType(DataType.Password)]
        // [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Current password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [Display(Name = "New password")]
        [Required(ErrorMessage = "Please enter New password.")]
        [DataType(DataType.Password)]
        // [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid New password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm new password")]
        [Required(ErrorMessage = "Please enter Confirm new password.")]
        [Compare("NewPassword", ErrorMessage = "The New password and Confirm new password does not match.")]
        [DataType(DataType.Password)]
        // [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Confirm new password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string ConfirmPassword { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class ChangeMobileDto
    {
        [Required(ErrorMessage = "Please enter Mobile Number.")]
        //[RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid Mobile Number")]
        [StringLength(15, ErrorMessage = "Mobile Number must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class TopupDto
    {
        public string PaymentId { get; set; }
        public BaseClass BaseClass { get; set; }
    }
    public class ChangeEmailDto
    {
        [Required(ErrorMessage = "Please enter Email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class EnableFingerPrint
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Cipher")]
        public string Cipher { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class GetFingerPritnStatus
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class GetSettingDTO
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        // [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        //[StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Product")]

        public string Product { get; set; }


        [Required(ErrorMessage = "Please enter Account")]

        public int AccountId { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class GetMainDashboardDto
    {
        public BaseClass BaseClass { get; set; }
    }

    public class GetAccountDashBoardDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Account ID")]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type")]
        public string AccountType { get; set; }

        [Required(ErrorMessage = "Please enter Account Key")]
        public string AccountKey { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class LibCurrencyDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Currency ID")]
        public string CurrencyId { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class RequestDebitDto
    {

        [Required(ErrorMessage = "Please enter Product")]

        public string Product { get; set; }


        [Required(ErrorMessage = "Please enter Account")]

        public int AccountId { get; set; }



        public string BranchId { get; set; }

        [Required(ErrorMessage = "Please enter Method")]

        public string Method { get; set; }


        [Required(ErrorMessage = "Please enter Device Id")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class AppLogDTO
    {

        public string ID { get; set; }
        public string DeviceID { get; set; }
        public string Message { get; set; }
        public string Platform { get; set; }
        public DateTime CreateTime { get; set; }
        public string UserId { get; set; }
        public BaseClass BaseClass { get; set; }

    }
    public class PostDTO
    {

        public BaseClass BaseClass { get; set; }

    }

    public class SendCountries
    {
        public string IsoCode3 { get; set; }
        public string PhoneCode { get; set; }
        public string CountryName { get; set; }
        public bool? IsAllowedCountry { get; set; }
        public bool? IsMobileAllowed { get; set; }
        public int? MobileMax { get; set; }
        public int? MobileMin { get; set; }
        public bool? IdForensic { get; set; }
        public bool? PassportScan { get; set; }
    }


    public class ForensicResultDto
    {

        [Required(ErrorMessage = "Please enter InstanceID")]

        public string InstanceID { get; set; }
        [Required(ErrorMessage = "Please enter DocumentNo")]

        public string DocumentNo { get; set; }
        [Required(ErrorMessage = "Please enter Document Type")]

        public string DocumentType { get; set; }
        [Required(ErrorMessage = "Please enter Device Id")]

        public string DeviceID { get; set; }


        [Required(ErrorMessage = "Please enter Customer Type")]
        public string CustomerType { get; set; }
        public string VisiblePattern1 { get; set; }
        public string VisiblePattern2 { get; set; }
        public string VisiblePattern3 { get; set; }
        public string BirthDateCheckDigit { get; set; }
        public string BirthDateValid { get; set; }
        public string CompositeCheckDigit { get; set; }
        public string DocumentClassification { get; set; }
        public string DocumentExpired { get; set; }
        public string DocumentNumberCheckDigit { get; set; }
        public string DocumentNumberCrossCheck { get; set; }
        public string ExpirationDateCheckDigit { get; set; }
        public string ExpirationDateValid { get; set; }
        public string FullNameCrossCheck { get; set; }
        public string IssuingStateValid { get; set; }
        public string PersonalNumberCheckDigit { get; set; }
        public string Barcode { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class CheckStatusDTO
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Conference.")]
        public string ConferenceLink { get; set; }
        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }


    }
    public class CallCustomerDTO
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }
    }

    public class CallScheduledCustomerDTO
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }
        [Required(ErrorMessage = "Please enter Call Id.")]
        public string CallId { get; set; }
        [Required(ErrorMessage = "Please enter  Id.")]
        public int Id { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class ContactUsDTO
    {



        [Required(ErrorMessage = "Please enter Subject.")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Please enter Message.")]
        public string Message { get; set; }


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }



    public class UpdateCallStatusDto
    {

        //public string AgentFName { get; set; }
        //public string AgentMName { get; set; }
        //public string AgentLName { get; set; }

        [Required(ErrorMessage = "Please enter Existing Type.")]
        public string IsExisting { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Agent name.")]
        public string AgentName { get; set; }

        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Conference Link.")]
        public string EncodedUrl { get; set; }

        [Required(ErrorMessage = "Please enter Call Id.")]
        public string CallId { get; set; }

        public string AmlCheck { get; set; }

        public string CountryJurisdiction1 { get; set; }
        public string Tin1 { get; set; }
        public string ReasonA1 { get; set; }
        public string ReasonInput1 { get; set; }

        public string CountryJurisdiction2 { get; set; }
        public string Tin2 { get; set; }
        public string ReasonA2 { get; set; }
        public string ReasonInput2 { get; set; }

        public string CountryJurisdiction3 { get; set; }
        public string Tin3 { get; set; }
        public string ReasonA3 { get; set; }
        public string ReasonInput3 { get; set; }

        public string CountryJurisdiction4 { get; set; }
        public string Tin4 { get; set; }
        public string ReasonA4 { get; set; }
        public string ReasonInput4 { get; set; }

        public string CountryJurisdiction5 { get; set; }
        public string Tin5 { get; set; }
        public string ReasonA5 { get; set; }
        public string ReasonInput5 { get; set; }

        public string CashDeposit { get; set; }
        public string CashWithdrawal { get; set; }
        public string ChequeDeposit { get; set; }
        public string ChequeWithdrawal { get; set; }
        public string InternalTransfer { get; set; }
        public string MoneyTransfers { get; set; }
        public string InwardInternational { get; set; }
        public string OutnwardInternational { get; set; }

        [StringLength(2, ErrorMessage = "Please enter a valid Status.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Status.")]
        public string Dd_status { get; set; }

        [Required(ErrorMessage = "Please enter Remarks.")]
        public string Remarks { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class AgentFatcaResponseDTO
    {
        public string CountryJurisdiction { get; set; }
        public string Tin { get; set; }
        public bool ReasonA { get; set; }
        public bool ReasonB { get; set; }
        public bool ReasonC { get; set; }
    }

    public class CallStatusDto
    {
        [Required(ErrorMessage = "Please enter Customer Id.")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Conference Link.")]
        public string ConferenceLink { get; set; }

        [StringLength(1, ErrorMessage = "Please enter a valid Status.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Status.")]
        public string Status { get; set; }
        //[StringLength(1, ErrorMessage = "Please enter a valid Status.", MinimumLength = 1)]

    }
    public class ExitingSchedule
    {
        public string Date { get; set; }
        public string Time { get; set; }

    }

    public class CheckExistanceSchedule
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public String ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }
        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }
        [Required(ErrorMessage = "Please enter TimeZone.")]
        public Int32 DateToSend { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public enum Gender { M, F }

    public class TransferAcctDto
    {
        public string ReversalId { get; set; }
        public string Iban { get; set; }
        public string FullName { get; set; }
        public string CountryCode { get; set; }
        public string BankCode { get; set; }
        public string SwiftCode { get; set; }
        public string BenificiaryId { get; set; }

        [Required(ErrorMessage = "Account ID is mandatory")]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Account Type is mandatory")]
        public string AccountType { get; set; }

        [Required(ErrorMessage = "Entry Type is mandatory")]
        public string EntryType { get; set; }

        [Required(ErrorMessage = "Apply Type is mandatory")]
        public string ApplyType { get; set; }

        public string EffectiveDate { get; set; }

        [Required(ErrorMessage = "Transfer Type is mandatory")]
        public string TransferType { get; set; }

        [Required(ErrorMessage = "Currency Code is mandatory")]
        public string CcyCode { get; set; }

        [Required(ErrorMessage = "Amount is mandatory")]
        public double Amount { get; set; }

        [Required(ErrorMessage = "Transfer Type is mandatory")]
        public double ExchangeRate { get; set; }

        [Required(ErrorMessage = "Local Eq. is mandatory")]
        public double LocalEq { get; set; }

        [Required(ErrorMessage = "Memo is mandatory")]
        public string Memo { get; set; }

        public string PaymentId { get; set; }
    }

    //public class FinancialEntry
    //{
    //    [Required(ErrorMessage = "AccountId  is required")]
    //    public string AcctId { get; set; }
    //    [Required(ErrorMessage = "Account Type  is required")]
    //    public string AcctType { get; set; }
    //    [Required(ErrorMessage = "Entry Type  is required")]
    //    public string EntryType { get; set; }
    //    public DateTime? EffDt { get; set; }
    //    [Required(ErrorMessage = "Currency Code  is required")]
    //    public string CurCode { get; set; }
    //    [Required(ErrorMessage = "Amount  is required")]
    //    public double Amt { get; set; }

    //    public double ForeignAmount { get; set; }

    //    [Required(ErrorMessage = "Exchange Rate  is required")]
    //    public double Rate { get; set; }
    //    [Required(ErrorMessage = "Memo  is required")]
    //    public string Memo { get; set; }
    //    [Required(ErrorMessage = "Channel Id  is required")]
    //    public string ChannelId { get; set; }
    //    [Required(ErrorMessage = "Description Id  is required")]
    //    public string DescId { get; set; }

    //    public string ChequeId { get; set; }
    //    public DateTime? ChequeDate { get; set; }
    //    public string ReferenceId { get; set; }
    //    public string VAccountID { get; set; }
    //}


}
