﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class FatcaCrsDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }

        public string Response1 { get; set; }
        public string Response2 { get; set; }
        public string Response3 { get; set; }
        public string Response4 { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
