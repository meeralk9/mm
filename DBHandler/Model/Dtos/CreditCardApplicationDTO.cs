﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class CreditCardApplicationDTO
    {
        [Required]
        public int Method { get; set; }
        [Required]
        public int CardId { get; set; }
        public int BranchId { get; set; }
        [Required]
        public string AvailableBalance { get; set; }
        [Required]
        public string NameOnCard { get; set; }
        [Required]
        public string DeviceID { get; set; }
        public string CardCategory { get; set; }
        public string Address { get; set; }
        public string PaymentOption { get; set; }
        public string HomeTel { get; set; }
        public string MotherName { get; set; }
        public double CreditLimit { get; set; }
        public string PledgeProduct { get; set; }
        public string PledgeAccount { get; set; }
        public double Rate { get; set; }
        public BaseClass BaseClass { get; set; }
    }
    public class StatementDatesDTO
    {
        public string Token { get; set; }
        public BaseClass BaseClass { get; set; }
    }
    public class StatementDTO
    {
        public string Token { get; set; }
        public string DeviceID { get; set; }
        public DateTime Date { get; set; }
        public int IsEmail { get; set; }
        public BaseClass BaseClass{get;set;}
    }
    public class CreditCardLimit
    {
        [Required]
        public int CardId { get; set; }
        [Required]
        public string CurrencyId { get; set; }
        [Required]
        public string AvailableBalance { get; set; }

        [Required]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }
}
