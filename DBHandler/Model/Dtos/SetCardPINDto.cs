﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class SetCardPINDto
    {
        [Required(ErrorMessage = "Please Provide Card Number")]
        [StringLength(6, ErrorMessage = "Please enter a valid Card Number.", MinimumLength = 6)]
        public string CardNumber { get; set; }

        [Required(ErrorMessage = "Please Provide PIN")]
        [StringLength(4, ErrorMessage = "Please enter a valid PIN.", MinimumLength = 4)]
        public string CardPin { get; set; }

        [Required(ErrorMessage = "Please Provide Confirm PIN")]
        [StringLength(4, ErrorMessage = "Please enter a valid Confirm PIN.", MinimumLength = 4)]
        public string ReEnterPIN { get; set; }

        [Required(ErrorMessage = "Please Provide All Data")]
        public string TrackingId { get; set; }
        public BaseClass BaseClass { get; set; }

    }
}
