﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class GetBeneficiaryDto
    {
        
        [Required(ErrorMessage = "Please enter Beneficiary ID")]
        public string BeneficiaryID { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class AddBeneficiaryDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }
        
        [Required(ErrorMessage = "Please enter Beneficiary Type")]
        [StringLength(1, ErrorMessage = "Beneficiary Type must not be greater than 1 character.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter Nick Name")]
        [StringLength(50, ErrorMessage = "Nick Name must not be greater than 50 characters.")]
        public string NickName { get; set; }

        [Required(ErrorMessage = "Please enter Full Name")]
        [StringLength(100, ErrorMessage = "Full Name must not be greater than 100 characters.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter Account No.")]
        [StringLength(50, ErrorMessage = "Account No. must not be greater than 50 characters.")]
        public string AccountNo { get; set; }

        //[Required(ErrorMessage = "Please enter Swift Code")]
        [StringLength(50, ErrorMessage = "Swift Code must not be greater than 50 characters.")]
        public string SwiftCode { get; set; }

        //[Required(ErrorMessage = "Please enter Sort/BIC Code")]
        [StringLength(50, ErrorMessage = "Sort/BIC Code must not be greater than 50 characters.")]
        public string SortCode { get; set; }

        [Required(ErrorMessage = "Please select Bank")]
        [StringLength(50, ErrorMessage = "Bank must not be greater than 50 characters.")]
        public string BankCode { get; set; }

        [Required(ErrorMessage = "Please select Country")]
        [StringLength(50, ErrorMessage = "Country must not be greater than 50 characters.")]
        public string CountryCode { get; set; }

        //[Required(ErrorMessage = "Please enter Address")]
        [StringLength(100, ErrorMessage = "Address must not be greater than 100 characters.")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessage = "Bank Name must not be greater than 50 characters.")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Please enter SendOTP")]

        public bool SendOTP { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string BeneficiaryAccountCurrency { get; set; }

        public string BeneficiaryCountry { get; set; }

        public string BeneficiaryBankCountry { get; set; }

        public string BeneficiaryBankAddress { get; set; }

        public string RoutingNo { get; set; }

        public string BSB { get; set; }
        public string IFSCCode { get; set; }
        public string BeneficiaryIban { get; set; }
        public BaseClass BaseClass { get; set; }

    }

    public class UpdateBeneficiaryDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Beneficiary ID")]
        public string BeneficiaryID { get; set; }

        [Required(ErrorMessage = "Please enter Beneficiary Type")]
        [StringLength(1, ErrorMessage = "Beneficiary Type must not be greater than 1 character.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter Nick Name")]
        [StringLength(50, ErrorMessage = "Nick Name must not be greater than 50 characters.")]
        public string NickName { get; set; }

        [Required(ErrorMessage = "Please enter Full Name")]
        [StringLength(100, ErrorMessage = "Full Name must not be greater than 100 characters.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter Account No.")]
        [StringLength(50, ErrorMessage = "Account No. must not be greater than 50 characters.")]
        public string AccountNo { get; set; }

        //[Required(ErrorMessage = "Please enter Swift Code")]
        [StringLength(50, ErrorMessage = "Swift Code must not be greater than 50 characters.")]
        public string SwiftCode { get; set; }

        //[Required(ErrorMessage = "Please enter Sort/BIC Code")]
        [StringLength(50, ErrorMessage = "Sort/BIC Code must not be greater than 50 characters.")]
        public string SortCode { get; set; }

        [Required(ErrorMessage = "Please select Bank")]
        [StringLength(50, ErrorMessage = "Bank must not be greater than 50 characters.")]
        public string BankCode { get; set; }

        [Required(ErrorMessage = "Please select Bank Name")]
        [StringLength(50, ErrorMessage = "Bank Name must not be greater than 50 characters.")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Please select Country")]
        [StringLength(50, ErrorMessage = "Country must not be greater than 50 characters.")]
        public string CountryCode { get; set; }

        //[Required(ErrorMessage = "Please enter Address")]
        [StringLength(100, ErrorMessage = "Address must not be greater than 100 characters.")]
        public string Address { get; set; }

        public BaseClass BaseClass { get; set; }
    }

    public class DeleteBeneficiaryDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Beneficiary ID")]
        public string BeneficiaryID { get; set; }

        public BaseClass BaseClass { get; set; } 
    }

    public class VerifyBeneficiaryDto
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter OTP Code.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid OTP Code.")]
        [StringLength(6, ErrorMessage = "OTP Code must be 6 characters long.", MinimumLength = 6)]
        public string OTPCode { get; set; }

        public BaseClass BaseClass { get; set; }
    }
}
