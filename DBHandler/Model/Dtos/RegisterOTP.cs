﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class RegisterOTP
    {
        public string Email { get; set; }
        public string IsoCode3 { get; set; }
        public string PhoneCode { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
