﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{


    public class TalentDTO
    {


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Last Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please select Document.")]
        public string Document { get; set; }
        //[StringLength(300, ErrorMessage = "Decription  must be at least {2} and at max {1} characters long.", MinimumLength = 50)]
        //[Required(ErrorMessage = "Please select Description.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter DOB.")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string TalentType { get; set; }

        [Required(ErrorMessage = "Please enter  Doc Type.")]
        public string DocType { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class TalentIPhoneDTO
    {


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Last Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please select Document.")]
        public string Document { get; set; }
        //[StringLength(300, ErrorMessage = "Decription  must be at least {2} and at max {1} characters long.", MinimumLength = 50)]
        //[Required(ErrorMessage = "Please select Description.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter DOB.")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string TalentType { get; set; }

        [Required(ErrorMessage = "Please enter FileName.")]
        public string FileName { get; set; }

        public BaseClass BaseClass { get; set; }
    }


    public class InternDTOIPhone
    {


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Last Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please select Document.")]
        public string Document { get; set; }
        //[StringLength(300, ErrorMessage = "Decription  must be at least {2} and at max {1} characters long.", MinimumLength = 50)]
        //[Required(ErrorMessage = "Please select Description.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter DOB.")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string InternType { get; set; }

        [Required(ErrorMessage = "Please enter self.")]
        public string Self { get; set; }

        [Required(ErrorMessage = "Please enter FileName.")]
        public string FileName { get; set; }

        public BaseClass BaseClass { get; set; }

    }
    public class InternDTO
    {


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Last Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Please enter Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please select Document.")]
        public string Document { get; set; }
        //[StringLength(300, ErrorMessage = "Decription  must be at least {2} and at max {1} characters long.", MinimumLength = 50)]
        //[Required(ErrorMessage = "Please select Description.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter DOB.")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string InternType { get; set; }

        [Required(ErrorMessage = "Please enter self.")]
        public string Self { get; set; }

        [Required(ErrorMessage = "Please enter  Doc Type.")]
        public string DocType { get; set; }

        public BaseClass BaseClass { get; set; }


    }


    public class DLDto
    {
        public string Id { get; set; }


        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }
        [Required(ErrorMessage = "Please enter Type.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter DOI.")]
        public DateTime? DOI { get; set; }

        [Required(ErrorMessage = "Please enter DOE.")]
        public DateTime? DOE { get; set; }


        [Required(ErrorMessage = "Please enter Front Card Image.")]
        public string FrontCardImage { get; set; }
        [Required(ErrorMessage = "Please enter Back Card Image.")]
        public string BackCardImage { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class PassportDto
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Please enter Type.")]
        public string Type { get; set; }
        // [Required(ErrorMessage = "Please Enter InstanceID")]
        public string InstanceID { get; set; }

        public int ScanPercentage { get; set; }

        [Required(ErrorMessage = "Please enter Full Name.")]
        [StringLength(100, ErrorMessage = "Full Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter First Name.")]
        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Middle Name must not be greater than 100 characters.")]
        public string MiddleName { get; set; }


        [StringLength(100, ErrorMessage = "Family Name must not be greater than 100 characters.")]
        public string FamilyName { get; set; }

        [Required(ErrorMessage = "Please enter Date of Birth.")]
        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please select Gender.")]
        [EnumDataType(typeof(Gender), ErrorMessage = "Please enter a valid Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Please enter Date of Expiry.")]
        public DateTime DOE { get; set; }

        [Required(ErrorMessage = "Please enter Document Code.")]
        // [StringLength(1, ErrorMessage = "Document Code must be 1 character long.", MinimumLength = 1)]
        public string DocumentCode { get; set; }

        [Required(ErrorMessage = "Please enter Document Number.")]
        public string DocumentNumber { get; set; }

        [Compare("Nationality", ErrorMessage = "Issuer and Nationality does not match.")]
        public string Issuer { get; set; }

        public string OptionalData1 { get; set; }
        public string OptionalData2 { get; set; }
        public string MrtDraw { get; set; }

        [Required(ErrorMessage = "Please enter Front Card Image.")]
        public string FrontCardImage { get; set; }

        public BaseClass BaseClass { get; set; }

    }
}
