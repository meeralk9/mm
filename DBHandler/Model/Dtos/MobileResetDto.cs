﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class MobileResetDto
    {
        [Display(Name = "ID No.")]
        [Required(ErrorMessage = "Please enter ID No.")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid ID No.")]
      
        public string IDNo { get; set; }
        
        [Required(ErrorMessage = "Please enter OTP Code.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid OTP Code.")]
        [StringLength(6, ErrorMessage = "OTP Code must be 6 characters long.", MinimumLength = 6)]
        public string OTPCode { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
     //   [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
     //   [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Confirm Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string ConfirmPassword { get; set; }

        public BaseClass BaseClass { get; set; }
    }


    public class FingerOTPDTO
    {
        
        [Required(ErrorMessage = "Please enter OTP Code.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid OTP Code.")]
        [StringLength(6, ErrorMessage = "OTP Code must be 6 characters long.", MinimumLength = 6)]
        public string OTPCode { get; set; }

        [Required(ErrorMessage = "Please enter Device ID.")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class VerifyOTP
    {

        [Required(ErrorMessage = "Please enter Type.")]

        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter OTP Code.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid OTP Code.")]
        [StringLength(6, ErrorMessage = "OTP Code must be 6 characters long.", MinimumLength = 6)]
        public string OTPCode { get; set; }

        public BaseClass BaseClass { get; set; }

    }

    public class SendOTP
    {
        [Required(ErrorMessage = "Please enter Type.")]
        public string Type { get; set; }

        public BaseClass BaseClass { get; set; }
    }
}
