﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class TimeSlotsDto
    {
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Send Date.")]
        public DateTime CallDate { get; set; }

       
        [Required(ErrorMessage = "Please Send Date.")]
        public Int32 DateToSend { get; set; }

        [Required(ErrorMessage = "Please Send Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter Product Id.")]
        public string ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Account Id.")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Device Id.")]
        public string DeviceID { get; set; }


        public BaseClass BaseClass { get; set; }


    }
}
