﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model.FeeChargesReversal
{
    public class FeeChargesReversalModel
    {
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = "FacId is required")]
        public string FacId { get; set; }
        [Required(ErrorMessage = "ProdId is required")]
        public string ProdId { get; set; }
        [Required(ErrorMessage = "RefNo is required")]
        public string RefNo { get; set; }
        [Required(ErrorMessage = "FeeType is required")]
        public string FeeType { get; set; }

        [Required(ErrorMessage = "ReversalType is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Partial", "Full" }, ErrorMessage = "- Valid value is either Partial or Full.")]
        public string ReversalType { get; set; }
        [Required(ErrorMessage = "ReversalAmt is required")]
        public decimal ReversalAmt { get; set; }
        [Required(ErrorMessage = "ReversalReason is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Erroneous Entry", "Incorrect Amount", "Incorrect Detials", "Exempted Segment", "Requested by Business", "Other" }, ErrorMessage = "- Valid value is either Erroneous Entry,Incorrect Amount,Incorrect Detials,Exempted Segment,Requested by Business, or Other.")]
        public string ReversalReason { get; set; }
        public string ReversalRemark { get; set; }
    }

    public class FeeChargesReversalResponse
    {
        public string FeeReverseRefNo { get; set; }
        public decimal FeeAvailReverseAmt { get; set; }
    }
}
