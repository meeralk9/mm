﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
   public class MonthWiseTimeLine
    {
        public List<TrxMonthwise> MonthlyTrx = new List<TrxMonthwise>();
        public List<Tabs> Tabs = new List<Tabs>();

    }
    public class Tabs
    {
        public string Tab { get; set; }
        public string Id { get; set; }
        public string Label { get; set; }

    }

    public class TrxMonthwise
    {
        public List<Transaction> transactions = new List<Transaction>();
    }

    public class Transaction
    {
        public string extraction { get; set; }
        public string movement { get; set; }
        public string description { get; set; }
        public decimal balance { get; set; }
        public string type{ get; set; }
        public string date{ get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
       // public List<CustomField> CustomFields { get; set; }
    }

    public class CustomField
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
