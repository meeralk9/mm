﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class HeaderSearchtransaction
    {
        public bool success { get; set; }
        public bool authenticated { get; set; }
        public string message { get; set; }
    }

    public class CustomFieldSearchtransaction
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class PayloadSearchtransaction
    {
        public string key { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public double balance { get; set; }
        public string movement { get; set; }
        public string type { get; set; }
        public DateTime extraction { get; set; }
        public string account_key { get; set; }
        public string account_name { get; set; }
        public List<CustomFieldSearchtransaction> custom_fields { get; set; }
    }

    public class RootObjectSearchtransaction
    {
        public HeaderSearchtransaction header { get; set; }
        public List<PayloadSearchtransaction> payload { get; set; }
    }
}
