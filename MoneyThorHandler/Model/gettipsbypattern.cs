﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model
{
    public class gettipsbypattern
    {
        public class Header
        {
            public bool success { get; set; }
        }

        public class Target
        {
            public string type { get; set; }
            public string key { get; set; }
        }

        public class Payload
        {
            public string tip_key { get; set; }
            public string title { get; set; }
            public string status { get; set; }
            public DateTime creation { get; set; }
            public string origin { get; set; }
            public string running_from { get; set; }
            public string running_to { get; set; }
            public int priority { get; set; }
            public string latency_mode { get; set; }
            public List<object> events { get; set; }
            public Target target { get; set; }
            public string data { get; set; }
        }

        public class RootObject
        {
            public Header header { get; set; }
            public List<Payload> payload { get; set; }
        }
    }
}
