﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.Category
{
    public class LastOccurrence
    {
        public string key { get; set; }
        public string date { get; set; }
    }

    public class Locale
    {
        public string language { get; set; }
        public string value { get; set; }
        public string label { get; set; }
    }

    public class Payload
    {
        public string category { get; set; }
        public LastOccurrence last_occurrence { get; set; }
        public int count { get; set; }
        public double min_debit_amount { get; set; }
        public double max_debit_amount { get; set; }
        public double sum { get; set; }
        public string type { get; set; }
        public string parent { get; set; }
        public List<Locale> locales { get; set; }
        public Image Image { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public List<Payload> payload { get; set; }
    }
}
