﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;

namespace MoneyThorHandler.Model
{

    public class MoneyThorCustomer
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public CreateCsutomer CustomerData { get; set; }
    }

    public class MoneyThorTransfer
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public TransactionDTO transfer{ get; set; }
    }

    public class CategoryModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }

        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
    }

    public class BudgetRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public string CustomerAccount { get; set; }
        public string CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string budgetKey { get; set; }
    }

    public class TotalCategoryRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        public string CustomerAccount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string movement { get; set; }
        public string category { get; set; }
        public string taget { get; set; }
        public bool? expand_tip { get; set; }
        public string MtAccount { get; set; }
    }

    public class totalsByCategoryResponse
    {
        public string min_date { get; set; }
        public string max_date { get; set; }
        public string type { get; set; }
        public string match { get; set; }
        public string target { get; set; }
        public List<string> accounts { get; set; }


    }
    public class TipRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        public string tip { get; set; }
    }

    public class TransactionSearchModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public string CustomerId { get; set; }
        public GetTransactionDetails GetTransactionDetails { get; set; }

    }

    public class GetTransactionDetails
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public string key { get; set; }
        public bool expand_tip { get; set; }
    }
    public class TimeLineDTO
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }

        public string CustomerAccount { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        public string Months { get; set; }
        public string query { get; set; }
        public string minAmount { get; set; }
        public string maxAmount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class CustomFieldss
    {
        public string name { get; set; }
        public string value { get; set; }
        public bool? immutable { get; set; } = false;
        public bool? core { get; set; } = false;
    }

    public class synccustomfieldDto
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        public synccustomfield synccustomfield { get; set; }
    }
    public class synccustomfield
    {
        public string key { get; set; }
        public List<CustomFieldss> custom_fields { get; set; }
     //   public SynchronizeCustomFieldsMode mode { get; set; }
    }

    public class SynchronizeCustomFieldsMode
    {
        public string type { get; set; }
        public string scope { get; set; }

    }
    public class TrackEventDto
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        [Required(ErrorMessage = "Event is required.")]
        public Root Root { get; set; }
    }

    public class Event
    {
        public string category { get; set; }
        public string action { get; set; }
        public string label { get; set; }
    }

    public class Root
    {
        public List<Event> events { get; set; }
    }

    public class GetTipsByPattenDto
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string customerId { get; set; }
        public string account_type { get; set; }
        public string[] accounts { get; set; }
        public DateTime? min_date { get; set; }
        public DateTime? max_date { get; set; }
        public string target { get; set; }
        public string family { get; set; }
        public string type { get; set; }
        public string match { get; set; }
        public bool? expand_tip { get; set; }
        public string origin { get; set; }
        public string status { get; set; }
    }
    public class EventValue
    {
        public string data { get; set; }
        public string comment { get; set; }
        public string reaction { get; set; }
        public string request_copy { get; set; }
        public string latency_days { get; set; }
        public string latency_event { get; set; }

    }
    public class SearchTransactionsDto
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string customerId { get; set; }
        public string Key { get; set; }
        public bool? similar { get; set; }
        public string account_type { get; set; }
        public string[] accounts { get; set; }
        public string movement { get; set; }
        public string status { get; set; }
        public string query { get; set; }
        public string currency { get; set; }
        public DateTime? min_date { get; set; }
        public DateTime? max_date { get; set; }
        public double? min_debit_amount { get; set; }
        public double? max_debit_amount { get; set; }
        public double? min_credit_amount { get; set; }
        public double? max_credit_amount { get; set; }
        public string[] days_of_week { get; set; }
        public DateTime? min_authorization_date { get; set; }
        public DateTime? max_authorization_date { get; set; }
        public DateTime? min_authorization_time { get; set; }
        public DateTime? max_authorization_time { get; set; }
        public double? sort { get; set; }
        public string order { get; set; }
        public int? page { get; set; }
        public int? count { get; set; }
        public bool? expand_tip { get; set; }
    }
    public class GetBalance
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }
        public string AccountType { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        public string[] AccountKeys { get; set; }
        public string Currency { get; set; }
        public DateTime? min_date { get; set; }
        public DateTime? max_date { get; set; }
    }

    public class TransactionDetailDTO
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public MoneyThorHandler.Model.TimeLine.TransactionDetail transactionDetail { get; set; }
        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
    }

    public class CreateBudgetModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public RequestBudget budget { get; set; }

        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
    }

    public class  UpdateBudgetModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public UodateBudgetDTO budget { get; set; }

        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
    }

    public class DeleteBudgetModel
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        public DeleteBudgetDTO budget { get; set; }

        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
    }

    public class UodateBudgetDTO
    {
        public string key { get; set; }
        public string name { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
    }

    public class DeleteBudgetDTO
    {
        public string key { get; set; }
    }
}
