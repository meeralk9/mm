﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;


namespace MoneyThorHandler.Model.Promotion
{
    public class PromotionRequest
    {
        [Required(ErrorMessage = " is a mandatory field.")]
        public SignOnRq SignOnRq { get; set; }


        [Required(ErrorMessage = "Customer ID is required.")]
        public string CustomerId { get; set; }
        [Required(ErrorMessage = "Type is required.")]
        public string type { get; set; }
        public string family { get; set; }
        [Required(ErrorMessage = "Min Date is required.")]
        public DateTime min_date { get; set; }
        [Required(ErrorMessage = "Max Date is required.")]
        public DateTime max_date { get; set; }
    }

    public class Target
    {
        public string type { get; set; }
        public string key { get; set; }
    }

    public class Payload
    {
        public string tip_key { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public DateTime creation { get; set; }
        public string origin { get; set; }
        public string running_from { get; set; }
        public string running_to { get; set; }
        public int priority { get; set; }
        public string latency_mode { get; set; }
        public List<object> events { get; set; }
        public Target target { get; set; }
        public string data { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public List<Payload> payload { get; set; }
    }
}
