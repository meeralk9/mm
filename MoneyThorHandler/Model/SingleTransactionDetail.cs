﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.SingleTransactionDetail
{
    public class Header
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public bool authenticated { get; set; }
    }

    public class CustomField
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Payload
    {
        public string key { get; set; }
        public string date { get; set; }
        public DateTime extraction { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string movement { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public double balance { get; set; }
        public List<CustomField> custom_fields { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public Payload payload { get; set; }
    }
}
