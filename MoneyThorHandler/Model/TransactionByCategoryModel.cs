﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.TransactionByCategoryModel
{
    public class Header
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int code { get; set; }
    }

    public class CustomField
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Tip
    {
        public string tip_key { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public DateTime creation { get; set; }
        public string origin { get; set; }
        public string running_from { get; set; }
        public string running_to { get; set; }
        public int priority { get; set; }
        public string latency_mode { get; set; }
        public List<object> events { get; set; }
        public string data { get; set; }
    }

    public class Transaction
    {
        public string account_key { get; set; }
        public string account_name { get; set; }
        public string key { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public double balance { get; set; }
        public string movement { get; set; }
        public string type { get; set; }
        public DateTime extraction { get; set; }
        public List<CustomField> custom_fields { get; set; }
        public List<Tip> tips { get; set; }
    }

    public class Payload
    {
        public string currency { get; set; }
        public List<Transaction> transactions { get; set; }
        public List<List<object>> history { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public List<Payload> payload { get; set; }
    }
}
