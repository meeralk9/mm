﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyThorHandler.Model.TotalCategory
{
    public class Header
    {
        public bool success { get; set; }
        public bool authenticated { get; set; }
        public string message { get; set; }
        public int code { get; set; }
    }

    public class Category
    {
        public string category { get; set; }
        public double amount { get; set; }
        public int count { get; set; }
        public List<List<object>> history { get; set; }
    }

    public class Payload
    {
        public List<Category> categories { get; set; }
        public string currency { get; set; }
        public double grand_total { get; set; }
        public List<List<object>> history_total { get; set; }
        public string last_transaction_date { get; set; }
    }

    public class RootObject
    {
        public Header header { get; set; }
        public Payload payload { get; set; }
    }
}
