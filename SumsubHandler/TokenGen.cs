﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SumsubHandler.TokenGen
{
    public class RequestTokenGen
    {
        public string applicantId { get; set; }
    }

    public class ErrorResponse
    {
        public string Error { get; set; }

    }

    public class ResponseTokenGen
    {
            public string token { get; set; }
            public string userId { get; set; }
    }
}
