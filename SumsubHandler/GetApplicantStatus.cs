﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SumsubHandler.GetApplicantStatus
{
    public class RequestGetApplicantStatus
    {
        public string applicantId { get; set; }
    }

    public class ErrorResponse
    {
        public string description { get; set; }
        public int code { get; set; }
        public string correlationId { get; set; }
    }

    public class ReviewResult
    {
        public string moderationComment { get; set; }
        public string reviewAnswer { get; set; }
    }

    public class _1791824894
    {
        public string moderationComment { get; set; }
        public string reviewAnswer { get; set; }
        public List<string> rejectLabels { get; set; }
        public string reviewRejectType { get; set; }
    }

    public class ImageReviewResults
    {
        public _1791824894 _1791824894 { get; set; }
    }

    public class IDENTITY
    {
        public ReviewResult reviewResult { get; set; }
        public string country { get; set; }
        public string idDocType { get; set; }
        public List<string> imageIds { get; set; }
        public ImageReviewResults imageReviewResults { get; set; }
        public bool forbidden { get; set; }
        public object stepStatuses { get; set; }
    }

    public class ResponseGetApplicantStatus
    {
        public IDENTITY IDENTITY { get; set; }
        public SELFIE SELFIE { get; set; }
    }

    public class SELFIE
    {
        public ReviewResult reviewResult { get; set; }
        public string country { get; set; }
        public string idDocType { get; set; }
        public List<int> imageIds { get; set; }
        public ImageReviewResults imageReviewResults { get; set; }
    }
}
